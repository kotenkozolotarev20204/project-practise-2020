#pragma once
#include "ComputersList.h"

using namespace std;

const string DATA_FILE = "RegList.txt";
const string ADMIN_DATA = "Admin 505090125";
string shoppingCartFile;

int checkDataValid(string data)
{
	if (data == ADMIN_DATA)
	{
		return 2;
	}

	ifstream fromFile(DATA_FILE);
	string buf;

	while (true)
	{
		getline(fromFile, buf);

		if (buf == data)
		{
			fromFile.close();
			return 1;
		}

		if (fromFile.eof())
		{
			fromFile.close();
			break;
		}
	}
	return 0;
}

bool checkLogin(string checkedLogin)
{
	ifstream fromFile(DATA_FILE);
	string buf, copy, login;
	int counter{};
	while (true)
	{
		getline(fromFile, buf);

		if (fromFile.eof())
		{
			break;
		}

		copy = buf[counter];
		while (copy != " ")
		{
			login += copy;
			counter++;
			copy = buf[counter];
		}

		if (checkedLogin == login)
		{
			fromFile.close();
			return false;
		}
		counter = 0;
		login = "";
	}
	fromFile.close();
	return true;
}

bool checkInvalidSymbols(string data)
{
	if (data.length() < 10)
	{
		return 0;
	}

	for (int i{}; i < data.length(); i++)
	{
		if (data[i] == ' ')
		{
			return 0;
		}
	}
	return 1;
}

void addUser(string data, string login)
{
	ofstream inFile(DATA_FILE, ios::app);
	inFile << data << endl;

	inFile.close();

	string binFile = login + ".dat";

	ofstream createFile(binFile);
	createFile.close();
}
