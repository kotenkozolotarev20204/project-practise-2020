#pragma once

#include "InputInformation.h"
#include "MyForm.h"
#include "ChangeSpecs.h"
#include "CheckOrders.h"

namespace ComputersShop {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� AdminMenu
	/// </summary>
	public ref class AdminMenu : public System::Windows::Forms::Form
	{
	public:
		AdminMenu(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~AdminMenu()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^ button1;
	protected:
	private: System::Windows::Forms::Button^ button2;

	private: System::Windows::Forms::LinkLabel^ linkLabel1;
	private: System::Windows::Forms::Button^ button3;

	protected:


	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(AdminMenu::typeid));
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->linkLabel1 = (gcnew System::Windows::Forms::LinkLabel());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(58, 12);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(127, 54);
			this->button1->TabIndex = 0;
			this->button1->Text = L"���������� ���������� � �������\r\n";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &AdminMenu::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(58, 72);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(127, 51);
			this->button2->TabIndex = 1;
			this->button2->Text = L"�������������� ��������\r\n";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &AdminMenu::button2_Click);
			// 
			// linkLabel1
			// 
			this->linkLabel1->AutoSize = true;
			this->linkLabel1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->linkLabel1->Location = System::Drawing::Point(89, 192);
			this->linkLabel1->Name = L"linkLabel1";
			this->linkLabel1->Size = System::Drawing::Size(68, 13);
			this->linkLabel1->TabIndex = 17;
			this->linkLabel1->TabStop = true;
			this->linkLabel1->Text = L"���� �����";
			this->linkLabel1->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &AdminMenu::linkLabel1_LinkClicked);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(58, 129);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(127, 49);
			this->button3->TabIndex = 18;
			this->button3->Text = L"�������� �������";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &AdminMenu::button3_Click);
			// 
			// AdminMenu
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->ClientSize = System::Drawing::Size(244, 214);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->linkLabel1);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->Name = L"AdminMenu";
			this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
			this->Text = L"������ ����������";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		InputInformation form;
		form.Owner = this;
		this->Hide();
		form.ShowDialog();
	}
	
	System::Void button2_Click(System::Object^ sender, System::EventArgs^ e)
	{
		if (getFileLength(PATH_CATALOG) == 0)
		{
			MessageBox::Show("������� ������! �������� ���� �� ���� ���������!");
			InputInformation form;
			form.Owner = this;
			this->Hide();
			form.ShowDialog();
		}
		else
		{
			ChangeSpecs form;
			form.Owner = this;
			form.ShowDialog();
		}
	}

	System::Void linkLabel1_LinkClicked(System::Object^ sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^ e) 
	{
		this->Hide();
		this->Owner->Show();
	}

	System::Void button3_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		if (!System::IO::File::Exists(msclr::interop::marshal_as<System::String^>(ORDER_FILE)))
		{
			ofstream file(ORDER_FILE, ios::binary);
			file.close();
		}

		CheckOrders form;
		form.Owner = this;
		this->Hide();
		form.ShowDialog();
	}

};
}
