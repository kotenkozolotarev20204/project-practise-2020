﻿#pragma once

#include <stdlib.h>
#include <msclr/marshal_cppstd.h>
#include "ShoppingCart.h"

namespace ComputersShop {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Сводка для Catalog
	/// </summary>
	public ref class UserBin : public System::Windows::Forms::Form
	{
	public:
		UserBin(void)
		{
			InitializeComponent();
			//
			//TODO: добавьте код конструктора
			//
		}

	protected:
		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		~UserBin()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:

	protected:

	private: System::Windows::Forms::Label^ label1;

private: System::Windows::Forms::Button^ button1;
private: System::Windows::Forms::PictureBox^ pictureBox4;
private: System::Windows::Forms::Label^ label81;
private: System::Windows::Forms::Label^ label80;
private: System::Windows::Forms::Label^ label79;
private: System::Windows::Forms::Label^ label78;
private: System::Windows::Forms::Label^ label77;
private: System::Windows::Forms::Label^ label76;
private: System::Windows::Forms::Label^ label75;
private: System::Windows::Forms::Label^ label74;
private: System::Windows::Forms::Label^ label73;
private: System::Windows::Forms::Label^ label72;
private: System::Windows::Forms::Label^ NameComp2;
private: System::Windows::Forms::Label^ CPU2;
private: System::Windows::Forms::Label^ Video2;
private: System::Windows::Forms::Label^ Motherboard2;
private: System::Windows::Forms::Label^ PSU2;
private: System::Windows::Forms::Label^ Price2;
private: System::Windows::Forms::Label^ RAM2;
private: System::Windows::Forms::Label^ HDD2;
private: System::Windows::Forms::Label^ SSD2;
private: System::Windows::Forms::Label^ Amount2;
	private: System::Windows::Forms::GroupBox^ groupBox3;


private: System::Windows::Forms::PictureBox^ pictureBox3;
private: System::Windows::Forms::Label^ label61;
private: System::Windows::Forms::Label^ label60;
private: System::Windows::Forms::Label^ label59;
private: System::Windows::Forms::Label^ label58;
private: System::Windows::Forms::Label^ label57;
private: System::Windows::Forms::Label^ label56;
private: System::Windows::Forms::Label^ label55;
private: System::Windows::Forms::Label^ label54;
private: System::Windows::Forms::Label^ label53;
private: System::Windows::Forms::Label^ label52;
private: System::Windows::Forms::Label^ NameComp3;
private: System::Windows::Forms::Label^ CPU3;
private: System::Windows::Forms::Label^ Video3;
private: System::Windows::Forms::Label^ Motherboard3;
private: System::Windows::Forms::Label^ PSU3;
private: System::Windows::Forms::Label^ Price3;
private: System::Windows::Forms::Label^ RAM3;
private: System::Windows::Forms::Label^ HDD3;
private: System::Windows::Forms::Label^ SSD3;
private: System::Windows::Forms::Label^ Amount3;
	private: System::Windows::Forms::GroupBox^ groupBox4;

private: System::Windows::Forms::PictureBox^ pictureBox1;
private: System::Windows::Forms::Label^ label2;
private: System::Windows::Forms::Label^ label3;
private: System::Windows::Forms::Label^ label4;
private: System::Windows::Forms::Label^ label5;
private: System::Windows::Forms::Label^ label6;
private: System::Windows::Forms::Label^ label7;
private: System::Windows::Forms::Label^ label8;
private: System::Windows::Forms::Label^ label9;
private: System::Windows::Forms::Label^ label19;
private: System::Windows::Forms::Label^ label15;
private: System::Windows::Forms::Label^ NameComp0;
private: System::Windows::Forms::Label^ CPU0;
private: System::Windows::Forms::Label^ Video0;
private: System::Windows::Forms::Label^ Motherboard0;
private: System::Windows::Forms::Label^ PSU0;
private: System::Windows::Forms::Label^ Price0;
private: System::Windows::Forms::Label^ RAM0;
private: System::Windows::Forms::Label^ HDD0;
private: System::Windows::Forms::Label^ SSD0;
private: System::Windows::Forms::Label^ Amount0;
private: System::Windows::Forms::GroupBox^ groupBox1;
private: System::Windows::Forms::PictureBox^ pictureBox2;
private: System::Windows::Forms::Label^ label41;
private: System::Windows::Forms::Label^ label40;
private: System::Windows::Forms::Label^ label39;
private: System::Windows::Forms::Label^ label38;
private: System::Windows::Forms::Label^ label37;
private: System::Windows::Forms::Label^ label36;
private: System::Windows::Forms::Label^ label35;
private: System::Windows::Forms::Label^ label34;
private: System::Windows::Forms::Label^ label33;
private: System::Windows::Forms::Label^ label32;
private: System::Windows::Forms::Label^ NameComp1;
private: System::Windows::Forms::Label^ CPU1;
private: System::Windows::Forms::Label^ Video1;
private: System::Windows::Forms::Label^ Motherboard1;
private: System::Windows::Forms::Label^ PSU1;
private: System::Windows::Forms::Label^ Price1;
private: System::Windows::Forms::Label^ RAM1;
private: System::Windows::Forms::Label^ HDD1;
private: System::Windows::Forms::Label^ SSD1;
private: System::Windows::Forms::Label^ Amount1;
private: System::Windows::Forms::GroupBox^ groupBox2;
private: System::Windows::Forms::LinkLabel^ linkLabel2;
private: System::Windows::Forms::LinkLabel^ linkLabel1;
private: System::Windows::Forms::Label^ label12;
private: System::Windows::Forms::NumericUpDown^ numericUpDown3;
private: System::Windows::Forms::Label^ label13;
private: System::Windows::Forms::NumericUpDown^ numericUpDown4;
private: System::Windows::Forms::Label^ label10;
private: System::Windows::Forms::NumericUpDown^ numericUpDown1;
private: System::Windows::Forms::Label^ label11;
private: System::Windows::Forms::NumericUpDown^ numericUpDown2;
private: System::Windows::Forms::LinkLabel^ linkLabel3;

private:

private:

	protected:


	private:
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(UserBin::typeid));
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->pictureBox4 = (gcnew System::Windows::Forms::PictureBox());
			this->label81 = (gcnew System::Windows::Forms::Label());
			this->label80 = (gcnew System::Windows::Forms::Label());
			this->label79 = (gcnew System::Windows::Forms::Label());
			this->label78 = (gcnew System::Windows::Forms::Label());
			this->label77 = (gcnew System::Windows::Forms::Label());
			this->label76 = (gcnew System::Windows::Forms::Label());
			this->label75 = (gcnew System::Windows::Forms::Label());
			this->label74 = (gcnew System::Windows::Forms::Label());
			this->label73 = (gcnew System::Windows::Forms::Label());
			this->label72 = (gcnew System::Windows::Forms::Label());
			this->NameComp2 = (gcnew System::Windows::Forms::Label());
			this->CPU2 = (gcnew System::Windows::Forms::Label());
			this->Video2 = (gcnew System::Windows::Forms::Label());
			this->Motherboard2 = (gcnew System::Windows::Forms::Label());
			this->PSU2 = (gcnew System::Windows::Forms::Label());
			this->Price2 = (gcnew System::Windows::Forms::Label());
			this->RAM2 = (gcnew System::Windows::Forms::Label());
			this->HDD2 = (gcnew System::Windows::Forms::Label());
			this->SSD2 = (gcnew System::Windows::Forms::Label());
			this->Amount2 = (gcnew System::Windows::Forms::Label());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->numericUpDown3 = (gcnew System::Windows::Forms::NumericUpDown());
			this->pictureBox3 = (gcnew System::Windows::Forms::PictureBox());
			this->label61 = (gcnew System::Windows::Forms::Label());
			this->label60 = (gcnew System::Windows::Forms::Label());
			this->label59 = (gcnew System::Windows::Forms::Label());
			this->label58 = (gcnew System::Windows::Forms::Label());
			this->label57 = (gcnew System::Windows::Forms::Label());
			this->label56 = (gcnew System::Windows::Forms::Label());
			this->label55 = (gcnew System::Windows::Forms::Label());
			this->label54 = (gcnew System::Windows::Forms::Label());
			this->label53 = (gcnew System::Windows::Forms::Label());
			this->label52 = (gcnew System::Windows::Forms::Label());
			this->NameComp3 = (gcnew System::Windows::Forms::Label());
			this->CPU3 = (gcnew System::Windows::Forms::Label());
			this->Video3 = (gcnew System::Windows::Forms::Label());
			this->Motherboard3 = (gcnew System::Windows::Forms::Label());
			this->PSU3 = (gcnew System::Windows::Forms::Label());
			this->Price3 = (gcnew System::Windows::Forms::Label());
			this->RAM3 = (gcnew System::Windows::Forms::Label());
			this->HDD3 = (gcnew System::Windows::Forms::Label());
			this->SSD3 = (gcnew System::Windows::Forms::Label());
			this->Amount3 = (gcnew System::Windows::Forms::Label());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->numericUpDown4 = (gcnew System::Windows::Forms::NumericUpDown());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->NameComp0 = (gcnew System::Windows::Forms::Label());
			this->CPU0 = (gcnew System::Windows::Forms::Label());
			this->Video0 = (gcnew System::Windows::Forms::Label());
			this->Motherboard0 = (gcnew System::Windows::Forms::Label());
			this->PSU0 = (gcnew System::Windows::Forms::Label());
			this->Price0 = (gcnew System::Windows::Forms::Label());
			this->RAM0 = (gcnew System::Windows::Forms::Label());
			this->HDD0 = (gcnew System::Windows::Forms::Label());
			this->SSD0 = (gcnew System::Windows::Forms::Label());
			this->Amount0 = (gcnew System::Windows::Forms::Label());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->numericUpDown1 = (gcnew System::Windows::Forms::NumericUpDown());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->label41 = (gcnew System::Windows::Forms::Label());
			this->label40 = (gcnew System::Windows::Forms::Label());
			this->label39 = (gcnew System::Windows::Forms::Label());
			this->label38 = (gcnew System::Windows::Forms::Label());
			this->label37 = (gcnew System::Windows::Forms::Label());
			this->label36 = (gcnew System::Windows::Forms::Label());
			this->label35 = (gcnew System::Windows::Forms::Label());
			this->label34 = (gcnew System::Windows::Forms::Label());
			this->label33 = (gcnew System::Windows::Forms::Label());
			this->label32 = (gcnew System::Windows::Forms::Label());
			this->NameComp1 = (gcnew System::Windows::Forms::Label());
			this->CPU1 = (gcnew System::Windows::Forms::Label());
			this->Video1 = (gcnew System::Windows::Forms::Label());
			this->Motherboard1 = (gcnew System::Windows::Forms::Label());
			this->PSU1 = (gcnew System::Windows::Forms::Label());
			this->Price1 = (gcnew System::Windows::Forms::Label());
			this->RAM1 = (gcnew System::Windows::Forms::Label());
			this->HDD1 = (gcnew System::Windows::Forms::Label());
			this->SSD1 = (gcnew System::Windows::Forms::Label());
			this->Amount1 = (gcnew System::Windows::Forms::Label());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->numericUpDown2 = (gcnew System::Windows::Forms::NumericUpDown());
			this->linkLabel2 = (gcnew System::Windows::Forms::LinkLabel());
			this->linkLabel1 = (gcnew System::Windows::Forms::LinkLabel());
			this->linkLabel3 = (gcnew System::Windows::Forms::LinkLabel());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->BeginInit();
			this->groupBox3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->BeginInit();
			this->groupBox4->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->groupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			this->groupBox2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown2))->BeginInit();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 20, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label1->Location = System::Drawing::Point(19, 9);
			this->label1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(121, 31);
			this->label1->TabIndex = 30;
			this->label1->Text = L"Корзина";
			// 
			// button1
			// 
			this->button1->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->button1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->button1->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button1->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button1->Location = System::Drawing::Point(649, 8);
			this->button1->Margin = System::Windows::Forms::Padding(2);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(143, 36);
			this->button1->TabIndex = 44;
			this->button1->Text = L"Оформить заказ";
			this->button1->UseVisualStyleBackColor = false;
			this->button1->Click += gcnew System::EventHandler(this, &UserBin::button1_Click);
			// 
			// pictureBox4
			// 
			this->pictureBox4->Location = System::Drawing::Point(15, 16);
			this->pictureBox4->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox4->Name = L"pictureBox4";
			this->pictureBox4->Size = System::Drawing::Size(180, 195);
			this->pictureBox4->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox4->TabIndex = 0;
			this->pictureBox4->TabStop = false;
			// 
			// label81
			// 
			this->label81->AutoSize = true;
			this->label81->Location = System::Drawing::Point(217, 48);
			this->label81->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label81->Name = L"label81";
			this->label81->Size = System::Drawing::Size(60, 13);
			this->label81->TabIndex = 1;
			this->label81->Text = L"Название:";
			// 
			// label80
			// 
			this->label80->AutoSize = true;
			this->label80->Location = System::Drawing::Point(218, 79);
			this->label80->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label80->Name = L"label80";
			this->label80->Size = System::Drawing::Size(66, 13);
			this->label80->TabIndex = 3;
			this->label80->Text = L"Процессор:";
			// 
			// label79
			// 
			this->label79->AutoSize = true;
			this->label79->Location = System::Drawing::Point(218, 108);
			this->label79->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label79->Name = L"label79";
			this->label79->Size = System::Drawing::Size(70, 13);
			this->label79->TabIndex = 5;
			this->label79->Text = L"Видеокарта:";
			// 
			// label78
			// 
			this->label78->AutoSize = true;
			this->label78->Location = System::Drawing::Point(525, 76);
			this->label78->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label78->Name = L"label78";
			this->label78->Size = System::Drawing::Size(71, 13);
			this->label78->TabIndex = 7;
			this->label78->Text = L"Объём ОЗУ:";
			// 
			// label77
			// 
			this->label77->AutoSize = true;
			this->label77->Location = System::Drawing::Point(218, 136);
			this->label77->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label77->Name = L"label77";
			this->label77->Size = System::Drawing::Size(110, 13);
			this->label77->TabIndex = 9;
			this->label77->Text = L"Материнская плата:";
			// 
			// label76
			// 
			this->label76->AutoSize = true;
			this->label76->Location = System::Drawing::Point(525, 105);
			this->label76->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label76->Name = L"label76";
			this->label76->Size = System::Drawing::Size(72, 13);
			this->label76->TabIndex = 11;
			this->label76->Text = L"Объём HDD:";
			// 
			// label75
			// 
			this->label75->AutoSize = true;
			this->label75->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label75->Location = System::Drawing::Point(525, 48);
			this->label75->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label75->Name = L"label75";
			this->label75->Size = System::Drawing::Size(36, 13);
			this->label75->TabIndex = 13;
			this->label75->Text = L"Цена:";
			// 
			// label74
			// 
			this->label74->AutoSize = true;
			this->label74->Location = System::Drawing::Point(525, 161);
			this->label74->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label74->Name = L"label74";
			this->label74->Size = System::Drawing::Size(69, 13);
			this->label74->TabIndex = 16;
			this->label74->Text = L"Количество:";
			// 
			// label73
			// 
			this->label73->AutoSize = true;
			this->label73->Location = System::Drawing::Point(218, 164);
			this->label73->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label73->Name = L"label73";
			this->label73->Size = System::Drawing::Size(81, 13);
			this->label73->TabIndex = 23;
			this->label73->Text = L"Мощность БП:";
			// 
			// label72
			// 
			this->label72->AutoSize = true;
			this->label72->Location = System::Drawing::Point(525, 133);
			this->label72->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label72->Name = L"label72";
			this->label72->Size = System::Drawing::Size(70, 13);
			this->label72->TabIndex = 25;
			this->label72->Text = L"Объём SSD:";
			// 
			// NameComp2
			// 
			this->NameComp2->AutoSize = true;
			this->NameComp2->Location = System::Drawing::Point(329, 48);
			this->NameComp2->Name = L"NameComp2";
			this->NameComp2->Size = System::Drawing::Size(41, 13);
			this->NameComp2->TabIndex = 43;
			this->NameComp2->Text = L"label71";
			// 
			// CPU2
			// 
			this->CPU2->AutoSize = true;
			this->CPU2->Location = System::Drawing::Point(329, 79);
			this->CPU2->Name = L"CPU2";
			this->CPU2->Size = System::Drawing::Size(41, 13);
			this->CPU2->TabIndex = 44;
			this->CPU2->Text = L"label70";
			// 
			// Video2
			// 
			this->Video2->AutoSize = true;
			this->Video2->Location = System::Drawing::Point(329, 108);
			this->Video2->Name = L"Video2";
			this->Video2->Size = System::Drawing::Size(41, 13);
			this->Video2->TabIndex = 45;
			this->Video2->Text = L"label69";
			// 
			// Motherboard2
			// 
			this->Motherboard2->AutoSize = true;
			this->Motherboard2->Location = System::Drawing::Point(329, 136);
			this->Motherboard2->Name = L"Motherboard2";
			this->Motherboard2->Size = System::Drawing::Size(41, 13);
			this->Motherboard2->TabIndex = 46;
			this->Motherboard2->Text = L"label68";
			// 
			// PSU2
			// 
			this->PSU2->AutoSize = true;
			this->PSU2->Location = System::Drawing::Point(329, 164);
			this->PSU2->Name = L"PSU2";
			this->PSU2->Size = System::Drawing::Size(41, 13);
			this->PSU2->TabIndex = 47;
			this->PSU2->Text = L"label67";
			// 
			// Price2
			// 
			this->Price2->AutoSize = true;
			this->Price2->Location = System::Drawing::Point(652, 48);
			this->Price2->Name = L"Price2";
			this->Price2->Size = System::Drawing::Size(41, 13);
			this->Price2->TabIndex = 48;
			this->Price2->Text = L"label66";
			// 
			// RAM2
			// 
			this->RAM2->AutoSize = true;
			this->RAM2->Location = System::Drawing::Point(652, 76);
			this->RAM2->Name = L"RAM2";
			this->RAM2->Size = System::Drawing::Size(41, 13);
			this->RAM2->TabIndex = 49;
			this->RAM2->Text = L"label65";
			// 
			// HDD2
			// 
			this->HDD2->AutoSize = true;
			this->HDD2->Location = System::Drawing::Point(652, 105);
			this->HDD2->Name = L"HDD2";
			this->HDD2->Size = System::Drawing::Size(41, 13);
			this->HDD2->TabIndex = 50;
			this->HDD2->Text = L"label64";
			// 
			// SSD2
			// 
			this->SSD2->AutoSize = true;
			this->SSD2->Location = System::Drawing::Point(652, 133);
			this->SSD2->Name = L"SSD2";
			this->SSD2->Size = System::Drawing::Size(41, 13);
			this->SSD2->TabIndex = 51;
			this->SSD2->Text = L"label63";
			// 
			// Amount2
			// 
			this->Amount2->AutoSize = true;
			this->Amount2->Location = System::Drawing::Point(652, 161);
			this->Amount2->Name = L"Amount2";
			this->Amount2->Size = System::Drawing::Size(41, 13);
			this->Amount2->TabIndex = 52;
			this->Amount2->Text = L"label62";
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->label12);
			this->groupBox3->Controls->Add(this->Amount2);
			this->groupBox3->Controls->Add(this->numericUpDown3);
			this->groupBox3->Controls->Add(this->SSD2);
			this->groupBox3->Controls->Add(this->HDD2);
			this->groupBox3->Controls->Add(this->RAM2);
			this->groupBox3->Controls->Add(this->Price2);
			this->groupBox3->Controls->Add(this->PSU2);
			this->groupBox3->Controls->Add(this->Motherboard2);
			this->groupBox3->Controls->Add(this->Video2);
			this->groupBox3->Controls->Add(this->CPU2);
			this->groupBox3->Controls->Add(this->NameComp2);
			this->groupBox3->Controls->Add(this->label72);
			this->groupBox3->Controls->Add(this->label73);
			this->groupBox3->Controls->Add(this->label74);
			this->groupBox3->Controls->Add(this->label75);
			this->groupBox3->Controls->Add(this->label76);
			this->groupBox3->Controls->Add(this->label77);
			this->groupBox3->Controls->Add(this->label78);
			this->groupBox3->Controls->Add(this->label79);
			this->groupBox3->Controls->Add(this->label80);
			this->groupBox3->Controls->Add(this->label81);
			this->groupBox3->Controls->Add(this->pictureBox4);
			this->groupBox3->Location = System::Drawing::Point(11, 498);
			this->groupBox3->Margin = System::Windows::Forms::Padding(2);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Padding = System::Windows::Forms::Padding(2);
			this->groupBox3->Size = System::Drawing::Size(782, 222);
			this->groupBox3->TabIndex = 45;
			this->groupBox3->TabStop = false;
			this->groupBox3->Visible = false;
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(524, 193);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(75, 13);
			this->label12->TabIndex = 58;
			this->label12->Text = L"Для покупки:";
			// 
			// numericUpDown3
			// 
			this->numericUpDown3->Location = System::Drawing::Point(648, 191);
			this->numericUpDown3->Name = L"numericUpDown3";
			this->numericUpDown3->Size = System::Drawing::Size(52, 20);
			this->numericUpDown3->TabIndex = 57;
			this->numericUpDown3->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->numericUpDown3->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
			this->numericUpDown3->ValueChanged += gcnew System::EventHandler(this, &UserBin::numericUpDown_ValueChanged);
			// 
			// pictureBox3
			// 
			this->pictureBox3->Location = System::Drawing::Point(15, 16);
			this->pictureBox3->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox3->Name = L"pictureBox3";
			this->pictureBox3->Size = System::Drawing::Size(180, 195);
			this->pictureBox3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox3->TabIndex = 0;
			this->pictureBox3->TabStop = false;
			// 
			// label61
			// 
			this->label61->AutoSize = true;
			this->label61->Location = System::Drawing::Point(217, 46);
			this->label61->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label61->Name = L"label61";
			this->label61->Size = System::Drawing::Size(60, 13);
			this->label61->TabIndex = 53;
			this->label61->Text = L"Название:";
			// 
			// label60
			// 
			this->label60->AutoSize = true;
			this->label60->Location = System::Drawing::Point(218, 77);
			this->label60->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label60->Name = L"label60";
			this->label60->Size = System::Drawing::Size(66, 13);
			this->label60->TabIndex = 54;
			this->label60->Text = L"Процессор:";
			// 
			// label59
			// 
			this->label59->AutoSize = true;
			this->label59->Location = System::Drawing::Point(218, 106);
			this->label59->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label59->Name = L"label59";
			this->label59->Size = System::Drawing::Size(70, 13);
			this->label59->TabIndex = 55;
			this->label59->Text = L"Видеокарта:";
			// 
			// label58
			// 
			this->label58->AutoSize = true;
			this->label58->Location = System::Drawing::Point(525, 74);
			this->label58->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label58->Name = L"label58";
			this->label58->Size = System::Drawing::Size(71, 13);
			this->label58->TabIndex = 56;
			this->label58->Text = L"Объём ОЗУ:";
			// 
			// label57
			// 
			this->label57->AutoSize = true;
			this->label57->Location = System::Drawing::Point(218, 134);
			this->label57->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label57->Name = L"label57";
			this->label57->Size = System::Drawing::Size(110, 13);
			this->label57->TabIndex = 57;
			this->label57->Text = L"Материнская плата:";
			// 
			// label56
			// 
			this->label56->AutoSize = true;
			this->label56->Location = System::Drawing::Point(525, 103);
			this->label56->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label56->Name = L"label56";
			this->label56->Size = System::Drawing::Size(72, 13);
			this->label56->TabIndex = 58;
			this->label56->Text = L"Объём HDD:";
			// 
			// label55
			// 
			this->label55->AutoSize = true;
			this->label55->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label55->Location = System::Drawing::Point(525, 46);
			this->label55->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label55->Name = L"label55";
			this->label55->Size = System::Drawing::Size(36, 13);
			this->label55->TabIndex = 59;
			this->label55->Text = L"Цена:";
			// 
			// label54
			// 
			this->label54->AutoSize = true;
			this->label54->Location = System::Drawing::Point(525, 159);
			this->label54->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label54->Name = L"label54";
			this->label54->Size = System::Drawing::Size(69, 13);
			this->label54->TabIndex = 60;
			this->label54->Text = L"Количество:";
			// 
			// label53
			// 
			this->label53->AutoSize = true;
			this->label53->Location = System::Drawing::Point(218, 162);
			this->label53->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label53->Name = L"label53";
			this->label53->Size = System::Drawing::Size(81, 13);
			this->label53->TabIndex = 61;
			this->label53->Text = L"Мощность БП:";
			// 
			// label52
			// 
			this->label52->AutoSize = true;
			this->label52->Location = System::Drawing::Point(525, 131);
			this->label52->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label52->Name = L"label52";
			this->label52->Size = System::Drawing::Size(70, 13);
			this->label52->TabIndex = 62;
			this->label52->Text = L"Объём SSD:";
			// 
			// NameComp3
			// 
			this->NameComp3->AutoSize = true;
			this->NameComp3->Location = System::Drawing::Point(329, 46);
			this->NameComp3->Name = L"NameComp3";
			this->NameComp3->Size = System::Drawing::Size(41, 13);
			this->NameComp3->TabIndex = 63;
			this->NameComp3->Text = L"label51";
			// 
			// CPU3
			// 
			this->CPU3->AutoSize = true;
			this->CPU3->Location = System::Drawing::Point(329, 77);
			this->CPU3->Name = L"CPU3";
			this->CPU3->Size = System::Drawing::Size(41, 13);
			this->CPU3->TabIndex = 64;
			this->CPU3->Text = L"label50";
			// 
			// Video3
			// 
			this->Video3->AutoSize = true;
			this->Video3->Location = System::Drawing::Point(329, 106);
			this->Video3->Name = L"Video3";
			this->Video3->Size = System::Drawing::Size(41, 13);
			this->Video3->TabIndex = 65;
			this->Video3->Text = L"label49";
			// 
			// Motherboard3
			// 
			this->Motherboard3->AutoSize = true;
			this->Motherboard3->Location = System::Drawing::Point(329, 134);
			this->Motherboard3->Name = L"Motherboard3";
			this->Motherboard3->Size = System::Drawing::Size(41, 13);
			this->Motherboard3->TabIndex = 66;
			this->Motherboard3->Text = L"label48";
			// 
			// PSU3
			// 
			this->PSU3->AutoSize = true;
			this->PSU3->Location = System::Drawing::Point(329, 162);
			this->PSU3->Name = L"PSU3";
			this->PSU3->Size = System::Drawing::Size(41, 13);
			this->PSU3->TabIndex = 67;
			this->PSU3->Text = L"label47";
			// 
			// Price3
			// 
			this->Price3->AutoSize = true;
			this->Price3->Location = System::Drawing::Point(652, 46);
			this->Price3->Name = L"Price3";
			this->Price3->Size = System::Drawing::Size(41, 13);
			this->Price3->TabIndex = 68;
			this->Price3->Text = L"label46";
			// 
			// RAM3
			// 
			this->RAM3->AutoSize = true;
			this->RAM3->Location = System::Drawing::Point(652, 74);
			this->RAM3->Name = L"RAM3";
			this->RAM3->Size = System::Drawing::Size(41, 13);
			this->RAM3->TabIndex = 69;
			this->RAM3->Text = L"label45";
			// 
			// HDD3
			// 
			this->HDD3->AutoSize = true;
			this->HDD3->Location = System::Drawing::Point(652, 103);
			this->HDD3->Name = L"HDD3";
			this->HDD3->Size = System::Drawing::Size(41, 13);
			this->HDD3->TabIndex = 70;
			this->HDD3->Text = L"label44";
			// 
			// SSD3
			// 
			this->SSD3->AutoSize = true;
			this->SSD3->Location = System::Drawing::Point(652, 131);
			this->SSD3->Name = L"SSD3";
			this->SSD3->Size = System::Drawing::Size(41, 13);
			this->SSD3->TabIndex = 71;
			this->SSD3->Text = L"label43";
			// 
			// Amount3
			// 
			this->Amount3->AutoSize = true;
			this->Amount3->Location = System::Drawing::Point(652, 159);
			this->Amount3->Name = L"Amount3";
			this->Amount3->Size = System::Drawing::Size(41, 13);
			this->Amount3->TabIndex = 72;
			this->Amount3->Text = L"label42";
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->label13);
			this->groupBox4->Controls->Add(this->Amount3);
			this->groupBox4->Controls->Add(this->numericUpDown4);
			this->groupBox4->Controls->Add(this->SSD3);
			this->groupBox4->Controls->Add(this->HDD3);
			this->groupBox4->Controls->Add(this->RAM3);
			this->groupBox4->Controls->Add(this->Price3);
			this->groupBox4->Controls->Add(this->PSU3);
			this->groupBox4->Controls->Add(this->Motherboard3);
			this->groupBox4->Controls->Add(this->Video3);
			this->groupBox4->Controls->Add(this->CPU3);
			this->groupBox4->Controls->Add(this->NameComp3);
			this->groupBox4->Controls->Add(this->label52);
			this->groupBox4->Controls->Add(this->label53);
			this->groupBox4->Controls->Add(this->label54);
			this->groupBox4->Controls->Add(this->label55);
			this->groupBox4->Controls->Add(this->label56);
			this->groupBox4->Controls->Add(this->label57);
			this->groupBox4->Controls->Add(this->label58);
			this->groupBox4->Controls->Add(this->label59);
			this->groupBox4->Controls->Add(this->label60);
			this->groupBox4->Controls->Add(this->label61);
			this->groupBox4->Controls->Add(this->pictureBox3);
			this->groupBox4->Location = System::Drawing::Point(11, 724);
			this->groupBox4->Margin = System::Windows::Forms::Padding(2);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Padding = System::Windows::Forms::Padding(2);
			this->groupBox4->Size = System::Drawing::Size(782, 222);
			this->groupBox4->TabIndex = 46;
			this->groupBox4->TabStop = false;
			this->groupBox4->Visible = false;
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(524, 193);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(75, 13);
			this->label13->TabIndex = 58;
			this->label13->Text = L"Для покупки:";
			// 
			// numericUpDown4
			// 
			this->numericUpDown4->Location = System::Drawing::Point(648, 191);
			this->numericUpDown4->Name = L"numericUpDown4";
			this->numericUpDown4->Size = System::Drawing::Size(52, 20);
			this->numericUpDown4->TabIndex = 57;
			this->numericUpDown4->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->numericUpDown4->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
			this->numericUpDown4->ValueChanged += gcnew System::EventHandler(this, &UserBin::numericUpDown_ValueChanged);
			// 
			// pictureBox1
			// 
			this->pictureBox1->Location = System::Drawing::Point(15, 16);
			this->pictureBox1->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(180, 195);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(217, 48);
			this->label2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(60, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Название:";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(218, 79);
			this->label3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(66, 13);
			this->label3->TabIndex = 3;
			this->label3->Text = L"Процессор:";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(218, 108);
			this->label4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(70, 13);
			this->label4->TabIndex = 5;
			this->label4->Text = L"Видеокарта:";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(525, 76);
			this->label5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(71, 13);
			this->label5->TabIndex = 7;
			this->label5->Text = L"Объём ОЗУ:";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(218, 136);
			this->label6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(110, 13);
			this->label6->TabIndex = 9;
			this->label6->Text = L"Материнская плата:";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(525, 105);
			this->label7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(72, 13);
			this->label7->TabIndex = 11;
			this->label7->Text = L"Объём HDD:";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label8->Location = System::Drawing::Point(525, 48);
			this->label8->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(36, 13);
			this->label8->TabIndex = 13;
			this->label8->Text = L"Цена:";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(525, 161);
			this->label9->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(69, 13);
			this->label9->TabIndex = 16;
			this->label9->Text = L"Количество:";
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(218, 164);
			this->label19->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(81, 13);
			this->label19->TabIndex = 23;
			this->label19->Text = L"Мощность БП:";
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(525, 133);
			this->label15->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(70, 13);
			this->label15->TabIndex = 25;
			this->label15->Text = L"Объём SSD:";
			// 
			// NameComp0
			// 
			this->NameComp0->AutoSize = true;
			this->NameComp0->Location = System::Drawing::Point(329, 48);
			this->NameComp0->Name = L"NameComp0";
			this->NameComp0->Size = System::Drawing::Size(41, 13);
			this->NameComp0->TabIndex = 43;
			this->NameComp0->Text = L"label22";
			// 
			// CPU0
			// 
			this->CPU0->AutoSize = true;
			this->CPU0->Location = System::Drawing::Point(329, 79);
			this->CPU0->Name = L"CPU0";
			this->CPU0->Size = System::Drawing::Size(41, 13);
			this->CPU0->TabIndex = 44;
			this->CPU0->Text = L"label23";
			// 
			// Video0
			// 
			this->Video0->AutoSize = true;
			this->Video0->Location = System::Drawing::Point(329, 108);
			this->Video0->Name = L"Video0";
			this->Video0->Size = System::Drawing::Size(41, 13);
			this->Video0->TabIndex = 45;
			this->Video0->Text = L"label24";
			// 
			// Motherboard0
			// 
			this->Motherboard0->AutoSize = true;
			this->Motherboard0->Location = System::Drawing::Point(329, 136);
			this->Motherboard0->Name = L"Motherboard0";
			this->Motherboard0->Size = System::Drawing::Size(41, 13);
			this->Motherboard0->TabIndex = 46;
			this->Motherboard0->Text = L"label25";
			// 
			// PSU0
			// 
			this->PSU0->AutoSize = true;
			this->PSU0->Location = System::Drawing::Point(329, 164);
			this->PSU0->Name = L"PSU0";
			this->PSU0->Size = System::Drawing::Size(41, 13);
			this->PSU0->TabIndex = 47;
			this->PSU0->Text = L"label26";
			// 
			// Price0
			// 
			this->Price0->AutoSize = true;
			this->Price0->Location = System::Drawing::Point(652, 48);
			this->Price0->Name = L"Price0";
			this->Price0->Size = System::Drawing::Size(41, 13);
			this->Price0->TabIndex = 48;
			this->Price0->Text = L"label27";
			// 
			// RAM0
			// 
			this->RAM0->AutoSize = true;
			this->RAM0->Location = System::Drawing::Point(652, 76);
			this->RAM0->Name = L"RAM0";
			this->RAM0->Size = System::Drawing::Size(41, 13);
			this->RAM0->TabIndex = 49;
			this->RAM0->Text = L"label28";
			// 
			// HDD0
			// 
			this->HDD0->AutoSize = true;
			this->HDD0->Location = System::Drawing::Point(652, 105);
			this->HDD0->Name = L"HDD0";
			this->HDD0->Size = System::Drawing::Size(41, 13);
			this->HDD0->TabIndex = 50;
			this->HDD0->Text = L"label29";
			// 
			// SSD0
			// 
			this->SSD0->AutoSize = true;
			this->SSD0->Location = System::Drawing::Point(652, 133);
			this->SSD0->Name = L"SSD0";
			this->SSD0->Size = System::Drawing::Size(41, 13);
			this->SSD0->TabIndex = 51;
			this->SSD0->Text = L"label30";
			// 
			// Amount0
			// 
			this->Amount0->AutoSize = true;
			this->Amount0->Location = System::Drawing::Point(652, 161);
			this->Amount0->Name = L"Amount0";
			this->Amount0->Size = System::Drawing::Size(41, 13);
			this->Amount0->TabIndex = 52;
			this->Amount0->Text = L"label31";
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->label10);
			this->groupBox1->Controls->Add(this->numericUpDown1);
			this->groupBox1->Controls->Add(this->Amount0);
			this->groupBox1->Controls->Add(this->SSD0);
			this->groupBox1->Controls->Add(this->HDD0);
			this->groupBox1->Controls->Add(this->RAM0);
			this->groupBox1->Controls->Add(this->Price0);
			this->groupBox1->Controls->Add(this->PSU0);
			this->groupBox1->Controls->Add(this->Motherboard0);
			this->groupBox1->Controls->Add(this->Video0);
			this->groupBox1->Controls->Add(this->CPU0);
			this->groupBox1->Controls->Add(this->NameComp0);
			this->groupBox1->Controls->Add(this->label15);
			this->groupBox1->Controls->Add(this->label19);
			this->groupBox1->Controls->Add(this->label9);
			this->groupBox1->Controls->Add(this->label8);
			this->groupBox1->Controls->Add(this->label7);
			this->groupBox1->Controls->Add(this->label6);
			this->groupBox1->Controls->Add(this->label5);
			this->groupBox1->Controls->Add(this->label4);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->pictureBox1);
			this->groupBox1->Location = System::Drawing::Point(10, 46);
			this->groupBox1->Margin = System::Windows::Forms::Padding(2);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Padding = System::Windows::Forms::Padding(2);
			this->groupBox1->Size = System::Drawing::Size(782, 222);
			this->groupBox1->TabIndex = 28;
			this->groupBox1->TabStop = false;
			this->groupBox1->Visible = false;
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(525, 193);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(75, 13);
			this->label10->TabIndex = 56;
			this->label10->Text = L"Для покупки:";
			// 
			// numericUpDown1
			// 
			this->numericUpDown1->Location = System::Drawing::Point(649, 191);
			this->numericUpDown1->Name = L"numericUpDown1";
			this->numericUpDown1->Size = System::Drawing::Size(52, 20);
			this->numericUpDown1->TabIndex = 55;
			this->numericUpDown1->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->numericUpDown1->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
			this->numericUpDown1->ValueChanged += gcnew System::EventHandler(this, &UserBin::numericUpDown_ValueChanged);
			// 
			// pictureBox2
			// 
			this->pictureBox2->Location = System::Drawing::Point(15, 16);
			this->pictureBox2->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(180, 195);
			this->pictureBox2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox2->TabIndex = 0;
			this->pictureBox2->TabStop = false;
			// 
			// label41
			// 
			this->label41->AutoSize = true;
			this->label41->Location = System::Drawing::Point(217, 46);
			this->label41->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label41->Name = L"label41";
			this->label41->Size = System::Drawing::Size(60, 13);
			this->label41->TabIndex = 53;
			this->label41->Text = L"Название:";
			// 
			// label40
			// 
			this->label40->AutoSize = true;
			this->label40->Location = System::Drawing::Point(218, 77);
			this->label40->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label40->Name = L"label40";
			this->label40->Size = System::Drawing::Size(66, 13);
			this->label40->TabIndex = 54;
			this->label40->Text = L"Процессор:";
			// 
			// label39
			// 
			this->label39->AutoSize = true;
			this->label39->Location = System::Drawing::Point(218, 106);
			this->label39->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label39->Name = L"label39";
			this->label39->Size = System::Drawing::Size(70, 13);
			this->label39->TabIndex = 55;
			this->label39->Text = L"Видеокарта:";
			// 
			// label38
			// 
			this->label38->AutoSize = true;
			this->label38->Location = System::Drawing::Point(525, 74);
			this->label38->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label38->Name = L"label38";
			this->label38->Size = System::Drawing::Size(71, 13);
			this->label38->TabIndex = 56;
			this->label38->Text = L"Объём ОЗУ:";
			// 
			// label37
			// 
			this->label37->AutoSize = true;
			this->label37->Location = System::Drawing::Point(218, 134);
			this->label37->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label37->Name = L"label37";
			this->label37->Size = System::Drawing::Size(110, 13);
			this->label37->TabIndex = 57;
			this->label37->Text = L"Материнская плата:";
			// 
			// label36
			// 
			this->label36->AutoSize = true;
			this->label36->Location = System::Drawing::Point(525, 103);
			this->label36->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label36->Name = L"label36";
			this->label36->Size = System::Drawing::Size(72, 13);
			this->label36->TabIndex = 58;
			this->label36->Text = L"Объём HDD:";
			// 
			// label35
			// 
			this->label35->AutoSize = true;
			this->label35->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label35->Location = System::Drawing::Point(525, 46);
			this->label35->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label35->Name = L"label35";
			this->label35->Size = System::Drawing::Size(36, 13);
			this->label35->TabIndex = 59;
			this->label35->Text = L"Цена:";
			// 
			// label34
			// 
			this->label34->AutoSize = true;
			this->label34->Location = System::Drawing::Point(525, 159);
			this->label34->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label34->Name = L"label34";
			this->label34->Size = System::Drawing::Size(69, 13);
			this->label34->TabIndex = 60;
			this->label34->Text = L"Количество:";
			// 
			// label33
			// 
			this->label33->AutoSize = true;
			this->label33->Location = System::Drawing::Point(218, 162);
			this->label33->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label33->Name = L"label33";
			this->label33->Size = System::Drawing::Size(81, 13);
			this->label33->TabIndex = 61;
			this->label33->Text = L"Мощность БП:";
			// 
			// label32
			// 
			this->label32->AutoSize = true;
			this->label32->Location = System::Drawing::Point(525, 131);
			this->label32->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label32->Name = L"label32";
			this->label32->Size = System::Drawing::Size(70, 13);
			this->label32->TabIndex = 62;
			this->label32->Text = L"Объём SSD:";
			// 
			// NameComp1
			// 
			this->NameComp1->AutoSize = true;
			this->NameComp1->Location = System::Drawing::Point(329, 46);
			this->NameComp1->Name = L"NameComp1";
			this->NameComp1->Size = System::Drawing::Size(41, 13);
			this->NameComp1->TabIndex = 63;
			this->NameComp1->Text = L"label21";
			// 
			// CPU1
			// 
			this->CPU1->AutoSize = true;
			this->CPU1->Location = System::Drawing::Point(329, 77);
			this->CPU1->Name = L"CPU1";
			this->CPU1->Size = System::Drawing::Size(41, 13);
			this->CPU1->TabIndex = 64;
			this->CPU1->Text = L"label20";
			// 
			// Video1
			// 
			this->Video1->AutoSize = true;
			this->Video1->Location = System::Drawing::Point(329, 106);
			this->Video1->Name = L"Video1";
			this->Video1->Size = System::Drawing::Size(41, 13);
			this->Video1->TabIndex = 65;
			this->Video1->Text = L"label18";
			// 
			// Motherboard1
			// 
			this->Motherboard1->AutoSize = true;
			this->Motherboard1->Location = System::Drawing::Point(329, 134);
			this->Motherboard1->Name = L"Motherboard1";
			this->Motherboard1->Size = System::Drawing::Size(41, 13);
			this->Motherboard1->TabIndex = 66;
			this->Motherboard1->Text = L"label17";
			// 
			// PSU1
			// 
			this->PSU1->AutoSize = true;
			this->PSU1->Location = System::Drawing::Point(329, 162);
			this->PSU1->Name = L"PSU1";
			this->PSU1->Size = System::Drawing::Size(41, 13);
			this->PSU1->TabIndex = 67;
			this->PSU1->Text = L"label16";
			// 
			// Price1
			// 
			this->Price1->AutoSize = true;
			this->Price1->Location = System::Drawing::Point(652, 46);
			this->Price1->Name = L"Price1";
			this->Price1->Size = System::Drawing::Size(41, 13);
			this->Price1->TabIndex = 68;
			this->Price1->Text = L"label14";
			// 
			// RAM1
			// 
			this->RAM1->AutoSize = true;
			this->RAM1->Location = System::Drawing::Point(652, 74);
			this->RAM1->Name = L"RAM1";
			this->RAM1->Size = System::Drawing::Size(41, 13);
			this->RAM1->TabIndex = 69;
			this->RAM1->Text = L"label13";
			// 
			// HDD1
			// 
			this->HDD1->AutoSize = true;
			this->HDD1->Location = System::Drawing::Point(652, 103);
			this->HDD1->Name = L"HDD1";
			this->HDD1->Size = System::Drawing::Size(41, 13);
			this->HDD1->TabIndex = 70;
			this->HDD1->Text = L"label12";
			// 
			// SSD1
			// 
			this->SSD1->AutoSize = true;
			this->SSD1->Location = System::Drawing::Point(652, 131);
			this->SSD1->Name = L"SSD1";
			this->SSD1->Size = System::Drawing::Size(41, 13);
			this->SSD1->TabIndex = 71;
			this->SSD1->Text = L"label11";
			// 
			// Amount1
			// 
			this->Amount1->AutoSize = true;
			this->Amount1->Location = System::Drawing::Point(652, 159);
			this->Amount1->Name = L"Amount1";
			this->Amount1->Size = System::Drawing::Size(41, 13);
			this->Amount1->TabIndex = 72;
			this->Amount1->Text = L"label10";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->label11);
			this->groupBox2->Controls->Add(this->Amount1);
			this->groupBox2->Controls->Add(this->numericUpDown2);
			this->groupBox2->Controls->Add(this->SSD1);
			this->groupBox2->Controls->Add(this->HDD1);
			this->groupBox2->Controls->Add(this->RAM1);
			this->groupBox2->Controls->Add(this->Price1);
			this->groupBox2->Controls->Add(this->PSU1);
			this->groupBox2->Controls->Add(this->Motherboard1);
			this->groupBox2->Controls->Add(this->Video1);
			this->groupBox2->Controls->Add(this->CPU1);
			this->groupBox2->Controls->Add(this->NameComp1);
			this->groupBox2->Controls->Add(this->label32);
			this->groupBox2->Controls->Add(this->label33);
			this->groupBox2->Controls->Add(this->label34);
			this->groupBox2->Controls->Add(this->label35);
			this->groupBox2->Controls->Add(this->label36);
			this->groupBox2->Controls->Add(this->label37);
			this->groupBox2->Controls->Add(this->label38);
			this->groupBox2->Controls->Add(this->label39);
			this->groupBox2->Controls->Add(this->label40);
			this->groupBox2->Controls->Add(this->label41);
			this->groupBox2->Controls->Add(this->pictureBox2);
			this->groupBox2->Location = System::Drawing::Point(10, 272);
			this->groupBox2->Margin = System::Windows::Forms::Padding(2);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Padding = System::Windows::Forms::Padding(2);
			this->groupBox2->Size = System::Drawing::Size(782, 222);
			this->groupBox2->TabIndex = 43;
			this->groupBox2->TabStop = false;
			this->groupBox2->Visible = false;
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(525, 193);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(75, 13);
			this->label11->TabIndex = 58;
			this->label11->Text = L"Для покупки:";
			// 
			// numericUpDown2
			// 
			this->numericUpDown2->Location = System::Drawing::Point(649, 191);
			this->numericUpDown2->Name = L"numericUpDown2";
			this->numericUpDown2->Size = System::Drawing::Size(52, 20);
			this->numericUpDown2->TabIndex = 57;
			this->numericUpDown2->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->numericUpDown2->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
			this->numericUpDown2->ValueChanged += gcnew System::EventHandler(this, &UserBin::numericUpDown_ValueChanged);
			// 
			// linkLabel2
			// 
			this->linkLabel2->AutoSize = true;
			this->linkLabel2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->linkLabel2->Location = System::Drawing::Point(698, 947);
			this->linkLabel2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->linkLabel2->Name = L"linkLabel2";
			this->linkLabel2->Size = System::Drawing::Size(103, 17);
			this->linkLabel2->TabIndex = 48;
			this->linkLabel2->TabStop = true;
			this->linkLabel2->Text = L"Следующая →";
			this->linkLabel2->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &UserBin::linkLabel2_LinkClicked);
			// 
			// linkLabel1
			// 
			this->linkLabel1->AutoSize = true;
			this->linkLabel1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->linkLabel1->Location = System::Drawing::Point(9, 947);
			this->linkLabel1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->linkLabel1->Name = L"linkLabel1";
			this->linkLabel1->Size = System::Drawing::Size(112, 17);
			this->linkLabel1->TabIndex = 47;
			this->linkLabel1->TabStop = true;
			this->linkLabel1->Text = L"← Предыдущая";
			this->linkLabel1->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &UserBin::linkLabel1_LinkClicked);
			// 
			// linkLabel3
			// 
			this->linkLabel3->AutoSize = true;
			this->linkLabel3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->linkLabel3->Location = System::Drawing::Point(594, 18);
			this->linkLabel3->Name = L"linkLabel3";
			this->linkLabel3->Size = System::Drawing::Size(50, 16);
			this->linkLabel3->TabIndex = 49;
			this->linkLabel3->TabStop = true;
			this->linkLabel3->Text = L"Назад";
			this->linkLabel3->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &UserBin::linkLabel3_LinkClicked);
			// 
			// UserBin
			// 
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::None;
			this->AutoScroll = true;
			this->AutoValidate = System::Windows::Forms::AutoValidate::Disable;
			this->BackColor = System::Drawing::SystemColors::ButtonHighlight;
			this->ClientSize = System::Drawing::Size(824, 600);
			this->Controls->Add(this->linkLabel3);
			this->Controls->Add(this->linkLabel2);
			this->Controls->Add(this->linkLabel1);
			this->Controls->Add(this->groupBox4);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->label1);
			this->Cursor = System::Windows::Forms::Cursors::Default;
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Margin = System::Windows::Forms::Padding(2);
			this->MaximizeBox = false;
			this->Name = L"UserBin";
			this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Корзина";
			this->Load += gcnew System::EventHandler(this, &UserBin::UserBin_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->EndInit();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->EndInit();
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown2))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

		private: System::Void fillShoppingCart(Computers* computer)
		{
			int diff = shoppingCartFileLength - shoppingCartPage * 4;
			int groupBoxYcoordinate = 230;
			diff = abs(diff);

			if ((diff == 0))
			{
				this->Hide();
				this->Owner->Show();
			}
			else
			{
				char buff[10];

				groupBox1->Show();
				NameComp0->Text = msclr::interop::marshal_as<System::String^>(computer[0].name);
				CPU0->Text = msclr::interop::marshal_as<System::String^>(computer[0].component.cpu);
				Video0->Text = msclr::interop::marshal_as<System::String^>(computer[0].component.gpu);
				Motherboard0->Text = msclr::interop::marshal_as<System::String^>(computer[0].component.motherboard);
				PSU0->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[0].component.psu, buff, 10));
				RAM0->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[0].component.ram, buff, 10));
				HDD0->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[0].component.hdd, buff, 10));
				SSD0->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[0].component.ssd, buff, 10));
				Amount0->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[0].amount, buff, 10));
				std::string str = _ltoa(computer[0].cost, buff, 10);
				str += " Руб.";
				Price0->Text = msclr::interop::marshal_as<System::String^>(str);
				pictureBox1->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[0].photo);

				numericUpDown1->Maximum = computer[0].amount;
				numericUpDown1->Value = computer[0].buyAmount;


				System::Drawing::Point point = this->linkLabel1->Location;
				if (diff <= 4)
				{
					itemsOfThisPage = diff;
					linkLabel2->Hide();
					this->linkLabel1->Location = System::Drawing::Point(16, 1860 - groupBoxYcoordinate * (4 - diff));
					groupBox2->Hide();
					groupBox3->Hide();
					groupBox4->Hide();
					switch (diff - 1)
					{
					case 3:
					{
						groupBox4->Show();
					}
					case 2:
					{
						groupBox3->Show();
					}
					case 1:
					{
						groupBox2->Show();
					}
					}

					if (diff == 2)
					{
						this->ClientSize = System::Drawing::Size(824, 620 - groupBoxYcoordinate / 2);
					}

					if (diff == 1)
					{
						this->ClientSize = System::Drawing::Size(824, 600 - groupBoxYcoordinate);
					}
				}
				else
				{
					itemsOfThisPage = 4;
					linkLabel2->Show();
					groupBox2->Show();
					groupBox3->Show();
					groupBox4->Show();
				}

				if (diff != shoppingCartFileLength)
				{
					linkLabel1->Show();
				}
				else
				{
					linkLabel1->Hide();
				}

				for (int i = 1; i < 4; i++)
				{
					string nameGroupBox = "groupBox";
					nameGroupBox += _itoa(i + 1, buff, 10);
					if (this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Visible == true)
					{
						string nameElement = "NameComp";
						nameElement += _itoa(i, buff, 10);

						this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(computer[i].name);

						nameElement = "CPU";
						nameElement += _itoa(i, buff, 10);

						this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(computer[i].component.cpu);

						nameElement = "Video";
						nameElement += _itoa(i, buff, 10);

						this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(computer[i].component.gpu);

						nameElement = "Motherboard";
						nameElement += _itoa(i, buff, 10);

						this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(computer[i].component.motherboard);

						nameElement = "PSU";
						nameElement += _itoa(i, buff, 10);

						this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[i].component.psu, buff, 10));

						nameElement = "RAM";
						nameElement += _itoa(i, buff, 10);

						this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[i].component.ram, buff, 10));

						nameElement = "HDD";
						nameElement += _itoa(i, buff, 10);

						this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[i].component.hdd, buff, 10));

						nameElement = "SSD";
						nameElement += _itoa(i, buff, 10);

						this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[i].component.ssd, buff, 10));

						nameElement = "Amount";
						nameElement += _itoa(i, buff, 10);

						this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[i].amount, buff, 10));

						nameElement = "Price";
						nameElement += _itoa(i, buff, 10);

						this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(strcat(_ltoa(computer[i].cost, buff, 10), " Руб."));
					}
				}

				if (groupBox2->Visible == true)
				{
					pictureBox2->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[1].photo);

					numericUpDown2->Maximum = computer[1].amount;
					numericUpDown2->Value = computer[1].buyAmount;
				}

				if (groupBox3->Visible == true)
				{
					pictureBox3->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[2].photo);

					numericUpDown3->Maximum = computer[2].amount;
					numericUpDown3->Value = computer[2].buyAmount;
				}

				if (groupBox4->Visible == true)
				{
					pictureBox4->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[3].photo);

					numericUpDown4->Maximum = computer[3].amount;
					numericUpDown4->Value = computer[3].buyAmount;
				}
			}
		}

private: System::Void UserBin_Load(System::Object^ sender, System::EventArgs^ e) 
{
	shoppingCartFileLength = getFileLength(shoppingCartFile);
	Computers* computer;
	computer = getShoppingCartComputers(shoppingCartPage * 4);

	fillShoppingCart(computer);
	delete[4] computer;
}
private: System::Void linkLabel1_LinkClicked(System::Object^ sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^ e) {
	shoppingCartPage--;
	Computers* computer;
	computer = getShoppingCartComputers(shoppingCartPage * 4);

	fillShoppingCart(computer);
	delete[4] computer;
}
private: System::Void linkLabel2_LinkClicked(System::Object^ sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^ e) {
	shoppingCartPage++;
	Computers* computer;
	computer = getShoppingCartComputers(shoppingCartPage * 4);

	fillShoppingCart(computer);
	delete[4] computer;
}

private: System::Void numericUpDown_ValueChanged(System::Object^ sender, System::EventArgs^ e)
{
	System::Windows::Forms::NumericUpDown^ numericUpDown = (NumericUpDown^)sender;
	int index = atoi(&msclr::interop::marshal_as<std::string>(numericUpDown->Name)[13]) - 1;
	Computers* computer;
	computer = getShoppingCartComputers(shoppingCartPage * 4);
	if ((int)numericUpDown->Value == 0)
	{
		deleteComputer(computer[index].name, shoppingCartFile);
		shoppingCartFileLength = getFileLength(shoppingCartFile);
		if ((shoppingCartFileLength - shoppingCartPage * 4  == 0) && (shoppingCartPage != 0))
		{
			shoppingCartPage--;
		}
		computer = getShoppingCartComputers(shoppingCartPage * 4);
		MessageBox::Show("Товар был удален из корзины!");
		fillShoppingCart(computer);
	}
	else
	{
		computer[index].buyAmount = (int)numericUpDown->Value;
		refreshShoppingCart(computer[index], index);
	}
	delete[4] computer;
}

private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e)
{
	MessageBox::Show("Заказ оформлен!");

	reduceAmount();
	addToOrderFile();

	ofstream clearFile(shoppingCartFile, ios::binary | ios::trunc);
	clearFile.close();
	this->Hide();
	this->Owner->Show();
}

	private: System::Void linkLabel3_LinkClicked(System::Object^ sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^ e)
	{
		this->Hide();
		this->Owner->Show();
	}
};
}