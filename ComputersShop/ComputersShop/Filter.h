#pragma once

namespace ComputersShop {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� Filter
	/// </summary>
	public ref class Filter : public System::Windows::Forms::Form
	{
	public:
		Filter(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~Filter()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:
	private: System::Windows::Forms::Label^ label11;

	private: System::Windows::Forms::Label^ label9;
	private: System::Windows::Forms::Label^ label8;
	private: System::Windows::Forms::Label^ label7;
	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::ComboBox^ filterName;

	private: System::Windows::Forms::ComboBox^ filterCPU;

	private: System::Windows::Forms::ComboBox^ filterGPU;

	private: System::Windows::Forms::ComboBox^ filterMotherboard;

	private: System::Windows::Forms::ComboBox^ filterRAM1;
	private: System::Windows::Forms::ComboBox^ filterRAM2;
	private: System::Windows::Forms::TextBox^ filterSPU;












	private: System::Windows::Forms::ComboBox^ filterHDD1;
	private: System::Windows::Forms::ComboBox^ filterHDD2;
	private: System::Windows::Forms::ComboBox^ filterSSD1;
	private: System::Windows::Forms::ComboBox^ filterSSD2;
	private: System::Windows::Forms::TextBox^ filterCost1;
	private: System::Windows::Forms::TextBox^ filterCost2;
	private: System::Windows::Forms::Button^ filterButton;







	private: System::Windows::Forms::Label^ label10;
	private: System::Windows::Forms::Label^ label12;








	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->filterName = (gcnew System::Windows::Forms::ComboBox());
			this->filterCPU = (gcnew System::Windows::Forms::ComboBox());
			this->filterGPU = (gcnew System::Windows::Forms::ComboBox());
			this->filterMotherboard = (gcnew System::Windows::Forms::ComboBox());
			this->filterRAM1 = (gcnew System::Windows::Forms::ComboBox());
			this->filterRAM2 = (gcnew System::Windows::Forms::ComboBox());
			this->filterSPU = (gcnew System::Windows::Forms::TextBox());
			this->filterHDD1 = (gcnew System::Windows::Forms::ComboBox());
			this->filterHDD2 = (gcnew System::Windows::Forms::ComboBox());
			this->filterSSD1 = (gcnew System::Windows::Forms::ComboBox());
			this->filterSSD2 = (gcnew System::Windows::Forms::ComboBox());
			this->filterCost1 = (gcnew System::Windows::Forms::TextBox());
			this->filterCost2 = (gcnew System::Windows::Forms::TextBox());
			this->filterButton = (gcnew System::Windows::Forms::Button());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(54, 321);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(78, 17);
			this->label11->TabIndex = 35;
			this->label11->Text = L"����, ���.";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(20, 292);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(109, 17);
			this->label9->TabIndex = 33;
			this->label9->Text = L"����� SSD, ��";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(52, 145);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(81, 17);
			this->label8->TabIndex = 32;
			this->label8->Text = L"���. �����";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(28, 263);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(105, 17);
			this->label7->TabIndex = 31;
			this->label7->Text = L"����� ��, ��";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(30, 235);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(102, 17);
			this->label6->TabIndex = 30;
			this->label6->Text = L"����� ��, ��";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(10, 172);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(122, 17);
			this->label5->TabIndex = 29;
			this->label5->Text = L"�������� ��, ��";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(45, 117);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(87, 17);
			this->label4->TabIndex = 28;
			this->label4->Text = L"����������";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(52, 89);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(80, 17);
			this->label3->TabIndex = 27;
			this->label3->Text = L"���������";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(60, 57);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(72, 17);
			this->label2->TabIndex = 26;
			this->label2->Text = L"��������";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label1->Location = System::Drawing::Point(154, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(75, 20);
			this->label1->TabIndex = 37;
			this->label1->Text = L"������";
			// 
			// filterName
			// 
			this->filterName->FormattingEnabled = true;
			this->filterName->Items->AddRange(gcnew cli::array< System::Object^  >(2) { L"Acer", L"HP" });
			this->filterName->Location = System::Drawing::Point(139, 48);
			this->filterName->Name = L"filterName";
			this->filterName->Size = System::Drawing::Size(210, 24);
			this->filterName->TabIndex = 38;
			// 
			// filterCPU
			// 
			this->filterCPU->FormattingEnabled = true;
			this->filterCPU->Items->AddRange(gcnew cli::array< System::Object^  >(2) { L"Intel", L"AMD" });
			this->filterCPU->Location = System::Drawing::Point(139, 80);
			this->filterCPU->Name = L"filterCPU";
			this->filterCPU->Size = System::Drawing::Size(210, 24);
			this->filterCPU->TabIndex = 39;
			// 
			// filterGPU
			// 
			this->filterGPU->FormattingEnabled = true;
			this->filterGPU->Items->AddRange(gcnew cli::array< System::Object^  >(2) { L"NVIDIA", L"AMD" });
			this->filterGPU->Location = System::Drawing::Point(138, 110);
			this->filterGPU->Name = L"filterGPU";
			this->filterGPU->Size = System::Drawing::Size(210, 24);
			this->filterGPU->TabIndex = 40;
			// 
			// filterMotherboard
			// 
			this->filterMotherboard->FormattingEnabled = true;
			this->filterMotherboard->Items->AddRange(gcnew cli::array< System::Object^  >(3) { L"GIGABYTE", L"Intel", L"AMD" });
			this->filterMotherboard->Location = System::Drawing::Point(139, 140);
			this->filterMotherboard->Name = L"filterMotherboard";
			this->filterMotherboard->Size = System::Drawing::Size(210, 24);
			this->filterMotherboard->TabIndex = 41;
			// 
			// filterRAM1
			// 
			this->filterRAM1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->filterRAM1->FormattingEnabled = true;
			this->filterRAM1->Items->AddRange(gcnew cli::array< System::Object^  >(10) {
				L"1", L"2", L"3", L"4", L"6", L"8", L"16", L"32",
					L"64", L"128"
			});
			this->filterRAM1->Location = System::Drawing::Point(138, 228);
			this->filterRAM1->Name = L"filterRAM1";
			this->filterRAM1->Size = System::Drawing::Size(76, 24);
			this->filterRAM1->TabIndex = 42;
			// 
			// filterRAM2
			// 
			this->filterRAM2->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->filterRAM2->FormattingEnabled = true;
			this->filterRAM2->Items->AddRange(gcnew cli::array< System::Object^  >(10) {
				L"1", L"2", L"3", L"4", L"6", L"8", L"16", L"32",
					L"64", L"128"
			});
			this->filterRAM2->Location = System::Drawing::Point(273, 228);
			this->filterRAM2->Name = L"filterRAM2";
			this->filterRAM2->Size = System::Drawing::Size(76, 24);
			this->filterRAM2->TabIndex = 43;
			// 
			// filterSPU
			// 
			this->filterSPU->Location = System::Drawing::Point(139, 166);
			this->filterSPU->Name = L"filterSPU";
			this->filterSPU->Size = System::Drawing::Size(209, 22);
			this->filterSPU->TabIndex = 44;
			// 
			// filterHDD1
			// 
			this->filterHDD1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->filterHDD1->FormattingEnabled = true;
			this->filterHDD1->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"128", L"256", L"512", L"1024", L"2048", L"4096",
					L"8192"
			});
			this->filterHDD1->Location = System::Drawing::Point(138, 256);
			this->filterHDD1->Name = L"filterHDD1";
			this->filterHDD1->Size = System::Drawing::Size(76, 24);
			this->filterHDD1->TabIndex = 45;
			// 
			// filterHDD2
			// 
			this->filterHDD2->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->filterHDD2->FormattingEnabled = true;
			this->filterHDD2->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"128", L"256", L"512", L"1024", L"2048", L"4096",
					L"8192"
			});
			this->filterHDD2->Location = System::Drawing::Point(273, 256);
			this->filterHDD2->Name = L"filterHDD2";
			this->filterHDD2->Size = System::Drawing::Size(76, 24);
			this->filterHDD2->TabIndex = 46;
			// 
			// filterSSD1
			// 
			this->filterSSD1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->filterSSD1->FormattingEnabled = true;
			this->filterSSD1->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"64", L"128", L"240", L"256", L"512", L"1024",
					L"2048"
			});
			this->filterSSD1->Location = System::Drawing::Point(138, 286);
			this->filterSSD1->Name = L"filterSSD1";
			this->filterSSD1->Size = System::Drawing::Size(76, 24);
			this->filterSSD1->TabIndex = 47;
			// 
			// filterSSD2
			// 
			this->filterSSD2->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->filterSSD2->FormattingEnabled = true;
			this->filterSSD2->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"64", L"128", L"240", L"256", L"512", L"1024",
					L"2048"
			});
			this->filterSSD2->Location = System::Drawing::Point(273, 285);
			this->filterSSD2->Name = L"filterSSD2";
			this->filterSSD2->Size = System::Drawing::Size(76, 24);
			this->filterSSD2->TabIndex = 48;
			// 
			// filterCost1
			// 
			this->filterCost1->Location = System::Drawing::Point(138, 321);
			this->filterCost1->Name = L"filterCost1";
			this->filterCost1->Size = System::Drawing::Size(76, 22);
			this->filterCost1->TabIndex = 49;
			// 
			// filterCost2
			// 
			this->filterCost2->Location = System::Drawing::Point(273, 321);
			this->filterCost2->Name = L"filterCost2";
			this->filterCost2->Size = System::Drawing::Size(76, 22);
			this->filterCost2->TabIndex = 50;
			// 
			// filterButton
			// 
			this->filterButton->BackColor = System::Drawing::SystemColors::Highlight;
			this->filterButton->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->filterButton->Location = System::Drawing::Point(138, 347);
			this->filterButton->Name = L"filterButton";
			this->filterButton->Size = System::Drawing::Size(109, 40);
			this->filterButton->TabIndex = 51;
			this->filterButton->Text = L"���������";
			this->filterButton->UseVisualStyleBackColor = false;
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(164, 205);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(26, 17);
			this->label10->TabIndex = 52;
			this->label10->Text = L"��";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(296, 205);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(27, 17);
			this->label12->TabIndex = 53;
			this->label12->Text = L"��";
			// 
			// Filter
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::ButtonHighlight;
			this->ClientSize = System::Drawing::Size(374, 403);
			this->Controls->Add(this->label12);
			this->Controls->Add(this->label10);
			this->Controls->Add(this->filterButton);
			this->Controls->Add(this->filterCost2);
			this->Controls->Add(this->filterCost1);
			this->Controls->Add(this->filterSSD2);
			this->Controls->Add(this->filterSSD1);
			this->Controls->Add(this->filterHDD2);
			this->Controls->Add(this->filterHDD1);
			this->Controls->Add(this->filterSPU);
			this->Controls->Add(this->filterRAM2);
			this->Controls->Add(this->filterRAM1);
			this->Controls->Add(this->filterMotherboard);
			this->Controls->Add(this->filterGPU);
			this->Controls->Add(this->filterCPU);
			this->Controls->Add(this->filterName);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->label11);
			this->Controls->Add(this->label9);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Name = L"Filter";
			this->Text = L" ";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	};
}
