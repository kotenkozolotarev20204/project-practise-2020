#pragma once
#include "File.h"

namespace ComputersShop {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� SignInForm
	/// </summary>
	public ref class SignInForm : public System::Windows::Forms::Form
	{
	public:
		SignInForm(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~SignInForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^ PasswordLabel;
	protected:
	private: System::Windows::Forms::Label^ LoginLabel;
	private: System::Windows::Forms::TextBox^ PasswordTextBox;
	private: System::Windows::Forms::TextBox^ LoginTextBox;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Button^ ConfirmButton;


	private: System::Windows::Forms::GroupBox^ groupBox1;
	private: System::Windows::Forms::LinkLabel^ linkLabel1;
	private: System::Windows::Forms::ToolTip^ toolTip1;
	private: System::ComponentModel::IContainer^ components;


	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(SignInForm::typeid));
			this->PasswordLabel = (gcnew System::Windows::Forms::Label());
			this->LoginLabel = (gcnew System::Windows::Forms::Label());
			this->PasswordTextBox = (gcnew System::Windows::Forms::TextBox());
			this->LoginTextBox = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->ConfirmButton = (gcnew System::Windows::Forms::Button());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->linkLabel1 = (gcnew System::Windows::Forms::LinkLabel());
			this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
			this->groupBox1->SuspendLayout();
			this->SuspendLayout();
			// 
			// PasswordLabel
			// 
			this->PasswordLabel->AutoSize = true;
			this->PasswordLabel->Location = System::Drawing::Point(14, 54);
			this->PasswordLabel->Name = L"PasswordLabel";
			this->PasswordLabel->Size = System::Drawing::Size(45, 13);
			this->PasswordLabel->TabIndex = 10;
			this->PasswordLabel->Text = L"������";
			// 
			// LoginLabel
			// 
			this->LoginLabel->AutoSize = true;
			this->LoginLabel->Location = System::Drawing::Point(14, 8);
			this->LoginLabel->Name = L"LoginLabel";
			this->LoginLabel->Size = System::Drawing::Size(29, 13);
			this->LoginLabel->TabIndex = 9;
			this->LoginLabel->Text = L"���";
			// 
			// PasswordTextBox
			// 
			this->PasswordTextBox->Location = System::Drawing::Point(14, 70);
			this->PasswordTextBox->MaxLength = 50;
			this->PasswordTextBox->Name = L"PasswordTextBox";
			this->PasswordTextBox->PasswordChar = '*';
			this->PasswordTextBox->Size = System::Drawing::Size(105, 20);
			this->PasswordTextBox->TabIndex = 8;
			this->toolTip1->SetToolTip(this->PasswordTextBox, L"�� ����� 10 �������� ��� ��������\r\n");
			// 
			// LoginTextBox
			// 
			this->LoginTextBox->Location = System::Drawing::Point(14, 24);
			this->LoginTextBox->MaxLength = 50;
			this->LoginTextBox->Name = L"LoginTextBox";
			this->LoginTextBox->Size = System::Drawing::Size(105, 20);
			this->LoginTextBox->TabIndex = 7;
			this->toolTip1->SetToolTip(this->LoginTextBox, L"�� ����� 10 �������� ��� ��������\r\n");
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label1->Location = System::Drawing::Point(6, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(220, 16);
			this->label1->TabIndex = 11;
			this->label1->Text = L"����������� � ������ ��������\r\n";
			// 
			// ConfirmButton
			// 
			this->ConfirmButton->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
			this->ConfirmButton->BackColor = System::Drawing::Color::OrangeRed;
			this->ConfirmButton->Cursor = System::Windows::Forms::Cursors::Hand;
			this->ConfirmButton->FlatAppearance->BorderColor = System::Drawing::Color::Silver;
			this->ConfirmButton->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->ConfirmButton->Font = (gcnew System::Drawing::Font(L"Artifakt Element", 9.749999F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->ConfirmButton->ForeColor = System::Drawing::Color::Transparent;
			this->ConfirmButton->Location = System::Drawing::Point(77, 145);
			this->ConfirmButton->Name = L"ConfirmButton";
			this->ConfirmButton->Size = System::Drawing::Size(72, 29);
			this->ConfirmButton->TabIndex = 12;
			this->ConfirmButton->Text = L"��";
			this->ConfirmButton->UseVisualStyleBackColor = false;
			this->ConfirmButton->Click += gcnew System::EventHandler(this, &SignInForm::button2_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->LoginTextBox);
			this->groupBox1->Controls->Add(this->PasswordTextBox);
			this->groupBox1->Controls->Add(this->LoginLabel);
			this->groupBox1->Controls->Add(this->PasswordLabel);
			this->groupBox1->Location = System::Drawing::Point(45, 32);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(135, 100);
			this->groupBox1->TabIndex = 15;
			this->groupBox1->TabStop = false;
			// 
			// linkLabel1
			// 
			this->linkLabel1->AutoSize = true;
			this->linkLabel1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->linkLabel1->Location = System::Drawing::Point(94, 191);
			this->linkLabel1->Name = L"linkLabel1";
			this->linkLabel1->Size = System::Drawing::Size(39, 13);
			this->linkLabel1->TabIndex = 16;
			this->linkLabel1->TabStop = true;
			this->linkLabel1->Text = L"�����";
			this->linkLabel1->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &SignInForm::linkLabel1_LinkClicked);
			// 
			// toolTip1
			// 
			this->toolTip1->AutomaticDelay = 250;
			this->toolTip1->BackColor = System::Drawing::SystemColors::MenuHighlight;
			this->toolTip1->ForeColor = System::Drawing::Color::Transparent;
			// 
			// SignInForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->ClientSize = System::Drawing::Size(227, 225);
			this->Controls->Add(this->linkLabel1);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->ConfirmButton);
			this->Controls->Add(this->label1);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->Name = L"SignInForm";
			this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
			this->Text = L"�����������";
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	string SystemToStl(String^ s)
	{
		using namespace Runtime::InteropServices;
		const char* ptr = (const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
		return string(ptr);
	}

	private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		string userData, typedLogin, typedPassword;

		typedLogin = SystemToStl(LoginTextBox->Text);
		typedPassword = SystemToStl(PasswordTextBox->Text);
		userData = typedLogin + ' ' + typedPassword;

		if (!System::IO::File::Exists(msclr::interop::marshal_as<System::String^>(DATA_FILE)))
		{
			ofstream file(DATA_FILE, ios::binary);
			file.close();
		}
		
		if (!checkInvalidSymbols(typedPassword) || !checkInvalidSymbols(typedLogin))
		{
			MessageBox::Show("������������ ����� ��� ������!");
		}
		else if (!checkLogin(typedLogin))
		{
			MessageBox::Show("������ ����� ��� �����!");
		}
		else
		{
			addUser(userData, typedLogin);
			this->Hide();
			this->Owner->Show();
		}
	}

	System::Void linkLabel1_LinkClicked(System::Object^ sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^ e) 
	{
		this->Hide();
		this->Owner->Show();
	}
};
}
