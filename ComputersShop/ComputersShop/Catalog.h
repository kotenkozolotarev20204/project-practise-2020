﻿#pragma once

#include <stdlib.h>
#include <msclr/marshal_cppstd.h>
#include <string>
#include "MyForm.h"
#include "InputInformation.h"
#include "UserBin.h"
#include "SimpleFunctions.h"

unsigned int storePage = 0;
bool checkFilter = false;
std::string name = "", cpu = "", gpu = "", motherboard = "", photo = "";
int ram1 = 0, ram2 = 0, hdd1 = 0, hdd2 = 0, ssd1 = 0, ssd2 = 0, psu1 = 0, psu2 = 0, cost1 = 0, cost2 = 0;

namespace ComputersShop {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Сводка для Catalog
	/// </summary>
	public ref class Catalog : public System::Windows::Forms::Form
	{
	public:
		Catalog(void)
		{
			InitializeComponent();
			//
			//TODO: добавьте код конструктора
			//
		}

	protected:
		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		~Catalog()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^ Amount2;
	protected:
	private: System::Windows::Forms::Label^ Amount7;
	private: System::Windows::Forms::Label^ label37;
	private: System::Windows::Forms::Label^ Amount3;
	private: System::Windows::Forms::Label^ label21;
	private: System::Windows::Forms::Label^ Amount4;
	private: System::Windows::Forms::Label^ label25;
	private: System::Windows::Forms::Button^ button8;
	private: System::Windows::Forms::Label^ Price7;
	private: System::Windows::Forms::Label^ label94;
	private: System::Windows::Forms::Label^ HDD7;
	private: System::Windows::Forms::Label^ label96;
	private: System::Windows::Forms::Label^ Motherboard7;
	private: System::Windows::Forms::Label^ label98;
	private: System::Windows::Forms::Label^ RAM7;
	private: System::Windows::Forms::Label^ label100;
	private: System::Windows::Forms::Label^ Video7;
	private: System::Windows::Forms::Label^ label102;
	private: System::Windows::Forms::Label^ CPU7;
	private: System::Windows::Forms::Label^ Amount5;
	private: System::Windows::Forms::Label^ label29;
	private: System::Windows::Forms::Label^ Amount6;
	private: System::Windows::Forms::Label^ label33;
	private: System::Windows::Forms::Label^ label17;
	private: System::Windows::Forms::Label^ NameComp5;
	private: System::Windows::Forms::Label^ label104;
	private: System::Windows::Forms::Label^ NameComp7;
	private: System::Windows::Forms::Label^ label106;
	private: System::Windows::Forms::PictureBox^ pictureBox8;
	private: System::Windows::Forms::GroupBox^ groupBox8;
	private: System::Windows::Forms::Button^ button7;
	private: System::Windows::Forms::GroupBox^ groupBox7;
	private: System::Windows::Forms::Label^ Price6;
	private: System::Windows::Forms::Label^ label80;
	private: System::Windows::Forms::Label^ HDD6;
	private: System::Windows::Forms::Label^ label82;
	private: System::Windows::Forms::Label^ Motherboard6;
	private: System::Windows::Forms::Label^ label84;
	private: System::Windows::Forms::Label^ RAM6;
	private: System::Windows::Forms::Label^ label86;
	private: System::Windows::Forms::Label^ Video6;
	private: System::Windows::Forms::Label^ label88;
	private: System::Windows::Forms::Label^ CPU6;
	private: System::Windows::Forms::Label^ label90;
	private: System::Windows::Forms::Label^ NameComp6;
	private: System::Windows::Forms::Label^ label92;
	private: System::Windows::Forms::PictureBox^ pictureBox7;
	private: System::Windows::Forms::Label^ label78;
	private: System::Windows::Forms::PictureBox^ pictureBox6;
	private: System::Windows::Forms::Label^ label76;
	private: System::Windows::Forms::Label^ Amount0;
	private: System::Windows::Forms::Label^ label9;
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::Label^ Price0;
	private: System::Windows::Forms::Label^ label8;
	private: System::Windows::Forms::Label^ HDD0;
	private: System::Windows::Forms::Label^ label7;
	private: System::Windows::Forms::Label^ Motherboard0;
	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::Label^ RAM0;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::Label^ Video0;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::Label^ CPU0;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::Label^ NameComp0;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::GroupBox^ groupBox1;
	private: System::Windows::Forms::PictureBox^ pictureBox1;
	private: System::Windows::Forms::Label^ Amount1;
	private: System::Windows::Forms::Label^ label13;
	private: System::Windows::Forms::PictureBox^ pictureBox2;
	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::Label^ Price2;
	private: System::Windows::Forms::Label^ label24;
	private: System::Windows::Forms::GroupBox^ groupBox4;
	private: System::Windows::Forms::Button^ button4;
	private: System::Windows::Forms::Label^ Price3;
	private: System::Windows::Forms::Label^ label38;
	private: System::Windows::Forms::Label^ HDD3;
	private: System::Windows::Forms::Label^ label40;
	private: System::Windows::Forms::Label^ Motherboard3;
	private: System::Windows::Forms::Label^ label42;
	private: System::Windows::Forms::Label^ RAM3;
	private: System::Windows::Forms::Label^ label44;
	private: System::Windows::Forms::Label^ Video3;
	private: System::Windows::Forms::Label^ label46;
	private: System::Windows::Forms::Label^ CPU3;
	private: System::Windows::Forms::Label^ label48;
	private: System::Windows::Forms::Label^ NameComp3;
	private: System::Windows::Forms::Label^ label50;
	private: System::Windows::Forms::PictureBox^ pictureBox4;
	private: System::Windows::Forms::GroupBox^ groupBox3;
	private: System::Windows::Forms::Label^ HDD2;
	private: System::Windows::Forms::Label^ label26;
	private: System::Windows::Forms::Label^ Motherboard2;
	private: System::Windows::Forms::Label^ label28;
	private: System::Windows::Forms::Label^ RAM2;
	private: System::Windows::Forms::Label^ label30;
	private: System::Windows::Forms::Label^ Video2;
	private: System::Windows::Forms::Label^ label32;
	private: System::Windows::Forms::Label^ CPU2;
	private: System::Windows::Forms::Label^ label34;
	private: System::Windows::Forms::Label^ NameComp2;
	private: System::Windows::Forms::Label^ label36;
	private: System::Windows::Forms::PictureBox^ pictureBox3;
	private: System::Windows::Forms::Label^ CPU5;
	private: System::Windows::Forms::Label^ HDD1;
	private: System::Windows::Forms::Label^ label12;
	private: System::Windows::Forms::Label^ Motherboard1;
	private: System::Windows::Forms::Label^ label14;
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Label^ Price1;
	private: System::Windows::Forms::GroupBox^ groupBox2;
	private: System::Windows::Forms::Label^ label10;
	private: System::Windows::Forms::Label^ RAM1;
	private: System::Windows::Forms::Label^ label16;
	private: System::Windows::Forms::Label^ Video1;
	private: System::Windows::Forms::Label^ label18;
	private: System::Windows::Forms::Label^ CPU1;
	private: System::Windows::Forms::Label^ label20;
	private: System::Windows::Forms::Label^ NameComp1;
	private: System::Windows::Forms::Label^ label22;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Button^ button6;
	private: System::Windows::Forms::Label^ label66;
	private: System::Windows::Forms::Label^ HDD5;
	private: System::Windows::Forms::Label^ Motherboard5;
	private: System::Windows::Forms::Label^ label70;
	private: System::Windows::Forms::Label^ RAM5;
	private: System::Windows::Forms::Label^ label72;
	private: System::Windows::Forms::Label^ Price5;
	private: System::Windows::Forms::Label^ label68;
	private: System::Windows::Forms::Label^ Video5;
	private: System::Windows::Forms::Label^ label74;
	private: System::Windows::Forms::GroupBox^ groupBox5;
	private: System::Windows::Forms::Button^ button5;
	private: System::Windows::Forms::Label^ Price4;
	private: System::Windows::Forms::Label^ label52;
	private: System::Windows::Forms::Label^ HDD4;
	private: System::Windows::Forms::Label^ label54;
	private: System::Windows::Forms::Label^ Motherboard4;
	private: System::Windows::Forms::Label^ label56;
	private: System::Windows::Forms::Label^ RAM4;
	private: System::Windows::Forms::Label^ label58;
	private: System::Windows::Forms::Label^ Video4;
	private: System::Windows::Forms::Label^ label60;
	private: System::Windows::Forms::Label^ CPU4;
	private: System::Windows::Forms::Label^ label62;
	private: System::Windows::Forms::Label^ NameComp4;
	private: System::Windows::Forms::Label^ label64;
	private: System::Windows::Forms::PictureBox^ pictureBox5;
	private: System::Windows::Forms::GroupBox^ groupBox6;
	private: System::Windows::Forms::LinkLabel^ BackMenu;
	private: System::Windows::Forms::LinkLabel^ linkLabel1;
	private: System::Windows::Forms::LinkLabel^ linkLabel2;
	private: System::Windows::Forms::Label^ SSD0;
	private: System::Windows::Forms::Label^ label15;
	private: System::Windows::Forms::Label^ PSU0;
	private: System::Windows::Forms::Label^ label19;
	private: System::Windows::Forms::Label^ SSD1;
	private: System::Windows::Forms::Label^ label27;
	private: System::Windows::Forms::Label^ PSU1;
	private: System::Windows::Forms::Label^ label35;
	private: System::Windows::Forms::Label^ SSD3;
	private: System::Windows::Forms::Label^ label47;
	private: System::Windows::Forms::Label^ PSU3;
	private: System::Windows::Forms::Label^ label43;
	private: System::Windows::Forms::Label^ PSU2;
	private: System::Windows::Forms::Label^ label39;
	private: System::Windows::Forms::Label^ SSD2;
	private: System::Windows::Forms::Label^ label23;
	private: System::Windows::Forms::Label^ SSD4;
	private: System::Windows::Forms::Label^ label55;
	private: System::Windows::Forms::Label^ PSU4;
	private: System::Windows::Forms::Label^ label51;
	private: System::Windows::Forms::Label^ SSD5;
	private: System::Windows::Forms::Label^ label59;
	private: System::Windows::Forms::Label^ PSU5;
	private: System::Windows::Forms::Label^ label63;
	private: System::Windows::Forms::Label^ SSD6;
	private: System::Windows::Forms::Label^ label67;
	private: System::Windows::Forms::Label^ PSU6;
	private: System::Windows::Forms::Label^ label71;
	private: System::Windows::Forms::Label^ SSD7;
	private: System::Windows::Forms::Label^ label75;
	private: System::Windows::Forms::Label^ PSU7;
	private: System::Windows::Forms::Label^ label79;
	private: System::Windows::Forms::PictureBox^ pictureBox9;
	private: System::Windows::Forms::Label^ BinLabel;
	private: System::Windows::Forms::GroupBox^ Filter;
	private: System::Windows::Forms::Label^ label11;
	private: System::Windows::Forms::Label^ label31;
	private: System::Windows::Forms::Button^ filterButton;
	private: System::Windows::Forms::TextBox^ filterCost2;
	private: System::Windows::Forms::TextBox^ filterCost1;
	private: System::Windows::Forms::ComboBox^ filterSSD2;
	private: System::Windows::Forms::ComboBox^ filterSSD1;
	private: System::Windows::Forms::ComboBox^ filterHDD2;
	private: System::Windows::Forms::ComboBox^ filterHDD1;
	private: System::Windows::Forms::TextBox^ filterPSU1;
	private: System::Windows::Forms::ComboBox^ filterRAM2;
	private: System::Windows::Forms::ComboBox^ filterRAM1;
	private: System::Windows::Forms::ComboBox^ filterMotherboard;
	private: System::Windows::Forms::ComboBox^ filterGPU;
	private: System::Windows::Forms::ComboBox^ filterCPU;
	private: System::Windows::Forms::ComboBox^ filterName;
	private: System::Windows::Forms::Label^ label45;
	private: System::Windows::Forms::Label^ label49;
	private: System::Windows::Forms::Label^ label53;
	private: System::Windows::Forms::Label^ label57;
	private: System::Windows::Forms::Label^ label61;
	private: System::Windows::Forms::Label^ label65;
	private: System::Windows::Forms::Label^ label69;
	private: System::Windows::Forms::Label^ label73;
	private: System::Windows::Forms::Label^ label77;
	private: System::Windows::Forms::TextBox^ filterPSU2;
	private: System::Windows::Forms::Button^ Clear;

	public: System::Windows::Forms::Label^ AlertFilter;
	private: System::Windows::Forms::LinkLabel^ linkLabel3;
	public:
	private:

	protected:


	private:
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(Catalog::typeid));
			this->Amount2 = (gcnew System::Windows::Forms::Label());
			this->Amount7 = (gcnew System::Windows::Forms::Label());
			this->label37 = (gcnew System::Windows::Forms::Label());
			this->Amount3 = (gcnew System::Windows::Forms::Label());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->Amount4 = (gcnew System::Windows::Forms::Label());
			this->label25 = (gcnew System::Windows::Forms::Label());
			this->button8 = (gcnew System::Windows::Forms::Button());
			this->Price7 = (gcnew System::Windows::Forms::Label());
			this->label94 = (gcnew System::Windows::Forms::Label());
			this->HDD7 = (gcnew System::Windows::Forms::Label());
			this->label96 = (gcnew System::Windows::Forms::Label());
			this->Motherboard7 = (gcnew System::Windows::Forms::Label());
			this->label98 = (gcnew System::Windows::Forms::Label());
			this->RAM7 = (gcnew System::Windows::Forms::Label());
			this->label100 = (gcnew System::Windows::Forms::Label());
			this->Video7 = (gcnew System::Windows::Forms::Label());
			this->label102 = (gcnew System::Windows::Forms::Label());
			this->CPU7 = (gcnew System::Windows::Forms::Label());
			this->Amount5 = (gcnew System::Windows::Forms::Label());
			this->label29 = (gcnew System::Windows::Forms::Label());
			this->Amount6 = (gcnew System::Windows::Forms::Label());
			this->label33 = (gcnew System::Windows::Forms::Label());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->NameComp5 = (gcnew System::Windows::Forms::Label());
			this->label104 = (gcnew System::Windows::Forms::Label());
			this->NameComp7 = (gcnew System::Windows::Forms::Label());
			this->label106 = (gcnew System::Windows::Forms::Label());
			this->pictureBox8 = (gcnew System::Windows::Forms::PictureBox());
			this->groupBox8 = (gcnew System::Windows::Forms::GroupBox());
			this->SSD7 = (gcnew System::Windows::Forms::Label());
			this->label75 = (gcnew System::Windows::Forms::Label());
			this->PSU7 = (gcnew System::Windows::Forms::Label());
			this->label79 = (gcnew System::Windows::Forms::Label());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->groupBox7 = (gcnew System::Windows::Forms::GroupBox());
			this->SSD6 = (gcnew System::Windows::Forms::Label());
			this->label67 = (gcnew System::Windows::Forms::Label());
			this->PSU6 = (gcnew System::Windows::Forms::Label());
			this->label71 = (gcnew System::Windows::Forms::Label());
			this->Price6 = (gcnew System::Windows::Forms::Label());
			this->label80 = (gcnew System::Windows::Forms::Label());
			this->HDD6 = (gcnew System::Windows::Forms::Label());
			this->label82 = (gcnew System::Windows::Forms::Label());
			this->Motherboard6 = (gcnew System::Windows::Forms::Label());
			this->label84 = (gcnew System::Windows::Forms::Label());
			this->RAM6 = (gcnew System::Windows::Forms::Label());
			this->label86 = (gcnew System::Windows::Forms::Label());
			this->Video6 = (gcnew System::Windows::Forms::Label());
			this->label88 = (gcnew System::Windows::Forms::Label());
			this->CPU6 = (gcnew System::Windows::Forms::Label());
			this->label90 = (gcnew System::Windows::Forms::Label());
			this->NameComp6 = (gcnew System::Windows::Forms::Label());
			this->label92 = (gcnew System::Windows::Forms::Label());
			this->pictureBox7 = (gcnew System::Windows::Forms::PictureBox());
			this->label78 = (gcnew System::Windows::Forms::Label());
			this->pictureBox6 = (gcnew System::Windows::Forms::PictureBox());
			this->label76 = (gcnew System::Windows::Forms::Label());
			this->Amount0 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->Price0 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->HDD0 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->Motherboard0 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->RAM0 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->Video0 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->CPU0 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->NameComp0 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->SSD0 = (gcnew System::Windows::Forms::Label());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->PSU0 = (gcnew System::Windows::Forms::Label());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->AlertFilter = (gcnew System::Windows::Forms::Label());
			this->Amount1 = (gcnew System::Windows::Forms::Label());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->Price2 = (gcnew System::Windows::Forms::Label());
			this->label24 = (gcnew System::Windows::Forms::Label());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->SSD3 = (gcnew System::Windows::Forms::Label());
			this->label47 = (gcnew System::Windows::Forms::Label());
			this->PSU3 = (gcnew System::Windows::Forms::Label());
			this->label43 = (gcnew System::Windows::Forms::Label());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->Price3 = (gcnew System::Windows::Forms::Label());
			this->label38 = (gcnew System::Windows::Forms::Label());
			this->HDD3 = (gcnew System::Windows::Forms::Label());
			this->label40 = (gcnew System::Windows::Forms::Label());
			this->Motherboard3 = (gcnew System::Windows::Forms::Label());
			this->label42 = (gcnew System::Windows::Forms::Label());
			this->RAM3 = (gcnew System::Windows::Forms::Label());
			this->label44 = (gcnew System::Windows::Forms::Label());
			this->Video3 = (gcnew System::Windows::Forms::Label());
			this->label46 = (gcnew System::Windows::Forms::Label());
			this->CPU3 = (gcnew System::Windows::Forms::Label());
			this->label48 = (gcnew System::Windows::Forms::Label());
			this->NameComp3 = (gcnew System::Windows::Forms::Label());
			this->label50 = (gcnew System::Windows::Forms::Label());
			this->pictureBox4 = (gcnew System::Windows::Forms::PictureBox());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->PSU2 = (gcnew System::Windows::Forms::Label());
			this->label39 = (gcnew System::Windows::Forms::Label());
			this->SSD2 = (gcnew System::Windows::Forms::Label());
			this->label23 = (gcnew System::Windows::Forms::Label());
			this->HDD2 = (gcnew System::Windows::Forms::Label());
			this->label26 = (gcnew System::Windows::Forms::Label());
			this->Motherboard2 = (gcnew System::Windows::Forms::Label());
			this->label28 = (gcnew System::Windows::Forms::Label());
			this->RAM2 = (gcnew System::Windows::Forms::Label());
			this->label30 = (gcnew System::Windows::Forms::Label());
			this->Video2 = (gcnew System::Windows::Forms::Label());
			this->label32 = (gcnew System::Windows::Forms::Label());
			this->CPU2 = (gcnew System::Windows::Forms::Label());
			this->label34 = (gcnew System::Windows::Forms::Label());
			this->NameComp2 = (gcnew System::Windows::Forms::Label());
			this->label36 = (gcnew System::Windows::Forms::Label());
			this->pictureBox3 = (gcnew System::Windows::Forms::PictureBox());
			this->CPU5 = (gcnew System::Windows::Forms::Label());
			this->HDD1 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->Motherboard1 = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->Price1 = (gcnew System::Windows::Forms::Label());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->PSU1 = (gcnew System::Windows::Forms::Label());
			this->label35 = (gcnew System::Windows::Forms::Label());
			this->SSD1 = (gcnew System::Windows::Forms::Label());
			this->label27 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->RAM1 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->Video1 = (gcnew System::Windows::Forms::Label());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->CPU1 = (gcnew System::Windows::Forms::Label());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->NameComp1 = (gcnew System::Windows::Forms::Label());
			this->label22 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->label66 = (gcnew System::Windows::Forms::Label());
			this->HDD5 = (gcnew System::Windows::Forms::Label());
			this->Motherboard5 = (gcnew System::Windows::Forms::Label());
			this->label70 = (gcnew System::Windows::Forms::Label());
			this->RAM5 = (gcnew System::Windows::Forms::Label());
			this->label72 = (gcnew System::Windows::Forms::Label());
			this->Price5 = (gcnew System::Windows::Forms::Label());
			this->label68 = (gcnew System::Windows::Forms::Label());
			this->Video5 = (gcnew System::Windows::Forms::Label());
			this->label74 = (gcnew System::Windows::Forms::Label());
			this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
			this->SSD4 = (gcnew System::Windows::Forms::Label());
			this->label55 = (gcnew System::Windows::Forms::Label());
			this->PSU4 = (gcnew System::Windows::Forms::Label());
			this->label51 = (gcnew System::Windows::Forms::Label());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->Price4 = (gcnew System::Windows::Forms::Label());
			this->label52 = (gcnew System::Windows::Forms::Label());
			this->HDD4 = (gcnew System::Windows::Forms::Label());
			this->label54 = (gcnew System::Windows::Forms::Label());
			this->Motherboard4 = (gcnew System::Windows::Forms::Label());
			this->label56 = (gcnew System::Windows::Forms::Label());
			this->RAM4 = (gcnew System::Windows::Forms::Label());
			this->label58 = (gcnew System::Windows::Forms::Label());
			this->Video4 = (gcnew System::Windows::Forms::Label());
			this->label60 = (gcnew System::Windows::Forms::Label());
			this->CPU4 = (gcnew System::Windows::Forms::Label());
			this->label62 = (gcnew System::Windows::Forms::Label());
			this->NameComp4 = (gcnew System::Windows::Forms::Label());
			this->label64 = (gcnew System::Windows::Forms::Label());
			this->pictureBox5 = (gcnew System::Windows::Forms::PictureBox());
			this->groupBox6 = (gcnew System::Windows::Forms::GroupBox());
			this->SSD5 = (gcnew System::Windows::Forms::Label());
			this->label59 = (gcnew System::Windows::Forms::Label());
			this->PSU5 = (gcnew System::Windows::Forms::Label());
			this->label63 = (gcnew System::Windows::Forms::Label());
			this->linkLabel1 = (gcnew System::Windows::Forms::LinkLabel());
			this->linkLabel2 = (gcnew System::Windows::Forms::LinkLabel());
			this->pictureBox9 = (gcnew System::Windows::Forms::PictureBox());
			this->BinLabel = (gcnew System::Windows::Forms::Label());
			this->BackMenu = (gcnew System::Windows::Forms::LinkLabel());
			this->Filter = (gcnew System::Windows::Forms::GroupBox());
			this->Clear = (gcnew System::Windows::Forms::Button());
			this->filterPSU2 = (gcnew System::Windows::Forms::TextBox());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->filterCost2 = (gcnew System::Windows::Forms::TextBox());
			this->label31 = (gcnew System::Windows::Forms::Label());
			this->filterButton = (gcnew System::Windows::Forms::Button());
			this->filterCost1 = (gcnew System::Windows::Forms::TextBox());
			this->filterSSD2 = (gcnew System::Windows::Forms::ComboBox());
			this->filterSSD1 = (gcnew System::Windows::Forms::ComboBox());
			this->filterHDD2 = (gcnew System::Windows::Forms::ComboBox());
			this->filterHDD1 = (gcnew System::Windows::Forms::ComboBox());
			this->filterPSU1 = (gcnew System::Windows::Forms::TextBox());
			this->filterRAM2 = (gcnew System::Windows::Forms::ComboBox());
			this->filterRAM1 = (gcnew System::Windows::Forms::ComboBox());
			this->filterMotherboard = (gcnew System::Windows::Forms::ComboBox());
			this->filterGPU = (gcnew System::Windows::Forms::ComboBox());
			this->filterCPU = (gcnew System::Windows::Forms::ComboBox());
			this->filterName = (gcnew System::Windows::Forms::ComboBox());
			this->label45 = (gcnew System::Windows::Forms::Label());
			this->label49 = (gcnew System::Windows::Forms::Label());
			this->label53 = (gcnew System::Windows::Forms::Label());
			this->label57 = (gcnew System::Windows::Forms::Label());
			this->label61 = (gcnew System::Windows::Forms::Label());
			this->label65 = (gcnew System::Windows::Forms::Label());
			this->label69 = (gcnew System::Windows::Forms::Label());
			this->label73 = (gcnew System::Windows::Forms::Label());
			this->label77 = (gcnew System::Windows::Forms::Label());
			this->linkLabel3 = (gcnew System::Windows::Forms::LinkLabel());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox8))->BeginInit();
			this->groupBox8->SuspendLayout();
			this->groupBox7->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox6))->BeginInit();
			this->groupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			this->groupBox4->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->BeginInit();
			this->groupBox3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->BeginInit();
			this->groupBox2->SuspendLayout();
			this->groupBox5->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox5))->BeginInit();
			this->groupBox6->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox9))->BeginInit();
			this->Filter->SuspendLayout();
			this->SuspendLayout();
			// 
			// Amount2
			// 
			this->Amount2->AutoSize = true;
			this->Amount2->Location = System::Drawing::Point(652, 154);
			this->Amount2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Amount2->Name = L"Amount2";
			this->Amount2->Size = System::Drawing::Size(41, 13);
			this->Amount2->TabIndex = 19;
			this->Amount2->Text = L"label11";
			// 
			// Amount7
			// 
			this->Amount7->AutoSize = true;
			this->Amount7->Location = System::Drawing::Point(652, 158);
			this->Amount7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Amount7->Name = L"Amount7";
			this->Amount7->Size = System::Drawing::Size(13, 13);
			this->Amount7->TabIndex = 21;
			this->Amount7->Text = L"5";
			// 
			// label37
			// 
			this->label37->AutoSize = true;
			this->label37->Location = System::Drawing::Point(525, 158);
			this->label37->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label37->Name = L"label37";
			this->label37->Size = System::Drawing::Size(69, 13);
			this->label37->TabIndex = 20;
			this->label37->Text = L"Количество:";
			// 
			// Amount3
			// 
			this->Amount3->AutoSize = true;
			this->Amount3->Location = System::Drawing::Point(652, 158);
			this->Amount3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Amount3->Name = L"Amount3";
			this->Amount3->Size = System::Drawing::Size(41, 13);
			this->Amount3->TabIndex = 21;
			this->Amount3->Text = L"label11";
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Location = System::Drawing::Point(525, 158);
			this->label21->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(69, 13);
			this->label21->TabIndex = 20;
			this->label21->Text = L"Количество:";
			// 
			// Amount4
			// 
			this->Amount4->AutoSize = true;
			this->Amount4->Location = System::Drawing::Point(652, 158);
			this->Amount4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Amount4->Name = L"Amount4";
			this->Amount4->Size = System::Drawing::Size(41, 13);
			this->Amount4->TabIndex = 21;
			this->Amount4->Text = L"label11";
			// 
			// label25
			// 
			this->label25->AutoSize = true;
			this->label25->Location = System::Drawing::Point(525, 158);
			this->label25->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label25->Name = L"label25";
			this->label25->Size = System::Drawing::Size(69, 13);
			this->label25->TabIndex = 20;
			this->label25->Text = L"Количество:";
			// 
			// button8
			// 
			this->button8->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->button8->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button8->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button8->Location = System::Drawing::Point(750, 132);
			this->button8->Margin = System::Windows::Forms::Padding(2);
			this->button8->Name = L"button8";
			this->button8->Size = System::Drawing::Size(125, 39);
			this->button8->TabIndex = 15;
			this->button8->Text = L"Купить";
			this->button8->UseVisualStyleBackColor = false;
			this->button8->Click += gcnew System::EventHandler(this, &Catalog::appToBin);
			// 
			// Price7
			// 
			this->Price7->AutoSize = true;
			this->Price7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Price7->Location = System::Drawing::Point(750, 73);
			this->Price7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Price7->Name = L"Price7";
			this->Price7->Size = System::Drawing::Size(119, 25);
			this->Price7->TabIndex = 14;
			this->Price7->Text = L"10000 руб.";
			// 
			// label94
			// 
			this->label94->AutoSize = true;
			this->label94->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label94->Location = System::Drawing::Point(750, 32);
			this->label94->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label94->Name = L"label94";
			this->label94->Size = System::Drawing::Size(64, 25);
			this->label94->TabIndex = 13;
			this->label94->Text = L"Цена:";
			// 
			// HDD7
			// 
			this->HDD7->AutoSize = true;
			this->HDD7->Location = System::Drawing::Point(652, 102);
			this->HDD7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->HDD7->Name = L"HDD7";
			this->HDD7->Size = System::Drawing::Size(35, 13);
			this->HDD7->TabIndex = 12;
			this->HDD7->Text = L"label2";
			// 
			// label96
			// 
			this->label96->AutoSize = true;
			this->label96->Location = System::Drawing::Point(525, 102);
			this->label96->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label96->Name = L"label96";
			this->label96->Size = System::Drawing::Size(72, 13);
			this->label96->TabIndex = 11;
			this->label96->Text = L"Объём HDD:";
			// 
			// Motherboard7
			// 
			this->Motherboard7->AutoSize = true;
			this->Motherboard7->Location = System::Drawing::Point(330, 130);
			this->Motherboard7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Motherboard7->Name = L"Motherboard7";
			this->Motherboard7->Size = System::Drawing::Size(41, 13);
			this->Motherboard7->TabIndex = 10;
			this->Motherboard7->Text = L"label10";
			// 
			// label98
			// 
			this->label98->AutoSize = true;
			this->label98->Location = System::Drawing::Point(218, 130);
			this->label98->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label98->Name = L"label98";
			this->label98->Size = System::Drawing::Size(110, 13);
			this->label98->TabIndex = 9;
			this->label98->Text = L"Материнская плата:";
			// 
			// RAM7
			// 
			this->RAM7->AutoSize = true;
			this->RAM7->Location = System::Drawing::Point(652, 73);
			this->RAM7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->RAM7->Name = L"RAM7";
			this->RAM7->Size = System::Drawing::Size(35, 13);
			this->RAM7->TabIndex = 8;
			this->RAM7->Text = L"label8";
			// 
			// label100
			// 
			this->label100->AutoSize = true;
			this->label100->Location = System::Drawing::Point(525, 73);
			this->label100->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label100->Name = L"label100";
			this->label100->Size = System::Drawing::Size(71, 13);
			this->label100->TabIndex = 7;
			this->label100->Text = L"Объём ОЗУ:";
			// 
			// Video7
			// 
			this->Video7->AutoSize = true;
			this->Video7->Location = System::Drawing::Point(330, 102);
			this->Video7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Video7->Name = L"Video7";
			this->Video7->Size = System::Drawing::Size(35, 13);
			this->Video7->TabIndex = 6;
			this->Video7->Text = L"label6";
			// 
			// label102
			// 
			this->label102->AutoSize = true;
			this->label102->Location = System::Drawing::Point(218, 102);
			this->label102->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label102->Name = L"label102";
			this->label102->Size = System::Drawing::Size(70, 13);
			this->label102->TabIndex = 5;
			this->label102->Text = L"Видеокарта:";
			// 
			// CPU7
			// 
			this->CPU7->AutoSize = true;
			this->CPU7->Location = System::Drawing::Point(330, 73);
			this->CPU7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->CPU7->Name = L"CPU7";
			this->CPU7->Size = System::Drawing::Size(35, 13);
			this->CPU7->TabIndex = 4;
			this->CPU7->Text = L"label4";
			// 
			// Amount5
			// 
			this->Amount5->AutoSize = true;
			this->Amount5->Location = System::Drawing::Point(652, 158);
			this->Amount5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Amount5->Name = L"Amount5";
			this->Amount5->Size = System::Drawing::Size(41, 13);
			this->Amount5->TabIndex = 21;
			this->Amount5->Text = L"label11";
			// 
			// label29
			// 
			this->label29->AutoSize = true;
			this->label29->Location = System::Drawing::Point(525, 158);
			this->label29->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label29->Name = L"label29";
			this->label29->Size = System::Drawing::Size(69, 13);
			this->label29->TabIndex = 20;
			this->label29->Text = L"Количество:";
			// 
			// Amount6
			// 
			this->Amount6->AutoSize = true;
			this->Amount6->Location = System::Drawing::Point(652, 158);
			this->Amount6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Amount6->Name = L"Amount6";
			this->Amount6->Size = System::Drawing::Size(41, 13);
			this->Amount6->TabIndex = 21;
			this->Amount6->Text = L"label11";
			// 
			// label33
			// 
			this->label33->AutoSize = true;
			this->label33->Location = System::Drawing::Point(525, 158);
			this->label33->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label33->Name = L"label33";
			this->label33->Size = System::Drawing::Size(69, 13);
			this->label33->TabIndex = 20;
			this->label33->Text = L"Количество:";
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(525, 154);
			this->label17->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(69, 13);
			this->label17->TabIndex = 18;
			this->label17->Text = L"Количество:";
			// 
			// NameComp5
			// 
			this->NameComp5->AutoSize = true;
			this->NameComp5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->NameComp5->Location = System::Drawing::Point(218, 45);
			this->NameComp5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->NameComp5->Name = L"NameComp5";
			this->NameComp5->Size = System::Drawing::Size(46, 17);
			this->NameComp5->TabIndex = 2;
			this->NameComp5->Text = L"label2";
			// 
			// label104
			// 
			this->label104->AutoSize = true;
			this->label104->Location = System::Drawing::Point(218, 73);
			this->label104->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label104->Name = L"label104";
			this->label104->Size = System::Drawing::Size(66, 13);
			this->label104->TabIndex = 3;
			this->label104->Text = L"Процессор:";
			// 
			// NameComp7
			// 
			this->NameComp7->AutoSize = true;
			this->NameComp7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->NameComp7->Location = System::Drawing::Point(218, 45);
			this->NameComp7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->NameComp7->Name = L"NameComp7";
			this->NameComp7->Size = System::Drawing::Size(46, 17);
			this->NameComp7->TabIndex = 2;
			this->NameComp7->Text = L"label2";
			// 
			// label106
			// 
			this->label106->AutoSize = true;
			this->label106->Location = System::Drawing::Point(218, 16);
			this->label106->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label106->Name = L"label106";
			this->label106->Size = System::Drawing::Size(60, 13);
			this->label106->TabIndex = 1;
			this->label106->Text = L"Название:";
			// 
			// pictureBox8
			// 
			this->pictureBox8->Location = System::Drawing::Point(15, 16);
			this->pictureBox8->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox8->Name = L"pictureBox8";
			this->pictureBox8->Size = System::Drawing::Size(180, 195);
			this->pictureBox8->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox8->TabIndex = 0;
			this->pictureBox8->TabStop = false;
			// 
			// groupBox8
			// 
			this->groupBox8->Controls->Add(this->SSD7);
			this->groupBox8->Controls->Add(this->label75);
			this->groupBox8->Controls->Add(this->PSU7);
			this->groupBox8->Controls->Add(this->label79);
			this->groupBox8->Controls->Add(this->Amount7);
			this->groupBox8->Controls->Add(this->label37);
			this->groupBox8->Controls->Add(this->button8);
			this->groupBox8->Controls->Add(this->Price7);
			this->groupBox8->Controls->Add(this->label94);
			this->groupBox8->Controls->Add(this->HDD7);
			this->groupBox8->Controls->Add(this->label96);
			this->groupBox8->Controls->Add(this->Motherboard7);
			this->groupBox8->Controls->Add(this->label98);
			this->groupBox8->Controls->Add(this->RAM7);
			this->groupBox8->Controls->Add(this->label100);
			this->groupBox8->Controls->Add(this->Video7);
			this->groupBox8->Controls->Add(this->label102);
			this->groupBox8->Controls->Add(this->CPU7);
			this->groupBox8->Controls->Add(this->label104);
			this->groupBox8->Controls->Add(this->NameComp7);
			this->groupBox8->Controls->Add(this->label106);
			this->groupBox8->Controls->Add(this->pictureBox8);
			this->groupBox8->Location = System::Drawing::Point(19, 1641);
			this->groupBox8->Margin = System::Windows::Forms::Padding(2);
			this->groupBox8->Name = L"groupBox8";
			this->groupBox8->Padding = System::Windows::Forms::Padding(2);
			this->groupBox8->Size = System::Drawing::Size(900, 222);
			this->groupBox8->TabIndex = 37;
			this->groupBox8->TabStop = false;
			this->groupBox8->Visible = false;
			// 
			// SSD7
			// 
			this->SSD7->AutoSize = true;
			this->SSD7->Location = System::Drawing::Point(652, 130);
			this->SSD7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->SSD7->Name = L"SSD7";
			this->SSD7->Size = System::Drawing::Size(41, 13);
			this->SSD7->TabIndex = 52;
			this->SSD7->Text = L"label23";
			// 
			// label75
			// 
			this->label75->AutoSize = true;
			this->label75->Location = System::Drawing::Point(525, 129);
			this->label75->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label75->Name = L"label75";
			this->label75->Size = System::Drawing::Size(70, 13);
			this->label75->TabIndex = 51;
			this->label75->Text = L"Объём SSD:";
			// 
			// PSU7
			// 
			this->PSU7->AutoSize = true;
			this->PSU7->Location = System::Drawing::Point(330, 158);
			this->PSU7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->PSU7->Name = L"PSU7";
			this->PSU7->Size = System::Drawing::Size(41, 13);
			this->PSU7->TabIndex = 50;
			this->PSU7->Text = L"label23";
			// 
			// label79
			// 
			this->label79->AutoSize = true;
			this->label79->Location = System::Drawing::Point(218, 158);
			this->label79->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label79->Name = L"label79";
			this->label79->Size = System::Drawing::Size(81, 13);
			this->label79->TabIndex = 49;
			this->label79->Text = L"Мощность БП:";
			// 
			// button7
			// 
			this->button7->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->button7->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button7->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button7->Location = System::Drawing::Point(750, 130);
			this->button7->Margin = System::Windows::Forms::Padding(2);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(125, 39);
			this->button7->TabIndex = 15;
			this->button7->Text = L"Купить";
			this->button7->UseVisualStyleBackColor = false;
			this->button7->Click += gcnew System::EventHandler(this, &Catalog::appToBin);
			// 
			// groupBox7
			// 
			this->groupBox7->Controls->Add(this->SSD6);
			this->groupBox7->Controls->Add(this->label67);
			this->groupBox7->Controls->Add(this->PSU6);
			this->groupBox7->Controls->Add(this->label71);
			this->groupBox7->Controls->Add(this->Amount6);
			this->groupBox7->Controls->Add(this->label33);
			this->groupBox7->Controls->Add(this->button7);
			this->groupBox7->Controls->Add(this->Price6);
			this->groupBox7->Controls->Add(this->label80);
			this->groupBox7->Controls->Add(this->HDD6);
			this->groupBox7->Controls->Add(this->label82);
			this->groupBox7->Controls->Add(this->Motherboard6);
			this->groupBox7->Controls->Add(this->label84);
			this->groupBox7->Controls->Add(this->RAM6);
			this->groupBox7->Controls->Add(this->label86);
			this->groupBox7->Controls->Add(this->Video6);
			this->groupBox7->Controls->Add(this->label88);
			this->groupBox7->Controls->Add(this->CPU6);
			this->groupBox7->Controls->Add(this->label90);
			this->groupBox7->Controls->Add(this->NameComp6);
			this->groupBox7->Controls->Add(this->label92);
			this->groupBox7->Controls->Add(this->pictureBox7);
			this->groupBox7->Location = System::Drawing::Point(19, 1414);
			this->groupBox7->Margin = System::Windows::Forms::Padding(2);
			this->groupBox7->Name = L"groupBox7";
			this->groupBox7->Padding = System::Windows::Forms::Padding(2);
			this->groupBox7->Size = System::Drawing::Size(900, 222);
			this->groupBox7->TabIndex = 36;
			this->groupBox7->TabStop = false;
			this->groupBox7->Visible = false;
			// 
			// SSD6
			// 
			this->SSD6->AutoSize = true;
			this->SSD6->Location = System::Drawing::Point(652, 130);
			this->SSD6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->SSD6->Name = L"SSD6";
			this->SSD6->Size = System::Drawing::Size(41, 13);
			this->SSD6->TabIndex = 48;
			this->SSD6->Text = L"label23";
			// 
			// label67
			// 
			this->label67->AutoSize = true;
			this->label67->Location = System::Drawing::Point(525, 130);
			this->label67->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label67->Name = L"label67";
			this->label67->Size = System::Drawing::Size(70, 13);
			this->label67->TabIndex = 47;
			this->label67->Text = L"Объём SSD:";
			// 
			// PSU6
			// 
			this->PSU6->AutoSize = true;
			this->PSU6->Location = System::Drawing::Point(330, 158);
			this->PSU6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->PSU6->Name = L"PSU6";
			this->PSU6->Size = System::Drawing::Size(41, 13);
			this->PSU6->TabIndex = 46;
			this->PSU6->Text = L"label23";
			// 
			// label71
			// 
			this->label71->AutoSize = true;
			this->label71->Location = System::Drawing::Point(218, 158);
			this->label71->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label71->Name = L"label71";
			this->label71->Size = System::Drawing::Size(81, 13);
			this->label71->TabIndex = 45;
			this->label71->Text = L"Мощность БП:";
			// 
			// Price6
			// 
			this->Price6->AutoSize = true;
			this->Price6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Price6->Location = System::Drawing::Point(750, 73);
			this->Price6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Price6->Name = L"Price6";
			this->Price6->Size = System::Drawing::Size(119, 25);
			this->Price6->TabIndex = 14;
			this->Price6->Text = L"10000 руб.";
			// 
			// label80
			// 
			this->label80->AutoSize = true;
			this->label80->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label80->Location = System::Drawing::Point(750, 32);
			this->label80->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label80->Name = L"label80";
			this->label80->Size = System::Drawing::Size(64, 25);
			this->label80->TabIndex = 13;
			this->label80->Text = L"Цена:";
			// 
			// HDD6
			// 
			this->HDD6->AutoSize = true;
			this->HDD6->Location = System::Drawing::Point(652, 102);
			this->HDD6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->HDD6->Name = L"HDD6";
			this->HDD6->Size = System::Drawing::Size(35, 13);
			this->HDD6->TabIndex = 12;
			this->HDD6->Text = L"label2";
			// 
			// label82
			// 
			this->label82->AutoSize = true;
			this->label82->Location = System::Drawing::Point(525, 102);
			this->label82->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label82->Name = L"label82";
			this->label82->Size = System::Drawing::Size(72, 13);
			this->label82->TabIndex = 11;
			this->label82->Text = L"Объём HDD:";
			// 
			// Motherboard6
			// 
			this->Motherboard6->AutoSize = true;
			this->Motherboard6->Location = System::Drawing::Point(330, 130);
			this->Motherboard6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Motherboard6->Name = L"Motherboard6";
			this->Motherboard6->Size = System::Drawing::Size(41, 13);
			this->Motherboard6->TabIndex = 10;
			this->Motherboard6->Text = L"label10";
			// 
			// label84
			// 
			this->label84->AutoSize = true;
			this->label84->Location = System::Drawing::Point(218, 130);
			this->label84->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label84->Name = L"label84";
			this->label84->Size = System::Drawing::Size(110, 13);
			this->label84->TabIndex = 9;
			this->label84->Text = L"Материнская плата:";
			// 
			// RAM6
			// 
			this->RAM6->AutoSize = true;
			this->RAM6->Location = System::Drawing::Point(652, 73);
			this->RAM6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->RAM6->Name = L"RAM6";
			this->RAM6->Size = System::Drawing::Size(35, 13);
			this->RAM6->TabIndex = 8;
			this->RAM6->Text = L"label8";
			// 
			// label86
			// 
			this->label86->AutoSize = true;
			this->label86->Location = System::Drawing::Point(525, 73);
			this->label86->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label86->Name = L"label86";
			this->label86->Size = System::Drawing::Size(71, 13);
			this->label86->TabIndex = 7;
			this->label86->Text = L"Объём ОЗУ:";
			// 
			// Video6
			// 
			this->Video6->AutoSize = true;
			this->Video6->Location = System::Drawing::Point(330, 102);
			this->Video6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Video6->Name = L"Video6";
			this->Video6->Size = System::Drawing::Size(35, 13);
			this->Video6->TabIndex = 6;
			this->Video6->Text = L"label6";
			// 
			// label88
			// 
			this->label88->AutoSize = true;
			this->label88->Location = System::Drawing::Point(218, 102);
			this->label88->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label88->Name = L"label88";
			this->label88->Size = System::Drawing::Size(70, 13);
			this->label88->TabIndex = 5;
			this->label88->Text = L"Видеокарта:";
			// 
			// CPU6
			// 
			this->CPU6->AutoSize = true;
			this->CPU6->Location = System::Drawing::Point(330, 73);
			this->CPU6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->CPU6->Name = L"CPU6";
			this->CPU6->Size = System::Drawing::Size(35, 13);
			this->CPU6->TabIndex = 4;
			this->CPU6->Text = L"label4";
			// 
			// label90
			// 
			this->label90->AutoSize = true;
			this->label90->Location = System::Drawing::Point(218, 73);
			this->label90->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label90->Name = L"label90";
			this->label90->Size = System::Drawing::Size(66, 13);
			this->label90->TabIndex = 3;
			this->label90->Text = L"Процессор:";
			// 
			// NameComp6
			// 
			this->NameComp6->AutoSize = true;
			this->NameComp6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->NameComp6->Location = System::Drawing::Point(218, 45);
			this->NameComp6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->NameComp6->Name = L"NameComp6";
			this->NameComp6->Size = System::Drawing::Size(46, 17);
			this->NameComp6->TabIndex = 2;
			this->NameComp6->Text = L"label2";
			// 
			// label92
			// 
			this->label92->AutoSize = true;
			this->label92->Location = System::Drawing::Point(218, 16);
			this->label92->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label92->Name = L"label92";
			this->label92->Size = System::Drawing::Size(60, 13);
			this->label92->TabIndex = 1;
			this->label92->Text = L"Название:";
			// 
			// pictureBox7
			// 
			this->pictureBox7->Location = System::Drawing::Point(15, 16);
			this->pictureBox7->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox7->Name = L"pictureBox7";
			this->pictureBox7->Size = System::Drawing::Size(180, 195);
			this->pictureBox7->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox7->TabIndex = 0;
			this->pictureBox7->TabStop = false;
			// 
			// label78
			// 
			this->label78->AutoSize = true;
			this->label78->Location = System::Drawing::Point(218, 16);
			this->label78->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label78->Name = L"label78";
			this->label78->Size = System::Drawing::Size(60, 13);
			this->label78->TabIndex = 1;
			this->label78->Text = L"Название:";
			// 
			// pictureBox6
			// 
			this->pictureBox6->Location = System::Drawing::Point(15, 16);
			this->pictureBox6->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox6->Name = L"pictureBox6";
			this->pictureBox6->Size = System::Drawing::Size(180, 195);
			this->pictureBox6->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox6->TabIndex = 0;
			this->pictureBox6->TabStop = false;
			// 
			// label76
			// 
			this->label76->AutoSize = true;
			this->label76->Location = System::Drawing::Point(218, 73);
			this->label76->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label76->Name = L"label76";
			this->label76->Size = System::Drawing::Size(66, 13);
			this->label76->TabIndex = 3;
			this->label76->Text = L"Процессор:";
			// 
			// Amount0
			// 
			this->Amount0->AutoSize = true;
			this->Amount0->Location = System::Drawing::Point(652, 158);
			this->Amount0->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Amount0->Name = L"Amount0";
			this->Amount0->Size = System::Drawing::Size(13, 13);
			this->Amount0->TabIndex = 17;
			this->Amount0->Text = L"3";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(525, 158);
			this->label9->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(69, 13);
			this->label9->TabIndex = 16;
			this->label9->Text = L"Количество:";
			// 
			// button1
			// 
			this->button1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->button1->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button1->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button1->Location = System::Drawing::Point(750, 134);
			this->button1->Margin = System::Windows::Forms::Padding(2);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(125, 39);
			this->button1->TabIndex = 15;
			this->button1->Text = L"Купить";
			this->button1->UseVisualStyleBackColor = false;
			this->button1->Click += gcnew System::EventHandler(this, &Catalog::appToBin);
			// 
			// Price0
			// 
			this->Price0->AutoSize = true;
			this->Price0->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Price0->Location = System::Drawing::Point(750, 73);
			this->Price0->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Price0->Name = L"Price0";
			this->Price0->Size = System::Drawing::Size(119, 25);
			this->Price0->TabIndex = 14;
			this->Price0->Text = L"10000 руб.";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label8->Location = System::Drawing::Point(751, 33);
			this->label8->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(64, 25);
			this->label8->TabIndex = 13;
			this->label8->Text = L"Цена:";
			// 
			// HDD0
			// 
			this->HDD0->AutoSize = true;
			this->HDD0->Location = System::Drawing::Point(652, 102);
			this->HDD0->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->HDD0->Name = L"HDD0";
			this->HDD0->Size = System::Drawing::Size(35, 13);
			this->HDD0->TabIndex = 12;
			this->HDD0->Text = L"label2";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(525, 102);
			this->label7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(72, 13);
			this->label7->TabIndex = 11;
			this->label7->Text = L"Объём HDD:";
			// 
			// Motherboard0
			// 
			this->Motherboard0->AutoSize = true;
			this->Motherboard0->Location = System::Drawing::Point(329, 130);
			this->Motherboard0->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Motherboard0->Name = L"Motherboard0";
			this->Motherboard0->Size = System::Drawing::Size(41, 13);
			this->Motherboard0->TabIndex = 10;
			this->Motherboard0->Text = L"label10";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(218, 130);
			this->label6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(110, 13);
			this->label6->TabIndex = 9;
			this->label6->Text = L"Материнская плата:";
			// 
			// RAM0
			// 
			this->RAM0->AutoSize = true;
			this->RAM0->Location = System::Drawing::Point(652, 72);
			this->RAM0->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->RAM0->Name = L"RAM0";
			this->RAM0->Size = System::Drawing::Size(35, 13);
			this->RAM0->TabIndex = 8;
			this->RAM0->Text = L"label8";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(525, 73);
			this->label5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(71, 13);
			this->label5->TabIndex = 7;
			this->label5->Text = L"Объём ОЗУ:";
			// 
			// Video0
			// 
			this->Video0->AutoSize = true;
			this->Video0->Location = System::Drawing::Point(330, 102);
			this->Video0->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Video0->Name = L"Video0";
			this->Video0->Size = System::Drawing::Size(35, 13);
			this->Video0->TabIndex = 6;
			this->Video0->Text = L"label6";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(218, 102);
			this->label4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(70, 13);
			this->label4->TabIndex = 5;
			this->label4->Text = L"Видеокарта:";
			// 
			// CPU0
			// 
			this->CPU0->AutoSize = true;
			this->CPU0->Location = System::Drawing::Point(330, 73);
			this->CPU0->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->CPU0->Name = L"CPU0";
			this->CPU0->Size = System::Drawing::Size(35, 13);
			this->CPU0->TabIndex = 4;
			this->CPU0->Text = L"label4";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(218, 73);
			this->label3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(66, 13);
			this->label3->TabIndex = 3;
			this->label3->Text = L"Процессор:";
			// 
			// NameComp0
			// 
			this->NameComp0->AutoSize = true;
			this->NameComp0->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->NameComp0->Location = System::Drawing::Point(217, 41);
			this->NameComp0->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->NameComp0->Name = L"NameComp0";
			this->NameComp0->Size = System::Drawing::Size(46, 17);
			this->NameComp0->TabIndex = 2;
			this->NameComp0->Text = L"label2";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(218, 16);
			this->label2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(60, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Название:";
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->SSD0);
			this->groupBox1->Controls->Add(this->label15);
			this->groupBox1->Controls->Add(this->PSU0);
			this->groupBox1->Controls->Add(this->label19);
			this->groupBox1->Controls->Add(this->Amount0);
			this->groupBox1->Controls->Add(this->label9);
			this->groupBox1->Controls->Add(this->button1);
			this->groupBox1->Controls->Add(this->Price0);
			this->groupBox1->Controls->Add(this->label8);
			this->groupBox1->Controls->Add(this->HDD0);
			this->groupBox1->Controls->Add(this->label7);
			this->groupBox1->Controls->Add(this->Motherboard0);
			this->groupBox1->Controls->Add(this->label6);
			this->groupBox1->Controls->Add(this->RAM0);
			this->groupBox1->Controls->Add(this->label5);
			this->groupBox1->Controls->Add(this->Video0);
			this->groupBox1->Controls->Add(this->label4);
			this->groupBox1->Controls->Add(this->CPU0);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->NameComp0);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->pictureBox1);
			this->groupBox1->Location = System::Drawing::Point(19, 49);
			this->groupBox1->Margin = System::Windows::Forms::Padding(2);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Padding = System::Windows::Forms::Padding(2);
			this->groupBox1->Size = System::Drawing::Size(900, 222);
			this->groupBox1->TabIndex = 28;
			this->groupBox1->TabStop = false;
			// 
			// SSD0
			// 
			this->SSD0->AutoSize = true;
			this->SSD0->Location = System::Drawing::Point(652, 130);
			this->SSD0->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->SSD0->Name = L"SSD0";
			this->SSD0->Size = System::Drawing::Size(41, 13);
			this->SSD0->TabIndex = 26;
			this->SSD0->Text = L"label23";
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(525, 130);
			this->label15->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(70, 13);
			this->label15->TabIndex = 25;
			this->label15->Text = L"Объём SSD:";
			// 
			// PSU0
			// 
			this->PSU0->AutoSize = true;
			this->PSU0->Location = System::Drawing::Point(330, 158);
			this->PSU0->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->PSU0->Name = L"PSU0";
			this->PSU0->Size = System::Drawing::Size(41, 13);
			this->PSU0->TabIndex = 24;
			this->PSU0->Text = L"label23";
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(218, 158);
			this->label19->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(81, 13);
			this->label19->TabIndex = 23;
			this->label19->Text = L"Мощность БП:";
			// 
			// pictureBox1
			// 
			this->pictureBox1->ImageLocation = L"";
			this->pictureBox1->Location = System::Drawing::Point(20, 20);
			this->pictureBox1->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(180, 195);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			// 
			// AlertFilter
			// 
			this->AlertFilter->AutoSize = true;
			this->AlertFilter->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 13.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->AlertFilter->Location = System::Drawing::Point(212, 19);
			this->AlertFilter->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->AlertFilter->Name = L"AlertFilter";
			this->AlertFilter->Size = System::Drawing::Size(487, 24);
			this->AlertFilter->TabIndex = 46;
			this->AlertFilter->Text = L"Товаров с такими критериями поиска не существует";
			// 
			// Amount1
			// 
			this->Amount1->AutoSize = true;
			this->Amount1->Location = System::Drawing::Point(652, 158);
			this->Amount1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Amount1->Name = L"Amount1";
			this->Amount1->Size = System::Drawing::Size(41, 13);
			this->Amount1->TabIndex = 19;
			this->Amount1->Text = L"label11";
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(525, 158);
			this->label13->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(69, 13);
			this->label13->TabIndex = 18;
			this->label13->Text = L"Количество:";
			// 
			// pictureBox2
			// 
			this->pictureBox2->Location = System::Drawing::Point(15, 16);
			this->pictureBox2->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(180, 195);
			this->pictureBox2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox2->TabIndex = 0;
			this->pictureBox2->TabStop = false;
			// 
			// button3
			// 
			this->button3->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->button3->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button3->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button3->Location = System::Drawing::Point(750, 128);
			this->button3->Margin = System::Windows::Forms::Padding(2);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(125, 39);
			this->button3->TabIndex = 15;
			this->button3->Text = L"Купить";
			this->button3->UseVisualStyleBackColor = false;
			this->button3->Click += gcnew System::EventHandler(this, &Catalog::appToBin);
			// 
			// Price2
			// 
			this->Price2->AutoSize = true;
			this->Price2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Price2->Location = System::Drawing::Point(750, 73);
			this->Price2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Price2->Name = L"Price2";
			this->Price2->Size = System::Drawing::Size(119, 25);
			this->Price2->TabIndex = 14;
			this->Price2->Text = L"10000 руб.";
			// 
			// label24
			// 
			this->label24->AutoSize = true;
			this->label24->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label24->Location = System::Drawing::Point(750, 32);
			this->label24->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label24->Name = L"label24";
			this->label24->Size = System::Drawing::Size(64, 25);
			this->label24->TabIndex = 13;
			this->label24->Text = L"Цена:";
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->SSD3);
			this->groupBox4->Controls->Add(this->label47);
			this->groupBox4->Controls->Add(this->PSU3);
			this->groupBox4->Controls->Add(this->label43);
			this->groupBox4->Controls->Add(this->Amount3);
			this->groupBox4->Controls->Add(this->label21);
			this->groupBox4->Controls->Add(this->button4);
			this->groupBox4->Controls->Add(this->Price3);
			this->groupBox4->Controls->Add(this->label38);
			this->groupBox4->Controls->Add(this->HDD3);
			this->groupBox4->Controls->Add(this->label40);
			this->groupBox4->Controls->Add(this->Motherboard3);
			this->groupBox4->Controls->Add(this->label42);
			this->groupBox4->Controls->Add(this->RAM3);
			this->groupBox4->Controls->Add(this->label44);
			this->groupBox4->Controls->Add(this->Video3);
			this->groupBox4->Controls->Add(this->label46);
			this->groupBox4->Controls->Add(this->CPU3);
			this->groupBox4->Controls->Add(this->label48);
			this->groupBox4->Controls->Add(this->NameComp3);
			this->groupBox4->Controls->Add(this->label50);
			this->groupBox4->Controls->Add(this->pictureBox4);
			this->groupBox4->Location = System::Drawing::Point(19, 731);
			this->groupBox4->Margin = System::Windows::Forms::Padding(2);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Padding = System::Windows::Forms::Padding(2);
			this->groupBox4->Size = System::Drawing::Size(900, 222);
			this->groupBox4->TabIndex = 33;
			this->groupBox4->TabStop = false;
			this->groupBox4->Visible = false;
			// 
			// SSD3
			// 
			this->SSD3->AutoSize = true;
			this->SSD3->Location = System::Drawing::Point(652, 130);
			this->SSD3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->SSD3->Name = L"SSD3";
			this->SSD3->Size = System::Drawing::Size(41, 13);
			this->SSD3->TabIndex = 38;
			this->SSD3->Text = L"label23";
			// 
			// label47
			// 
			this->label47->AutoSize = true;
			this->label47->Location = System::Drawing::Point(525, 130);
			this->label47->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label47->Name = L"label47";
			this->label47->Size = System::Drawing::Size(70, 13);
			this->label47->TabIndex = 37;
			this->label47->Text = L"Объём SSD:";
			// 
			// PSU3
			// 
			this->PSU3->AutoSize = true;
			this->PSU3->Location = System::Drawing::Point(332, 158);
			this->PSU3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->PSU3->Name = L"PSU3";
			this->PSU3->Size = System::Drawing::Size(41, 13);
			this->PSU3->TabIndex = 36;
			this->PSU3->Text = L"label23";
			// 
			// label43
			// 
			this->label43->AutoSize = true;
			this->label43->Location = System::Drawing::Point(219, 158);
			this->label43->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label43->Name = L"label43";
			this->label43->Size = System::Drawing::Size(81, 13);
			this->label43->TabIndex = 35;
			this->label43->Text = L"Мощность БП:";
			// 
			// button4
			// 
			this->button4->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->button4->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button4->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button4->Location = System::Drawing::Point(750, 130);
			this->button4->Margin = System::Windows::Forms::Padding(2);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(125, 39);
			this->button4->TabIndex = 15;
			this->button4->Text = L"Купить";
			this->button4->UseVisualStyleBackColor = false;
			this->button4->Click += gcnew System::EventHandler(this, &Catalog::appToBin);
			// 
			// Price3
			// 
			this->Price3->AutoSize = true;
			this->Price3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Price3->Location = System::Drawing::Point(750, 73);
			this->Price3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Price3->Name = L"Price3";
			this->Price3->Size = System::Drawing::Size(119, 25);
			this->Price3->TabIndex = 14;
			this->Price3->Text = L"10000 руб.";
			// 
			// label38
			// 
			this->label38->AutoSize = true;
			this->label38->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label38->Location = System::Drawing::Point(750, 32);
			this->label38->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label38->Name = L"label38";
			this->label38->Size = System::Drawing::Size(64, 25);
			this->label38->TabIndex = 13;
			this->label38->Text = L"Цена:";
			// 
			// HDD3
			// 
			this->HDD3->AutoSize = true;
			this->HDD3->Location = System::Drawing::Point(656, 102);
			this->HDD3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->HDD3->Name = L"HDD3";
			this->HDD3->Size = System::Drawing::Size(35, 13);
			this->HDD3->TabIndex = 12;
			this->HDD3->Text = L"label2";
			// 
			// label40
			// 
			this->label40->AutoSize = true;
			this->label40->Location = System::Drawing::Point(525, 102);
			this->label40->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label40->Name = L"label40";
			this->label40->Size = System::Drawing::Size(72, 13);
			this->label40->TabIndex = 11;
			this->label40->Text = L"Объём HDD:";
			// 
			// Motherboard3
			// 
			this->Motherboard3->AutoSize = true;
			this->Motherboard3->Location = System::Drawing::Point(330, 130);
			this->Motherboard3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Motherboard3->Name = L"Motherboard3";
			this->Motherboard3->Size = System::Drawing::Size(41, 13);
			this->Motherboard3->TabIndex = 10;
			this->Motherboard3->Text = L"label10";
			// 
			// label42
			// 
			this->label42->AutoSize = true;
			this->label42->Location = System::Drawing::Point(218, 130);
			this->label42->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label42->Name = L"label42";
			this->label42->Size = System::Drawing::Size(110, 13);
			this->label42->TabIndex = 9;
			this->label42->Text = L"Материнская плата:";
			// 
			// RAM3
			// 
			this->RAM3->AutoSize = true;
			this->RAM3->Location = System::Drawing::Point(657, 73);
			this->RAM3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->RAM3->Name = L"RAM3";
			this->RAM3->Size = System::Drawing::Size(37, 13);
			this->RAM3->TabIndex = 8;
			this->RAM3->Text = L"RAM3";
			// 
			// label44
			// 
			this->label44->AutoSize = true;
			this->label44->Location = System::Drawing::Point(525, 73);
			this->label44->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label44->Name = L"label44";
			this->label44->Size = System::Drawing::Size(71, 13);
			this->label44->TabIndex = 7;
			this->label44->Text = L"Объём ОЗУ:";
			// 
			// Video3
			// 
			this->Video3->AutoSize = true;
			this->Video3->Location = System::Drawing::Point(330, 102);
			this->Video3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Video3->Name = L"Video3";
			this->Video3->Size = System::Drawing::Size(35, 13);
			this->Video3->TabIndex = 6;
			this->Video3->Text = L"label6";
			// 
			// label46
			// 
			this->label46->AutoSize = true;
			this->label46->Location = System::Drawing::Point(218, 102);
			this->label46->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label46->Name = L"label46";
			this->label46->Size = System::Drawing::Size(70, 13);
			this->label46->TabIndex = 5;
			this->label46->Text = L"Видеокарта:";
			// 
			// CPU3
			// 
			this->CPU3->AutoSize = true;
			this->CPU3->Location = System::Drawing::Point(330, 73);
			this->CPU3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->CPU3->Name = L"CPU3";
			this->CPU3->Size = System::Drawing::Size(35, 13);
			this->CPU3->TabIndex = 4;
			this->CPU3->Text = L"label4";
			// 
			// label48
			// 
			this->label48->AutoSize = true;
			this->label48->Location = System::Drawing::Point(218, 73);
			this->label48->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label48->Name = L"label48";
			this->label48->Size = System::Drawing::Size(66, 13);
			this->label48->TabIndex = 3;
			this->label48->Text = L"Процессор:";
			// 
			// NameComp3
			// 
			this->NameComp3->AutoSize = true;
			this->NameComp3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->NameComp3->Location = System::Drawing::Point(218, 45);
			this->NameComp3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->NameComp3->Name = L"NameComp3";
			this->NameComp3->Size = System::Drawing::Size(46, 17);
			this->NameComp3->TabIndex = 2;
			this->NameComp3->Text = L"label2";
			// 
			// label50
			// 
			this->label50->AutoSize = true;
			this->label50->Location = System::Drawing::Point(218, 16);
			this->label50->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label50->Name = L"label50";
			this->label50->Size = System::Drawing::Size(60, 13);
			this->label50->TabIndex = 1;
			this->label50->Text = L"Название:";
			// 
			// pictureBox4
			// 
			this->pictureBox4->Location = System::Drawing::Point(15, 16);
			this->pictureBox4->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox4->Name = L"pictureBox4";
			this->pictureBox4->Size = System::Drawing::Size(180, 195);
			this->pictureBox4->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox4->TabIndex = 0;
			this->pictureBox4->TabStop = false;
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->PSU2);
			this->groupBox3->Controls->Add(this->label39);
			this->groupBox3->Controls->Add(this->SSD2);
			this->groupBox3->Controls->Add(this->label23);
			this->groupBox3->Controls->Add(this->Amount2);
			this->groupBox3->Controls->Add(this->label17);
			this->groupBox3->Controls->Add(this->button3);
			this->groupBox3->Controls->Add(this->Price2);
			this->groupBox3->Controls->Add(this->label24);
			this->groupBox3->Controls->Add(this->HDD2);
			this->groupBox3->Controls->Add(this->label26);
			this->groupBox3->Controls->Add(this->Motherboard2);
			this->groupBox3->Controls->Add(this->label28);
			this->groupBox3->Controls->Add(this->RAM2);
			this->groupBox3->Controls->Add(this->label30);
			this->groupBox3->Controls->Add(this->Video2);
			this->groupBox3->Controls->Add(this->label32);
			this->groupBox3->Controls->Add(this->CPU2);
			this->groupBox3->Controls->Add(this->label34);
			this->groupBox3->Controls->Add(this->NameComp2);
			this->groupBox3->Controls->Add(this->label36);
			this->groupBox3->Controls->Add(this->pictureBox3);
			this->groupBox3->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->groupBox3->Location = System::Drawing::Point(19, 504);
			this->groupBox3->Margin = System::Windows::Forms::Padding(2);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Padding = System::Windows::Forms::Padding(2);
			this->groupBox3->Size = System::Drawing::Size(900, 222);
			this->groupBox3->TabIndex = 29;
			this->groupBox3->TabStop = false;
			this->groupBox3->Visible = false;
			// 
			// PSU2
			// 
			this->PSU2->AutoSize = true;
			this->PSU2->Location = System::Drawing::Point(330, 154);
			this->PSU2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->PSU2->Name = L"PSU2";
			this->PSU2->Size = System::Drawing::Size(41, 13);
			this->PSU2->TabIndex = 34;
			this->PSU2->Text = L"label23";
			// 
			// label39
			// 
			this->label39->AutoSize = true;
			this->label39->Location = System::Drawing::Point(218, 154);
			this->label39->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label39->Name = L"label39";
			this->label39->Size = System::Drawing::Size(81, 13);
			this->label39->TabIndex = 33;
			this->label39->Text = L"Мощность БП:";
			// 
			// SSD2
			// 
			this->SSD2->AutoSize = true;
			this->SSD2->Location = System::Drawing::Point(652, 130);
			this->SSD2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->SSD2->Name = L"SSD2";
			this->SSD2->Size = System::Drawing::Size(41, 13);
			this->SSD2->TabIndex = 30;
			this->SSD2->Text = L"label23";
			// 
			// label23
			// 
			this->label23->AutoSize = true;
			this->label23->Location = System::Drawing::Point(525, 130);
			this->label23->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label23->Name = L"label23";
			this->label23->Size = System::Drawing::Size(70, 13);
			this->label23->TabIndex = 29;
			this->label23->Text = L"Объём SSD:";
			// 
			// HDD2
			// 
			this->HDD2->AutoSize = true;
			this->HDD2->Location = System::Drawing::Point(652, 102);
			this->HDD2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->HDD2->Name = L"HDD2";
			this->HDD2->Size = System::Drawing::Size(35, 13);
			this->HDD2->TabIndex = 12;
			this->HDD2->Text = L"label2";
			// 
			// label26
			// 
			this->label26->AutoSize = true;
			this->label26->Location = System::Drawing::Point(525, 102);
			this->label26->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label26->Name = L"label26";
			this->label26->Size = System::Drawing::Size(72, 13);
			this->label26->TabIndex = 11;
			this->label26->Text = L"Объём HDD:";
			// 
			// Motherboard2
			// 
			this->Motherboard2->AutoSize = true;
			this->Motherboard2->Location = System::Drawing::Point(330, 130);
			this->Motherboard2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Motherboard2->Name = L"Motherboard2";
			this->Motherboard2->Size = System::Drawing::Size(41, 13);
			this->Motherboard2->TabIndex = 10;
			this->Motherboard2->Text = L"label10";
			// 
			// label28
			// 
			this->label28->AutoSize = true;
			this->label28->Location = System::Drawing::Point(218, 130);
			this->label28->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label28->Name = L"label28";
			this->label28->Size = System::Drawing::Size(110, 13);
			this->label28->TabIndex = 9;
			this->label28->Text = L"Материнская плата:";
			// 
			// RAM2
			// 
			this->RAM2->AutoSize = true;
			this->RAM2->Location = System::Drawing::Point(652, 73);
			this->RAM2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->RAM2->Name = L"RAM2";
			this->RAM2->Size = System::Drawing::Size(35, 13);
			this->RAM2->TabIndex = 8;
			this->RAM2->Text = L"label8";
			// 
			// label30
			// 
			this->label30->AutoSize = true;
			this->label30->Location = System::Drawing::Point(525, 73);
			this->label30->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label30->Name = L"label30";
			this->label30->Size = System::Drawing::Size(71, 13);
			this->label30->TabIndex = 7;
			this->label30->Text = L"Объём ОЗУ:";
			// 
			// Video2
			// 
			this->Video2->AutoSize = true;
			this->Video2->Location = System::Drawing::Point(330, 102);
			this->Video2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Video2->Name = L"Video2";
			this->Video2->Size = System::Drawing::Size(35, 13);
			this->Video2->TabIndex = 6;
			this->Video2->Text = L"label6";
			// 
			// label32
			// 
			this->label32->AutoSize = true;
			this->label32->Location = System::Drawing::Point(218, 102);
			this->label32->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label32->Name = L"label32";
			this->label32->Size = System::Drawing::Size(70, 13);
			this->label32->TabIndex = 5;
			this->label32->Text = L"Видеокарта:";
			// 
			// CPU2
			// 
			this->CPU2->AutoSize = true;
			this->CPU2->Location = System::Drawing::Point(330, 73);
			this->CPU2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->CPU2->Name = L"CPU2";
			this->CPU2->Size = System::Drawing::Size(35, 13);
			this->CPU2->TabIndex = 4;
			this->CPU2->Text = L"label4";
			// 
			// label34
			// 
			this->label34->AutoSize = true;
			this->label34->Location = System::Drawing::Point(218, 73);
			this->label34->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label34->Name = L"label34";
			this->label34->Size = System::Drawing::Size(66, 13);
			this->label34->TabIndex = 3;
			this->label34->Text = L"Процессор:";
			// 
			// NameComp2
			// 
			this->NameComp2->AutoSize = true;
			this->NameComp2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->NameComp2->Location = System::Drawing::Point(218, 45);
			this->NameComp2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->NameComp2->Name = L"NameComp2";
			this->NameComp2->Size = System::Drawing::Size(46, 17);
			this->NameComp2->TabIndex = 2;
			this->NameComp2->Text = L"label2";
			// 
			// label36
			// 
			this->label36->AutoSize = true;
			this->label36->Location = System::Drawing::Point(218, 16);
			this->label36->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label36->Name = L"label36";
			this->label36->Size = System::Drawing::Size(60, 13);
			this->label36->TabIndex = 1;
			this->label36->Text = L"Название:";
			// 
			// pictureBox3
			// 
			this->pictureBox3->Location = System::Drawing::Point(15, 16);
			this->pictureBox3->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox3->Name = L"pictureBox3";
			this->pictureBox3->Size = System::Drawing::Size(180, 195);
			this->pictureBox3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox3->TabIndex = 0;
			this->pictureBox3->TabStop = false;
			// 
			// CPU5
			// 
			this->CPU5->AutoSize = true;
			this->CPU5->Location = System::Drawing::Point(330, 73);
			this->CPU5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->CPU5->Name = L"CPU5";
			this->CPU5->Size = System::Drawing::Size(35, 13);
			this->CPU5->TabIndex = 4;
			this->CPU5->Text = L"label4";
			// 
			// HDD1
			// 
			this->HDD1->AutoSize = true;
			this->HDD1->Location = System::Drawing::Point(652, 102);
			this->HDD1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->HDD1->Name = L"HDD1";
			this->HDD1->Size = System::Drawing::Size(35, 13);
			this->HDD1->TabIndex = 12;
			this->HDD1->Text = L"label2";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(525, 102);
			this->label12->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(72, 13);
			this->label12->TabIndex = 11;
			this->label12->Text = L"Объём HDD:";
			// 
			// Motherboard1
			// 
			this->Motherboard1->AutoSize = true;
			this->Motherboard1->Location = System::Drawing::Point(330, 130);
			this->Motherboard1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Motherboard1->Name = L"Motherboard1";
			this->Motherboard1->Size = System::Drawing::Size(41, 13);
			this->Motherboard1->TabIndex = 10;
			this->Motherboard1->Text = L"label10";
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(218, 130);
			this->label14->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(110, 13);
			this->label14->TabIndex = 9;
			this->label14->Text = L"Материнская плата:";
			// 
			// button2
			// 
			this->button2->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->button2->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button2->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button2->Location = System::Drawing::Point(750, 132);
			this->button2->Margin = System::Windows::Forms::Padding(2);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(125, 39);
			this->button2->TabIndex = 15;
			this->button2->Text = L"Купить";
			this->button2->UseVisualStyleBackColor = false;
			this->button2->Click += gcnew System::EventHandler(this, &Catalog::appToBin);
			// 
			// Price1
			// 
			this->Price1->AutoSize = true;
			this->Price1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Price1->Location = System::Drawing::Point(751, 73);
			this->Price1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Price1->Name = L"Price1";
			this->Price1->Size = System::Drawing::Size(119, 25);
			this->Price1->TabIndex = 14;
			this->Price1->Text = L"10000 руб.";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->PSU1);
			this->groupBox2->Controls->Add(this->label35);
			this->groupBox2->Controls->Add(this->SSD1);
			this->groupBox2->Controls->Add(this->label27);
			this->groupBox2->Controls->Add(this->Amount1);
			this->groupBox2->Controls->Add(this->label13);
			this->groupBox2->Controls->Add(this->button2);
			this->groupBox2->Controls->Add(this->Price1);
			this->groupBox2->Controls->Add(this->label10);
			this->groupBox2->Controls->Add(this->HDD1);
			this->groupBox2->Controls->Add(this->label12);
			this->groupBox2->Controls->Add(this->Motherboard1);
			this->groupBox2->Controls->Add(this->label14);
			this->groupBox2->Controls->Add(this->RAM1);
			this->groupBox2->Controls->Add(this->label16);
			this->groupBox2->Controls->Add(this->Video1);
			this->groupBox2->Controls->Add(this->label18);
			this->groupBox2->Controls->Add(this->CPU1);
			this->groupBox2->Controls->Add(this->label20);
			this->groupBox2->Controls->Add(this->NameComp1);
			this->groupBox2->Controls->Add(this->label22);
			this->groupBox2->Controls->Add(this->pictureBox2);
			this->groupBox2->Location = System::Drawing::Point(19, 276);
			this->groupBox2->Margin = System::Windows::Forms::Padding(2);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Padding = System::Windows::Forms::Padding(2);
			this->groupBox2->Size = System::Drawing::Size(900, 222);
			this->groupBox2->TabIndex = 32;
			this->groupBox2->TabStop = false;
			this->groupBox2->Visible = false;
			// 
			// PSU1
			// 
			this->PSU1->AutoSize = true;
			this->PSU1->Location = System::Drawing::Point(330, 158);
			this->PSU1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->PSU1->Name = L"PSU1";
			this->PSU1->Size = System::Drawing::Size(41, 13);
			this->PSU1->TabIndex = 32;
			this->PSU1->Text = L"label23";
			// 
			// label35
			// 
			this->label35->AutoSize = true;
			this->label35->Location = System::Drawing::Point(218, 158);
			this->label35->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label35->Name = L"label35";
			this->label35->Size = System::Drawing::Size(81, 13);
			this->label35->TabIndex = 31;
			this->label35->Text = L"Мощность БП:";
			// 
			// SSD1
			// 
			this->SSD1->AutoSize = true;
			this->SSD1->Location = System::Drawing::Point(652, 130);
			this->SSD1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->SSD1->Name = L"SSD1";
			this->SSD1->Size = System::Drawing::Size(41, 13);
			this->SSD1->TabIndex = 28;
			this->SSD1->Text = L"label23";
			// 
			// label27
			// 
			this->label27->AutoSize = true;
			this->label27->Location = System::Drawing::Point(525, 130);
			this->label27->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label27->Name = L"label27";
			this->label27->Size = System::Drawing::Size(70, 13);
			this->label27->TabIndex = 27;
			this->label27->Text = L"Объём SSD:";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label10->Location = System::Drawing::Point(750, 32);
			this->label10->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(64, 25);
			this->label10->TabIndex = 13;
			this->label10->Text = L"Цена:";
			// 
			// RAM1
			// 
			this->RAM1->AutoSize = true;
			this->RAM1->Location = System::Drawing::Point(652, 73);
			this->RAM1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->RAM1->Name = L"RAM1";
			this->RAM1->Size = System::Drawing::Size(35, 13);
			this->RAM1->TabIndex = 8;
			this->RAM1->Text = L"label8";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(525, 73);
			this->label16->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(71, 13);
			this->label16->TabIndex = 7;
			this->label16->Text = L"Объём ОЗУ:";
			// 
			// Video1
			// 
			this->Video1->AutoSize = true;
			this->Video1->Location = System::Drawing::Point(330, 102);
			this->Video1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Video1->Name = L"Video1";
			this->Video1->Size = System::Drawing::Size(35, 13);
			this->Video1->TabIndex = 6;
			this->Video1->Text = L"label6";
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(218, 102);
			this->label18->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(70, 13);
			this->label18->TabIndex = 5;
			this->label18->Text = L"Видеокарта:";
			// 
			// CPU1
			// 
			this->CPU1->AutoSize = true;
			this->CPU1->Location = System::Drawing::Point(330, 73);
			this->CPU1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->CPU1->Name = L"CPU1";
			this->CPU1->Size = System::Drawing::Size(35, 13);
			this->CPU1->TabIndex = 4;
			this->CPU1->Text = L"label4";
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(218, 73);
			this->label20->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(66, 13);
			this->label20->TabIndex = 3;
			this->label20->Text = L"Процессор:";
			// 
			// NameComp1
			// 
			this->NameComp1->AutoSize = true;
			this->NameComp1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->NameComp1->Location = System::Drawing::Point(218, 45);
			this->NameComp1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->NameComp1->Name = L"NameComp1";
			this->NameComp1->Size = System::Drawing::Size(46, 17);
			this->NameComp1->TabIndex = 2;
			this->NameComp1->Text = L"label2";
			// 
			// label22
			// 
			this->label22->AutoSize = true;
			this->label22->Location = System::Drawing::Point(218, 16);
			this->label22->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label22->Name = L"label22";
			this->label22->Size = System::Drawing::Size(60, 13);
			this->label22->TabIndex = 1;
			this->label22->Text = L"Название:";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 20, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label1->Location = System::Drawing::Point(28, 13);
			this->label1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(114, 31);
			this->label1->TabIndex = 30;
			this->label1->Text = L"Каталог";
			// 
			// button6
			// 
			this->button6->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->button6->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button6->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button6->Location = System::Drawing::Point(750, 130);
			this->button6->Margin = System::Windows::Forms::Padding(2);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(125, 39);
			this->button6->TabIndex = 15;
			this->button6->Text = L"Купить";
			this->button6->UseVisualStyleBackColor = false;
			this->button6->Click += gcnew System::EventHandler(this, &Catalog::appToBin);
			// 
			// label66
			// 
			this->label66->AutoSize = true;
			this->label66->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label66->Location = System::Drawing::Point(750, 32);
			this->label66->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label66->Name = L"label66";
			this->label66->Size = System::Drawing::Size(64, 25);
			this->label66->TabIndex = 13;
			this->label66->Text = L"Цена:";
			// 
			// HDD5
			// 
			this->HDD5->AutoSize = true;
			this->HDD5->Location = System::Drawing::Point(652, 102);
			this->HDD5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->HDD5->Name = L"HDD5";
			this->HDD5->Size = System::Drawing::Size(35, 13);
			this->HDD5->TabIndex = 12;
			this->HDD5->Text = L"label2";
			// 
			// Motherboard5
			// 
			this->Motherboard5->AutoSize = true;
			this->Motherboard5->Location = System::Drawing::Point(330, 130);
			this->Motherboard5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Motherboard5->Name = L"Motherboard5";
			this->Motherboard5->Size = System::Drawing::Size(41, 13);
			this->Motherboard5->TabIndex = 10;
			this->Motherboard5->Text = L"label10";
			// 
			// label70
			// 
			this->label70->AutoSize = true;
			this->label70->Location = System::Drawing::Point(218, 130);
			this->label70->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label70->Name = L"label70";
			this->label70->Size = System::Drawing::Size(110, 13);
			this->label70->TabIndex = 9;
			this->label70->Text = L"Материнская плата:";
			// 
			// RAM5
			// 
			this->RAM5->AutoSize = true;
			this->RAM5->Location = System::Drawing::Point(652, 73);
			this->RAM5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->RAM5->Name = L"RAM5";
			this->RAM5->Size = System::Drawing::Size(35, 13);
			this->RAM5->TabIndex = 8;
			this->RAM5->Text = L"label8";
			// 
			// label72
			// 
			this->label72->AutoSize = true;
			this->label72->Location = System::Drawing::Point(525, 73);
			this->label72->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label72->Name = L"label72";
			this->label72->Size = System::Drawing::Size(71, 13);
			this->label72->TabIndex = 7;
			this->label72->Text = L"Объём ОЗУ:";
			// 
			// Price5
			// 
			this->Price5->AutoSize = true;
			this->Price5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Price5->Location = System::Drawing::Point(750, 73);
			this->Price5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Price5->Name = L"Price5";
			this->Price5->Size = System::Drawing::Size(119, 25);
			this->Price5->TabIndex = 14;
			this->Price5->Text = L"10000 руб.";
			// 
			// label68
			// 
			this->label68->AutoSize = true;
			this->label68->Location = System::Drawing::Point(525, 102);
			this->label68->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label68->Name = L"label68";
			this->label68->Size = System::Drawing::Size(72, 13);
			this->label68->TabIndex = 11;
			this->label68->Text = L"Объём HDD:";
			// 
			// Video5
			// 
			this->Video5->AutoSize = true;
			this->Video5->Location = System::Drawing::Point(330, 102);
			this->Video5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Video5->Name = L"Video5";
			this->Video5->Size = System::Drawing::Size(35, 13);
			this->Video5->TabIndex = 6;
			this->Video5->Text = L"label6";
			// 
			// label74
			// 
			this->label74->AutoSize = true;
			this->label74->Location = System::Drawing::Point(218, 102);
			this->label74->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label74->Name = L"label74";
			this->label74->Size = System::Drawing::Size(70, 13);
			this->label74->TabIndex = 5;
			this->label74->Text = L"Видеокарта:";
			// 
			// groupBox5
			// 
			this->groupBox5->Controls->Add(this->SSD4);
			this->groupBox5->Controls->Add(this->label55);
			this->groupBox5->Controls->Add(this->PSU4);
			this->groupBox5->Controls->Add(this->label51);
			this->groupBox5->Controls->Add(this->Amount4);
			this->groupBox5->Controls->Add(this->label25);
			this->groupBox5->Controls->Add(this->button5);
			this->groupBox5->Controls->Add(this->Price4);
			this->groupBox5->Controls->Add(this->label52);
			this->groupBox5->Controls->Add(this->HDD4);
			this->groupBox5->Controls->Add(this->label54);
			this->groupBox5->Controls->Add(this->Motherboard4);
			this->groupBox5->Controls->Add(this->label56);
			this->groupBox5->Controls->Add(this->RAM4);
			this->groupBox5->Controls->Add(this->label58);
			this->groupBox5->Controls->Add(this->Video4);
			this->groupBox5->Controls->Add(this->label60);
			this->groupBox5->Controls->Add(this->CPU4);
			this->groupBox5->Controls->Add(this->label62);
			this->groupBox5->Controls->Add(this->NameComp4);
			this->groupBox5->Controls->Add(this->label64);
			this->groupBox5->Controls->Add(this->pictureBox5);
			this->groupBox5->Location = System::Drawing::Point(19, 959);
			this->groupBox5->Margin = System::Windows::Forms::Padding(2);
			this->groupBox5->Name = L"groupBox5";
			this->groupBox5->Padding = System::Windows::Forms::Padding(2);
			this->groupBox5->Size = System::Drawing::Size(900, 222);
			this->groupBox5->TabIndex = 34;
			this->groupBox5->TabStop = false;
			this->groupBox5->Visible = false;
			// 
			// SSD4
			// 
			this->SSD4->AutoSize = true;
			this->SSD4->Location = System::Drawing::Point(652, 130);
			this->SSD4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->SSD4->Name = L"SSD4";
			this->SSD4->Size = System::Drawing::Size(41, 13);
			this->SSD4->TabIndex = 40;
			this->SSD4->Text = L"label23";
			// 
			// label55
			// 
			this->label55->AutoSize = true;
			this->label55->Location = System::Drawing::Point(525, 130);
			this->label55->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label55->Name = L"label55";
			this->label55->Size = System::Drawing::Size(70, 13);
			this->label55->TabIndex = 39;
			this->label55->Text = L"Объём SSD:";
			// 
			// PSU4
			// 
			this->PSU4->AutoSize = true;
			this->PSU4->Location = System::Drawing::Point(330, 158);
			this->PSU4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->PSU4->Name = L"PSU4";
			this->PSU4->Size = System::Drawing::Size(41, 13);
			this->PSU4->TabIndex = 38;
			this->PSU4->Text = L"label23";
			// 
			// label51
			// 
			this->label51->AutoSize = true;
			this->label51->Location = System::Drawing::Point(218, 158);
			this->label51->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label51->Name = L"label51";
			this->label51->Size = System::Drawing::Size(81, 13);
			this->label51->TabIndex = 37;
			this->label51->Text = L"Мощность БП:";
			// 
			// button5
			// 
			this->button5->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(255)), static_cast<System::Int32>(static_cast<System::Byte>(128)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->button5->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button5->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button5->Location = System::Drawing::Point(750, 132);
			this->button5->Margin = System::Windows::Forms::Padding(2);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(125, 39);
			this->button5->TabIndex = 15;
			this->button5->Text = L"Купить";
			this->button5->UseVisualStyleBackColor = false;
			this->button5->Click += gcnew System::EventHandler(this, &Catalog::appToBin);
			// 
			// Price4
			// 
			this->Price4->AutoSize = true;
			this->Price4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Price4->Location = System::Drawing::Point(750, 73);
			this->Price4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Price4->Name = L"Price4";
			this->Price4->Size = System::Drawing::Size(119, 25);
			this->Price4->TabIndex = 14;
			this->Price4->Text = L"10000 руб.";
			// 
			// label52
			// 
			this->label52->AutoSize = true;
			this->label52->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label52->Location = System::Drawing::Point(750, 32);
			this->label52->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label52->Name = L"label52";
			this->label52->Size = System::Drawing::Size(64, 25);
			this->label52->TabIndex = 13;
			this->label52->Text = L"Цена:";
			// 
			// HDD4
			// 
			this->HDD4->AutoSize = true;
			this->HDD4->Location = System::Drawing::Point(652, 102);
			this->HDD4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->HDD4->Name = L"HDD4";
			this->HDD4->Size = System::Drawing::Size(35, 13);
			this->HDD4->TabIndex = 12;
			this->HDD4->Text = L"label2";
			// 
			// label54
			// 
			this->label54->AutoSize = true;
			this->label54->Location = System::Drawing::Point(525, 102);
			this->label54->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label54->Name = L"label54";
			this->label54->Size = System::Drawing::Size(72, 13);
			this->label54->TabIndex = 11;
			this->label54->Text = L"Объём HDD:";
			// 
			// Motherboard4
			// 
			this->Motherboard4->AutoSize = true;
			this->Motherboard4->Location = System::Drawing::Point(330, 130);
			this->Motherboard4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Motherboard4->Name = L"Motherboard4";
			this->Motherboard4->Size = System::Drawing::Size(41, 13);
			this->Motherboard4->TabIndex = 10;
			this->Motherboard4->Text = L"label10";
			// 
			// label56
			// 
			this->label56->AutoSize = true;
			this->label56->Location = System::Drawing::Point(218, 130);
			this->label56->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label56->Name = L"label56";
			this->label56->Size = System::Drawing::Size(110, 13);
			this->label56->TabIndex = 9;
			this->label56->Text = L"Материнская плата:";
			// 
			// RAM4
			// 
			this->RAM4->AutoSize = true;
			this->RAM4->Location = System::Drawing::Point(652, 73);
			this->RAM4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->RAM4->Name = L"RAM4";
			this->RAM4->Size = System::Drawing::Size(35, 13);
			this->RAM4->TabIndex = 8;
			this->RAM4->Text = L"label8";
			// 
			// label58
			// 
			this->label58->AutoSize = true;
			this->label58->Location = System::Drawing::Point(525, 73);
			this->label58->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label58->Name = L"label58";
			this->label58->Size = System::Drawing::Size(71, 13);
			this->label58->TabIndex = 7;
			this->label58->Text = L"Объём ОЗУ:";
			// 
			// Video4
			// 
			this->Video4->AutoSize = true;
			this->Video4->Location = System::Drawing::Point(330, 102);
			this->Video4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->Video4->Name = L"Video4";
			this->Video4->Size = System::Drawing::Size(35, 13);
			this->Video4->TabIndex = 6;
			this->Video4->Text = L"label6";
			// 
			// label60
			// 
			this->label60->AutoSize = true;
			this->label60->Location = System::Drawing::Point(217, 102);
			this->label60->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label60->Name = L"label60";
			this->label60->Size = System::Drawing::Size(70, 13);
			this->label60->TabIndex = 5;
			this->label60->Text = L"Видеокарта:";
			// 
			// CPU4
			// 
			this->CPU4->AutoSize = true;
			this->CPU4->Location = System::Drawing::Point(330, 73);
			this->CPU4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->CPU4->Name = L"CPU4";
			this->CPU4->Size = System::Drawing::Size(35, 13);
			this->CPU4->TabIndex = 4;
			this->CPU4->Text = L"label4";
			// 
			// label62
			// 
			this->label62->AutoSize = true;
			this->label62->Location = System::Drawing::Point(217, 73);
			this->label62->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label62->Name = L"label62";
			this->label62->Size = System::Drawing::Size(66, 13);
			this->label62->TabIndex = 3;
			this->label62->Text = L"Процессор:";
			// 
			// NameComp4
			// 
			this->NameComp4->AutoSize = true;
			this->NameComp4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->NameComp4->Location = System::Drawing::Point(218, 45);
			this->NameComp4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->NameComp4->Name = L"NameComp4";
			this->NameComp4->Size = System::Drawing::Size(46, 17);
			this->NameComp4->TabIndex = 2;
			this->NameComp4->Text = L"label2";
			// 
			// label64
			// 
			this->label64->AutoSize = true;
			this->label64->Location = System::Drawing::Point(218, 16);
			this->label64->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label64->Name = L"label64";
			this->label64->Size = System::Drawing::Size(60, 13);
			this->label64->TabIndex = 1;
			this->label64->Text = L"Название:";
			// 
			// pictureBox5
			// 
			this->pictureBox5->Location = System::Drawing::Point(15, 16);
			this->pictureBox5->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox5->Name = L"pictureBox5";
			this->pictureBox5->Size = System::Drawing::Size(180, 195);
			this->pictureBox5->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox5->TabIndex = 0;
			this->pictureBox5->TabStop = false;
			// 
			// groupBox6
			// 
			this->groupBox6->Controls->Add(this->SSD5);
			this->groupBox6->Controls->Add(this->label59);
			this->groupBox6->Controls->Add(this->PSU5);
			this->groupBox6->Controls->Add(this->label63);
			this->groupBox6->Controls->Add(this->Amount5);
			this->groupBox6->Controls->Add(this->label29);
			this->groupBox6->Controls->Add(this->button6);
			this->groupBox6->Controls->Add(this->Price5);
			this->groupBox6->Controls->Add(this->label66);
			this->groupBox6->Controls->Add(this->HDD5);
			this->groupBox6->Controls->Add(this->label68);
			this->groupBox6->Controls->Add(this->Motherboard5);
			this->groupBox6->Controls->Add(this->label70);
			this->groupBox6->Controls->Add(this->RAM5);
			this->groupBox6->Controls->Add(this->label72);
			this->groupBox6->Controls->Add(this->Video5);
			this->groupBox6->Controls->Add(this->label74);
			this->groupBox6->Controls->Add(this->CPU5);
			this->groupBox6->Controls->Add(this->label76);
			this->groupBox6->Controls->Add(this->NameComp5);
			this->groupBox6->Controls->Add(this->label78);
			this->groupBox6->Controls->Add(this->pictureBox6);
			this->groupBox6->Location = System::Drawing::Point(19, 1186);
			this->groupBox6->Margin = System::Windows::Forms::Padding(2);
			this->groupBox6->Name = L"groupBox6";
			this->groupBox6->Padding = System::Windows::Forms::Padding(2);
			this->groupBox6->Size = System::Drawing::Size(900, 222);
			this->groupBox6->TabIndex = 35;
			this->groupBox6->TabStop = false;
			this->groupBox6->Visible = false;
			// 
			// SSD5
			// 
			this->SSD5->AutoSize = true;
			this->SSD5->Location = System::Drawing::Point(652, 130);
			this->SSD5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->SSD5->Name = L"SSD5";
			this->SSD5->Size = System::Drawing::Size(41, 13);
			this->SSD5->TabIndex = 44;
			this->SSD5->Text = L"label23";
			// 
			// label59
			// 
			this->label59->AutoSize = true;
			this->label59->Location = System::Drawing::Point(525, 130);
			this->label59->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label59->Name = L"label59";
			this->label59->Size = System::Drawing::Size(70, 13);
			this->label59->TabIndex = 43;
			this->label59->Text = L"Объём SSD:";
			// 
			// PSU5
			// 
			this->PSU5->AutoSize = true;
			this->PSU5->Location = System::Drawing::Point(330, 158);
			this->PSU5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->PSU5->Name = L"PSU5";
			this->PSU5->Size = System::Drawing::Size(41, 13);
			this->PSU5->TabIndex = 42;
			this->PSU5->Text = L"label23";
			// 
			// label63
			// 
			this->label63->AutoSize = true;
			this->label63->Location = System::Drawing::Point(218, 158);
			this->label63->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label63->Name = L"label63";
			this->label63->Size = System::Drawing::Size(81, 13);
			this->label63->TabIndex = 41;
			this->label63->Text = L"Мощность БП:";
			// 
			// linkLabel1
			// 
			this->linkLabel1->AutoSize = true;
			this->linkLabel1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->linkLabel1->Location = System::Drawing::Point(16, 1865);
			this->linkLabel1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->linkLabel1->Name = L"linkLabel1";
			this->linkLabel1->Size = System::Drawing::Size(112, 17);
			this->linkLabel1->TabIndex = 41;
			this->linkLabel1->TabStop = true;
			this->linkLabel1->Text = L"← Предыдущая";
			this->linkLabel1->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &Catalog::linkLabel1_LinkClicked);
			// 
			// linkLabel2
			// 
			this->linkLabel2->AutoSize = true;
			this->linkLabel2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->linkLabel2->Location = System::Drawing::Point(816, 1865);
			this->linkLabel2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->linkLabel2->Name = L"linkLabel2";
			this->linkLabel2->Size = System::Drawing::Size(103, 17);
			this->linkLabel2->TabIndex = 42;
			this->linkLabel2->TabStop = true;
			this->linkLabel2->Text = L"Следующая →";
			this->linkLabel2->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &Catalog::linkLabel2_LinkClicked);
			// 
			// pictureBox9
			// 
			this->pictureBox9->Cursor = System::Windows::Forms::Cursors::Hand;
			this->pictureBox9->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox9.Image")));
			this->pictureBox9->Location = System::Drawing::Point(801, 18);
			this->pictureBox9->Name = L"pictureBox9";
			this->pictureBox9->Size = System::Drawing::Size(33, 26);
			this->pictureBox9->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox9->TabIndex = 43;
			this->pictureBox9->TabStop = false;
			this->pictureBox9->Click += gcnew System::EventHandler(this, &Catalog::pictureBox9_Click);
			// 
			// BinLabel
			// 
			this->BinLabel->AutoSize = true;
			this->BinLabel->Cursor = System::Windows::Forms::Cursors::Hand;
			this->BinLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->BinLabel->Location = System::Drawing::Point(833, 22);
			this->BinLabel->Name = L"BinLabel";
			this->BinLabel->Size = System::Drawing::Size(67, 18);
			this->BinLabel->TabIndex = 44;
			this->BinLabel->Text = L"Корзина";
			this->BinLabel->Click += gcnew System::EventHandler(this, &Catalog::BinLabel_Click);
			// 
			// BackMenu
			// 
			this->BackMenu->AutoSize = true;
			this->BackMenu->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->BackMenu->Location = System::Drawing::Point(39, 33);
			this->BackMenu->Name = L"BackMenu";
			this->BackMenu->Size = System::Drawing::Size(166, 20);
			this->BackMenu->TabIndex = 40;
			this->BackMenu->TabStop = true;
			this->BackMenu->Text = L"← В главное меню";
			// 
			// Filter
			// 
			this->Filter->Controls->Add(this->Clear);
			this->Filter->Controls->Add(this->filterPSU2);
			this->Filter->Controls->Add(this->label11);
			this->Filter->Controls->Add(this->filterCost2);
			this->Filter->Controls->Add(this->label31);
			this->Filter->Controls->Add(this->filterButton);
			this->Filter->Controls->Add(this->filterCost1);
			this->Filter->Controls->Add(this->filterSSD2);
			this->Filter->Controls->Add(this->filterSSD1);
			this->Filter->Controls->Add(this->filterHDD2);
			this->Filter->Controls->Add(this->filterHDD1);
			this->Filter->Controls->Add(this->filterPSU1);
			this->Filter->Controls->Add(this->filterRAM2);
			this->Filter->Controls->Add(this->filterRAM1);
			this->Filter->Controls->Add(this->filterMotherboard);
			this->Filter->Controls->Add(this->filterGPU);
			this->Filter->Controls->Add(this->filterCPU);
			this->Filter->Controls->Add(this->filterName);
			this->Filter->Controls->Add(this->label45);
			this->Filter->Controls->Add(this->label49);
			this->Filter->Controls->Add(this->label53);
			this->Filter->Controls->Add(this->label57);
			this->Filter->Controls->Add(this->label61);
			this->Filter->Controls->Add(this->label65);
			this->Filter->Controls->Add(this->label69);
			this->Filter->Controls->Add(this->label73);
			this->Filter->Controls->Add(this->label77);
			this->Filter->Location = System::Drawing::Point(952, 49);
			this->Filter->Margin = System::Windows::Forms::Padding(2);
			this->Filter->Name = L"Filter";
			this->Filter->Padding = System::Windows::Forms::Padding(2);
			this->Filter->Size = System::Drawing::Size(284, 309);
			this->Filter->TabIndex = 45;
			this->Filter->TabStop = false;
			this->Filter->Text = L"Фильтр";
			// 
			// Clear
			// 
			this->Clear->BackColor = System::Drawing::SystemColors::MenuHighlight;
			this->Clear->Cursor = System::Windows::Forms::Cursors::Hand;
			this->Clear->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->Clear->Location = System::Drawing::Point(163, 273);
			this->Clear->Margin = System::Windows::Forms::Padding(2);
			this->Clear->Name = L"Clear";
			this->Clear->Size = System::Drawing::Size(82, 32);
			this->Clear->TabIndex = 78;
			this->Clear->Text = L"Очистить";
			this->Clear->UseVisualStyleBackColor = false;
			this->Clear->Click += gcnew System::EventHandler(this, &Catalog::Clear_Click);
			// 
			// filterPSU2
			// 
			this->filterPSU2->Location = System::Drawing::Point(202, 144);
			this->filterPSU2->Margin = System::Windows::Forms::Padding(2);
			this->filterPSU2->Name = L"filterPSU2";
			this->filterPSU2->Size = System::Drawing::Size(58, 20);
			this->filterPSU2->TabIndex = 80;
			this->filterPSU2->TextChanged += gcnew System::EventHandler(this, &Catalog::filterElement_TextChanged);
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(223, 126);
			this->label11->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(22, 13);
			this->label11->TabIndex = 79;
			this->label11->Text = L"До";
			// 
			// filterCost2
			// 
			this->filterCost2->Location = System::Drawing::Point(202, 242);
			this->filterCost2->Margin = System::Windows::Forms::Padding(2);
			this->filterCost2->Name = L"filterCost2";
			this->filterCost2->Size = System::Drawing::Size(58, 20);
			this->filterCost2->TabIndex = 76;
			this->filterCost2->TextChanged += gcnew System::EventHandler(this, &Catalog::filterElement_TextChanged);
			// 
			// label31
			// 
			this->label31->AutoSize = true;
			this->label31->Location = System::Drawing::Point(119, 126);
			this->label31->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label31->Name = L"label31";
			this->label31->Size = System::Drawing::Size(20, 13);
			this->label31->TabIndex = 78;
			this->label31->Text = L"От";
			// 
			// filterButton
			// 
			this->filterButton->BackColor = System::Drawing::SystemColors::MenuHighlight;
			this->filterButton->Cursor = System::Windows::Forms::Cursors::Hand;
			this->filterButton->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->filterButton->Location = System::Drawing::Point(45, 273);
			this->filterButton->Margin = System::Windows::Forms::Padding(2);
			this->filterButton->Name = L"filterButton";
			this->filterButton->Size = System::Drawing::Size(82, 32);
			this->filterButton->TabIndex = 77;
			this->filterButton->Text = L"Применить";
			this->filterButton->UseVisualStyleBackColor = false;
			this->filterButton->Click += gcnew System::EventHandler(this, &Catalog::filterButton_Click);
			// 
			// filterCost1
			// 
			this->filterCost1->Location = System::Drawing::Point(100, 242);
			this->filterCost1->Margin = System::Windows::Forms::Padding(2);
			this->filterCost1->Name = L"filterCost1";
			this->filterCost1->Size = System::Drawing::Size(58, 20);
			this->filterCost1->TabIndex = 75;
			this->filterCost1->TextChanged += gcnew System::EventHandler(this, &Catalog::filterElement_TextChanged);
			// 
			// filterSSD2
			// 
			this->filterSSD2->Cursor = System::Windows::Forms::Cursors::Hand;
			this->filterSSD2->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->filterSSD2->FormattingEnabled = true;
			this->filterSSD2->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"64", L"128", L"240", L"256", L"512", L"1024",
					L"2048"
			});
			this->filterSSD2->Location = System::Drawing::Point(202, 213);
			this->filterSSD2->Margin = System::Windows::Forms::Padding(2);
			this->filterSSD2->Name = L"filterSSD2";
			this->filterSSD2->Size = System::Drawing::Size(58, 21);
			this->filterSSD2->TabIndex = 74;
			// 
			// filterSSD1
			// 
			this->filterSSD1->Cursor = System::Windows::Forms::Cursors::Hand;
			this->filterSSD1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->filterSSD1->FormattingEnabled = true;
			this->filterSSD1->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"64", L"128", L"240", L"256", L"512", L"1024",
					L"2048"
			});
			this->filterSSD1->Location = System::Drawing::Point(100, 214);
			this->filterSSD1->Margin = System::Windows::Forms::Padding(2);
			this->filterSSD1->Name = L"filterSSD1";
			this->filterSSD1->Size = System::Drawing::Size(58, 21);
			this->filterSSD1->TabIndex = 73;
			// 
			// filterHDD2
			// 
			this->filterHDD2->Cursor = System::Windows::Forms::Cursors::Hand;
			this->filterHDD2->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->filterHDD2->FormattingEnabled = true;
			this->filterHDD2->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"128", L"256", L"512", L"1024", L"2048", L"4096",
					L"8192"
			});
			this->filterHDD2->Location = System::Drawing::Point(202, 189);
			this->filterHDD2->Margin = System::Windows::Forms::Padding(2);
			this->filterHDD2->Name = L"filterHDD2";
			this->filterHDD2->Size = System::Drawing::Size(58, 21);
			this->filterHDD2->TabIndex = 72;
			// 
			// filterHDD1
			// 
			this->filterHDD1->Cursor = System::Windows::Forms::Cursors::Hand;
			this->filterHDD1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->filterHDD1->FormattingEnabled = true;
			this->filterHDD1->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"128", L"256", L"512", L"1024", L"2048", L"4096",
					L"8192"
			});
			this->filterHDD1->Location = System::Drawing::Point(100, 189);
			this->filterHDD1->Margin = System::Windows::Forms::Padding(2);
			this->filterHDD1->Name = L"filterHDD1";
			this->filterHDD1->Size = System::Drawing::Size(58, 21);
			this->filterHDD1->TabIndex = 71;
			// 
			// filterPSU1
			// 
			this->filterPSU1->Location = System::Drawing::Point(100, 144);
			this->filterPSU1->Margin = System::Windows::Forms::Padding(2);
			this->filterPSU1->Name = L"filterPSU1";
			this->filterPSU1->Size = System::Drawing::Size(58, 20);
			this->filterPSU1->TabIndex = 70;
			this->filterPSU1->TextChanged += gcnew System::EventHandler(this, &Catalog::filterElement_TextChanged);
			// 
			// filterRAM2
			// 
			this->filterRAM2->Cursor = System::Windows::Forms::Cursors::Hand;
			this->filterRAM2->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->filterRAM2->FormattingEnabled = true;
			this->filterRAM2->Items->AddRange(gcnew cli::array< System::Object^  >(8) { L"2", L"4", L"6", L"8", L"16", L"32", L"64", L"128" });
			this->filterRAM2->Location = System::Drawing::Point(202, 167);
			this->filterRAM2->Margin = System::Windows::Forms::Padding(2);
			this->filterRAM2->Name = L"filterRAM2";
			this->filterRAM2->Size = System::Drawing::Size(58, 21);
			this->filterRAM2->TabIndex = 69;
			// 
			// filterRAM1
			// 
			this->filterRAM1->Cursor = System::Windows::Forms::Cursors::Hand;
			this->filterRAM1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->filterRAM1->FormattingEnabled = true;
			this->filterRAM1->Items->AddRange(gcnew cli::array< System::Object^  >(8) { L"2", L"4", L"6", L"8", L"16", L"32", L"64", L"128" });
			this->filterRAM1->Location = System::Drawing::Point(100, 167);
			this->filterRAM1->Margin = System::Windows::Forms::Padding(2);
			this->filterRAM1->Name = L"filterRAM1";
			this->filterRAM1->Size = System::Drawing::Size(58, 21);
			this->filterRAM1->TabIndex = 68;
			// 
			// filterMotherboard
			// 
			this->filterMotherboard->FormattingEnabled = true;
			this->filterMotherboard->Items->AddRange(gcnew cli::array< System::Object^  >(4) { L"GIGABYTE", L"MSI", L"ASUS", L"Asrock" });
			this->filterMotherboard->Location = System::Drawing::Point(101, 95);
			this->filterMotherboard->Margin = System::Windows::Forms::Padding(2);
			this->filterMotherboard->Name = L"filterMotherboard";
			this->filterMotherboard->Size = System::Drawing::Size(158, 21);
			this->filterMotherboard->TabIndex = 67;
			this->filterMotherboard->TextChanged += gcnew System::EventHandler(this, &Catalog::forChangeFilterFields);
			// 
			// filterGPU
			// 
			this->filterGPU->FormattingEnabled = true;
			this->filterGPU->Items->AddRange(gcnew cli::array< System::Object^  >(3) { L"NVIDIA", L"AMD", L"Intel" });
			this->filterGPU->Location = System::Drawing::Point(101, 71);
			this->filterGPU->Margin = System::Windows::Forms::Padding(2);
			this->filterGPU->Name = L"filterGPU";
			this->filterGPU->Size = System::Drawing::Size(158, 21);
			this->filterGPU->TabIndex = 66;
			this->filterGPU->TextChanged += gcnew System::EventHandler(this, &Catalog::forChangeFilterFields);
			// 
			// filterCPU
			// 
			this->filterCPU->FormattingEnabled = true;
			this->filterCPU->Items->AddRange(gcnew cli::array< System::Object^  >(2) { L"Intel", L"AMD" });
			this->filterCPU->Location = System::Drawing::Point(101, 46);
			this->filterCPU->Margin = System::Windows::Forms::Padding(2);
			this->filterCPU->Name = L"filterCPU";
			this->filterCPU->Size = System::Drawing::Size(158, 21);
			this->filterCPU->TabIndex = 65;
			this->filterCPU->TextChanged += gcnew System::EventHandler(this, &Catalog::forChangeFilterFields);
			// 
			// filterName
			// 
			this->filterName->FormattingEnabled = true;
			this->filterName->Items->AddRange(gcnew cli::array< System::Object^  >(8) {
				L"Acer", L"HP", L"DELL", L"DEXP", L"ASUS", L"MSI",
					L"ZET", L"Lenovo"
			});
			this->filterName->Location = System::Drawing::Point(101, 20);
			this->filterName->Margin = System::Windows::Forms::Padding(2);
			this->filterName->Name = L"filterName";
			this->filterName->Size = System::Drawing::Size(158, 21);
			this->filterName->TabIndex = 64;
			this->filterName->TextChanged += gcnew System::EventHandler(this, &Catalog::forChangeFilterFields);
			// 
			// label45
			// 
			this->label45->AutoSize = true;
			this->label45->Location = System::Drawing::Point(38, 242);
			this->label45->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label45->Name = L"label45";
			this->label45->Size = System::Drawing::Size(59, 13);
			this->label45->TabIndex = 62;
			this->label45->Text = L"Цена, руб.";
			// 
			// label49
			// 
			this->label49->AutoSize = true;
			this->label49->Location = System::Drawing::Point(12, 219);
			this->label49->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label49->Name = L"label49";
			this->label49->Size = System::Drawing::Size(85, 13);
			this->label49->TabIndex = 61;
			this->label49->Text = L"Объём SSD, Гб";
			// 
			// label53
			// 
			this->label53->AutoSize = true;
			this->label53->Location = System::Drawing::Point(36, 99);
			this->label53->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label53->Name = L"label53";
			this->label53->Size = System::Drawing::Size(62, 13);
			this->label53->TabIndex = 60;
			this->label53->Text = L"Мат. плата";
			// 
			// label57
			// 
			this->label57->AutoSize = true;
			this->label57->Location = System::Drawing::Point(18, 195);
			this->label57->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label57->Name = L"label57";
			this->label57->Size = System::Drawing::Size(83, 13);
			this->label57->TabIndex = 59;
			this->label57->Text = L"Объём ЖД, Гб";
			// 
			// label61
			// 
			this->label61->AutoSize = true;
			this->label61->Location = System::Drawing::Point(20, 172);
			this->label61->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label61->Name = L"label61";
			this->label61->Size = System::Drawing::Size(79, 13);
			this->label61->TabIndex = 58;
			this->label61->Text = L"Объём ОП, Гб";
			// 
			// label65
			// 
			this->label65->AutoSize = true;
			this->label65->Location = System::Drawing::Point(2, 146);
			this->label65->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label65->Name = L"label65";
			this->label65->Size = System::Drawing::Size(96, 13);
			this->label65->TabIndex = 57;
			this->label65->Text = L"Мощность БП, Вт";
			// 
			// label69
			// 
			this->label69->AutoSize = true;
			this->label69->Location = System::Drawing::Point(31, 76);
			this->label69->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label69->Name = L"label69";
			this->label69->Size = System::Drawing::Size(67, 13);
			this->label69->TabIndex = 56;
			this->label69->Text = L"Видеокарта";
			// 
			// label73
			// 
			this->label73->AutoSize = true;
			this->label73->Location = System::Drawing::Point(36, 54);
			this->label73->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label73->Name = L"label73";
			this->label73->Size = System::Drawing::Size(63, 13);
			this->label73->TabIndex = 55;
			this->label73->Text = L"Процессор";
			// 
			// label77
			// 
			this->label77->AutoSize = true;
			this->label77->Location = System::Drawing::Point(42, 28);
			this->label77->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label77->Name = L"label77";
			this->label77->Size = System::Drawing::Size(57, 13);
			this->label77->TabIndex = 54;
			this->label77->Text = L"Название";
			// 
			// linkLabel3
			// 
			this->linkLabel3->AutoSize = true;
			this->linkLabel3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->linkLabel3->Location = System::Drawing::Point(1150, 24);
			this->linkLabel3->Name = L"linkLabel3";
			this->linkLabel3->Size = System::Drawing::Size(86, 16);
			this->linkLabel3->TabIndex = 47;
			this->linkLabel3->TabStop = true;
			this->linkLabel3->Text = L"Меню входа";
			this->linkLabel3->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &Catalog::linkLabel3_LinkClicked);
			// 
			// Catalog
			// 
			this->AllowDrop = true;
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::None;
			this->AutoScroll = true;
			this->BackColor = System::Drawing::SystemColors::ButtonHighlight;
			this->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
			this->ClientSize = System::Drawing::Size(1315, 569);
			this->Controls->Add(this->linkLabel3);
			this->Controls->Add(this->AlertFilter);
			this->Controls->Add(this->Filter);
			this->Controls->Add(this->BinLabel);
			this->Controls->Add(this->pictureBox9);
			this->Controls->Add(this->linkLabel2);
			this->Controls->Add(this->linkLabel1);
			this->Controls->Add(this->groupBox8);
			this->Controls->Add(this->groupBox7);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->groupBox4);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->groupBox5);
			this->Controls->Add(this->groupBox6);
			this->Cursor = System::Windows::Forms::Cursors::Default;
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Margin = System::Windows::Forms::Padding(2);
			this->MaximizeBox = false;
			this->MaximumSize = System::Drawing::Size(1430, 821);
			this->MinimumSize = System::Drawing::Size(1318, 43);
			this->Name = L"Catalog";
			this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Show;
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Каталог";
			this->Load += gcnew System::EventHandler(this, &Catalog::Catalog_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox8))->EndInit();
			this->groupBox8->ResumeLayout(false);
			this->groupBox8->PerformLayout();
			this->groupBox7->ResumeLayout(false);
			this->groupBox7->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox6))->EndInit();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->EndInit();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->EndInit();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox5->ResumeLayout(false);
			this->groupBox5->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox5))->EndInit();
			this->groupBox6->ResumeLayout(false);
			this->groupBox6->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox9))->EndInit();
			this->Filter->ResumeLayout(false);
			this->Filter->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	private: System::Void BinLabel_Click(System::Object^ sender, System::EventArgs^ e)
	{
		if (getFileLength(shoppingCartFile) == 0)
		{
			MessageBox::Show("Корзина пуста!");
		}
		else
		{
			indexRead = 0;
			position = 0;
			itemsOfThisPage = 0;

			UserBin form;
			form.Owner = this;
			this->Hide();
			form.ShowDialog();

			Computers* computer;
			computer = getComputers(0);
			shoppingCartPage = 0;

			fillForm(computer);
			delete[8] computer;
		}
	}

	private: System::Void pictureBox9_Click(System::Object^ sender, System::EventArgs^ e)
	{
		if (getFileLength(shoppingCartFile) == 0)
		{
			MessageBox::Show("Корзина пуста!");
		}
		else
		{
			indexRead = 0;
			position = 0;
			itemsOfThisPage = 0;

			UserBin form;
			form.Owner = this;
			this->Hide();
			form.ShowDialog();

			Computers* computer;
			computer = getComputers(0);
			shoppingCartPage = 0;

			fillForm(computer);
			delete[8] computer;
		}

	}

	private: System::Void fillForm(Computers* computer)
	{
		char buff[10];
		int groupBoxYcoordinate = 230;

		if (string(computer[0].name) != "")
		{
			AlertFilter->Hide();
			groupBox1->Show();
			NameComp0->Text = msclr::interop::marshal_as<System::String^>(computer[0].name);
			CPU0->Text = msclr::interop::marshal_as<System::String^>(computer[0].component.cpu);
			Video0->Text = msclr::interop::marshal_as<System::String^>(computer[0].component.gpu);
			Motherboard0->Text = msclr::interop::marshal_as<System::String^>(computer[0].component.motherboard);
			PSU0->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[0].component.psu, buff, 10));
			RAM0->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[0].component.ram, buff, 10));
			HDD0->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[0].component.hdd, buff, 10));
			SSD0->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[0].component.ssd, buff, 10));
			Amount0->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[0].amount, buff, 10));
			std::string str = _ltoa(computer[0].cost, buff, 10);
			str += " Руб.";
			Price0->Text = msclr::interop::marshal_as<System::String^>(str);
			pictureBox1->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[0].photo);
		}
		else
		{
			AlertFilter->Show();
			groupBox1->Hide();
		}

		int diff = fileLength - storePage * 8;
		diff = abs(diff);

		System::Drawing::Point point = this->linkLabel1->Location;
		if (diff <= 8)
		{
			itemsOfThisPage = diff;
			linkLabel2->Hide();
			//this->linkLabel1->Location = System::Drawing::Point(16, 1850 - groupBoxYcoordinate * (8 - diff));
			groupBox2->Hide();
			groupBox3->Hide();
			groupBox4->Hide();
			groupBox5->Hide();
			groupBox6->Hide();
			groupBox7->Hide();
			groupBox8->Hide();
			switch (diff - 1)
			{
			case 7:
			{
				groupBox8->Show();
			}
			case 6:
			{
				groupBox7->Show();
			}
			case 5:
			{
				groupBox6->Show();
			}
			case 4:
			{
				groupBox5->Show();
			}
			case 3:
			{
				groupBox4->Show();
			}
			case 2:
			{
				groupBox3->Show();
			}
			case 1:
			{
				groupBox2->Show();
			}
			}

			if (diff == 2)
			{
				this->ClientSize = System::Drawing::Size(831, 758 - groupBoxYcoordinate);
			}

		}
		else
		{
			itemsOfThisPage = 8;
			linkLabel2->Show();
			groupBox2->Show();
			groupBox3->Show();
			groupBox4->Show();
			groupBox5->Show();
			groupBox6->Show();
			groupBox7->Show();
			groupBox8->Show();
		}

		if (diff != fileLength)
		{
			linkLabel1->Show();
		}
		else
		{
			linkLabel1->Hide();
		}

		for (int i = 1; i < 8; i++)
		{
			string nameGroupBox = "groupBox";
			nameGroupBox += _itoa(i + 1, buff, 10);
			if (this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Visible == true)
			{
				string nameElement = "NameComp";
				nameElement += _itoa(i, buff, 10);
				this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(computer[i].name);
				nameElement = "CPU";
				nameElement += _itoa(i, buff, 10);
				this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(computer[i].component.cpu);
				nameElement = "Video";
				nameElement += _itoa(i, buff, 10);
				this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(computer[i].component.gpu);
				nameElement = "Motherboard";
				nameElement += _itoa(i, buff, 10);
				this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(computer[i].component.motherboard);
				nameElement = "PSU";
				nameElement += _itoa(i, buff, 10);
				this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[i].component.psu, buff, 10));
				nameElement = "RAM";
				nameElement += _itoa(i, buff, 10);
				this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[i].component.ram, buff, 10));
				nameElement = "HDD";
				nameElement += _itoa(i, buff, 10);
				this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[i].component.hdd, buff, 10));
				nameElement = "SSD";
				nameElement += _itoa(i, buff, 10);
				this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[i].component.ssd, buff, 10));
				nameElement = "Amount";
				nameElement += _itoa(i, buff, 10);
				this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[i].amount, buff, 10));
				nameElement = "Price";
				nameElement += _itoa(i, buff, 10);
				this->Controls[msclr::interop::marshal_as<System::String^>(nameGroupBox)]->Controls[msclr::interop::marshal_as<System::String^>(nameElement)]->Text = msclr::interop::marshal_as<System::String^>(strcat(_ltoa(computer[i].cost, buff, 10), " Руб."));
			}
		}
		if (groupBox2->Visible == true)
		{
			pictureBox2->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[1].photo);
		}
		if (groupBox3->Visible == true)
		{
			pictureBox3->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[2].photo);
		}

		if (groupBox4->Visible == true)
		{
			pictureBox4->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[3].photo);
		}

		if (groupBox5->Visible == true)
		{
			pictureBox5->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[4].photo);
		}

		if (groupBox6->Visible == true)
		{
			pictureBox6->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[5].photo);
		}

		if (groupBox7->Visible == true)
		{
			pictureBox7->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[6].photo);
		}

		if (groupBox8->Visible == true)
		{
			pictureBox8->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[7].photo);
		}

	}

	private: System::Void Catalog_Load(System::Object^ sender, System::EventArgs^ e) {
		fileLength = getStoreFileLength(name, cpu, gpu, motherboard, ram1, ram2, psu1, psu2, hdd1, hdd2, ssd1, ssd2, cost1, cost2);
		Computers* computer;
		computer = getComputersList(name, cpu, gpu, motherboard, ram1, ram2, psu1, psu2, hdd1, hdd2, ssd1, ssd2, cost1, cost2);

		fillForm(computer);
		delete[8] computer;
	}

	private: System::Void controlFilter()
	{
		Computers* computer;
		if (checkFilter)
		{
			name = msclr::interop::marshal_as<std::string>(filterName->Text);
			cpu = msclr::interop::marshal_as<std::string>(filterCPU->Text);
			gpu = msclr::interop::marshal_as<std::string>(filterGPU->Text);
			motherboard = msclr::interop::marshal_as<std::string>(filterMotherboard->Text);
			ram1 = atoi(msclr::interop::marshal_as<std::string>(filterRAM1->Text).c_str());
			ram2 = atoi(msclr::interop::marshal_as<std::string>(filterRAM2->Text).c_str());
			hdd1 = atoi(msclr::interop::marshal_as<std::string>(filterHDD1->Text).c_str());
			hdd2 = atoi(msclr::interop::marshal_as<std::string>(filterHDD2->Text).c_str());
			ssd1 = atoi(msclr::interop::marshal_as<std::string>(filterSSD1->Text).c_str());
			ssd2 = atoi(msclr::interop::marshal_as<std::string>(filterSSD2->Text).c_str());
			psu1 = atoi(msclr::interop::marshal_as<std::string>(filterPSU1->Text).c_str());
			psu2 = atoi(msclr::interop::marshal_as<std::string>(filterPSU2->Text).c_str());
			cost1 = atol(msclr::interop::marshal_as<std::string>(filterCost1->Text).c_str());
			cost2 = atol(msclr::interop::marshal_as<std::string>(filterCost2->Text).c_str());
		}

		fileLength = getStoreFileLength(name, cpu, gpu, motherboard, ram1, ram2, psu1, psu2, hdd1, hdd2, ssd1, ssd2, cost1, cost2);
		computer = getComputersList(name, cpu, gpu, motherboard, ram1, ram2, psu1, psu2, hdd1, hdd2, ssd1, ssd2, cost1, cost2);

		fillForm(computer);
		delete[8] computer;
	}

	private: System::Void filterButton_Click(System::Object^ sender, System::EventArgs^ e) {
		Color color;
		if ((filterPSU1->ForeColor == color.Black) && (filterPSU2->ForeColor == color.Black) &&
			(filterCost1->ForeColor == color.Black) && (filterCost2->ForeColor == color.Black) || ((filterPSU1->Text == "")
				&& (filterPSU2->Text == "") && (filterCost1->Text == "") && (filterCost2->Text == "")))
		{
			checkFilter = true;
			storePage = 0;
			position = 0;
			indexRead = 0;
			controlFilter();
		}
		else
		{
			MessageBox::Show("В полях 'От' и 'До' не могут быть строковые входные данные!");
		}
	}
	private: System::Void Clear_Click(System::Object^ sender, System::EventArgs^ e) {
		checkFilter = false;
		filterName->Text = "";
		filterCPU->Text = "";
		filterGPU->Text = "";
		filterMotherboard->Text = "";
		filterPSU1->Text = "";
		filterPSU2->Text = "";
		filterRAM1->SelectedIndex = -1;
		filterRAM2->SelectedIndex = -1;
		filterHDD1->SelectedIndex = -1;
		filterHDD2->SelectedIndex = -1;
		filterSSD1->SelectedIndex = -1;
		filterSSD2->SelectedIndex = -1;
		filterCost1->Text = "";
		filterCost2->Text = "";
	}
	private: System::Void linkLabel2_LinkClicked(System::Object^ sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^ e) {
		storePage++;
		indexRead = 0;
		controlFilter();
	}
	private: System::Void linkLabel1_LinkClicked(System::Object^ sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^ e) {
		storePage--;
		if (storePage == 0)
		{
			position = 0;
			indexRead = 0;
		}
		else
		{
			indexRead = 1;
		}
		controlFilter();
	}

	private: System::Void appToBin(System::Object^ sender, System::EventArgs^ e)
	{
		System::Windows::Forms::Button^ button = (Button^)sender;
		int index = atoi(&msclr::interop::marshal_as<std::string>(button->Name)[6]) - 1;
		if (storePage == 0)
		{
			position = 0;
			indexRead = 0;
		}
		else
		{
			indexRead = 1;
		}
		Computers* computer;
		computer = getComputersListThisPage(name, cpu, gpu, motherboard, ram1, ram2, psu1, psu2, hdd1, hdd2, ssd1, ssd2, cost1, cost2);

		if (computer[index].amount == 0)
		{
			MessageBox::Show("Товара нет в наличии!");
		}
		else
		{
			addComputerToShoppingCart(computer[index], shoppingCartFile);
		}
		delete[8] computer;
	}
	private: System::Void forChangeFilterFields(System::Object^ sender, System::EventArgs^ e) {
		checkFilter = false;
	}

	private: System::Void filterElement_TextChanged(System::Object^ sender, System::EventArgs^ e) {
		System::Windows::Forms::TextBox^ textBox = (TextBox^)sender;
		Color color;
		if (checkNoNumber(msclr::interop::marshal_as<std::string>(textBox->Text)))
		{
			textBox->ForeColor = color.Red;
		}
		else
		{
			textBox->ForeColor = color.Black;
		}
	}

	private: System::Void linkLabel3_LinkClicked(System::Object^ sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^ e)
	{
		indexRead = 0;
		itemsOfThisPage = 0;
		position = 0;

		this->Hide();
		this->Owner->Show();
	}
	};
};