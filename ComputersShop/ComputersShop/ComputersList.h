#pragma once
#include <fstream>
#include <iostream>
#include <string>
#include <msclr/marshal_cppstd.h>

using namespace std;

unsigned int adminPage = 0;
unsigned int fileLength = 0;
streampos position = 0;
int indexRead = 0;
int itemsOfThisPage = 0;

const string PATH_CATALOG = "ComputersList.dat";

struct Computers
{
	char name[120];
	struct components
	{
		char gpu[80];
		char cpu[80];
		char motherboard[40];
		int ram;
		int psu;
		int ssd;
		int hdd;
	} component;
	unsigned int amount;
	long cost;
	char photo[200];
	unsigned short buyAmount = 1;
};

void createComputersFile(string path)
{
	if (!System::IO::File::Exists(msclr::interop::marshal_as<System::String^>(path)))
	{
		ofstream file(path, ios::binary);
		file.close();
	}
}
bool getComplianceWithConditions(Computers computer, string _name, string _cpu, string _gpu, string _motherboard, int _ram1, int _ram2, int _psu1, int _psu2, int _hdd1, int _hdd2, int _ssd1, int _ssd2, long _cost1, long _cost2)
{
	if ((_name == "") || (string(computer.name).find(_name) != -1))
	{
		if ((_cpu == "") || (string(computer.component.cpu).find(_cpu) != -1))
		{
			if ((_gpu == "") || (string(computer.component.gpu).find(_gpu) != -1))
			{
				if ((_motherboard == "") || (string(computer.component.motherboard).find(_motherboard) != -1))
				{
					if ((computer.component.psu >= _psu1) && ((_psu2 == 0) || (computer.component.psu <= _psu2)))
					{
						if ((computer.component.ram >= _ram1) && ((_ram2 == 0) || (computer.component.ram <= _ram2)))
						{
							if ((computer.component.hdd >= _hdd1) && ((_hdd2 == 0) || (computer.component.hdd <= _hdd2)))
							{
								if ((computer.component.ssd >= _ssd1) && ((_ssd2 == 0) || (computer.component.ssd <= _ssd2)))
								{
									if ((computer.cost >= _cost1) && ((_cost2 == 0) || (computer.cost <= _cost2)))
									{
										return true;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return false;
}

int getStoreFileLength(string _name, string _cpu, string _gpu, string _motherboard, int _ram1, int _ram2, int _psu1, int _psu2, int _hdd1, int _hdd2, int _ssd1, int _ssd2, long _cost1, long _cost2)
{
	createComputersFile(PATH_CATALOG);

	ifstream fromFile(PATH_CATALOG, ios::binary);

	unsigned int counter = 0;

	Computers computer;

	while (true)
	{
		fromFile.read((char*)&computer, sizeof(Computers));
		if (fromFile.eof())
		{
			break;
		}

		if (getComplianceWithConditions(computer, _name, _cpu, _gpu, _motherboard, _ram1, _ram2, _psu1, _psu2, _hdd1, _hdd2, _ssd1, _ssd2, _cost1, _cost2))
		{
			counter++;
		}

	}

	fromFile.close();

	return counter;
}

void addComputerToStore(string _name, string _cpu, string _gpu, string _motherboard, int _ram, int _psu, int _hdd, int _ssd, int _amount, long _cost, string _photo, int place)
{
	Computers computer;
	ofstream inFile(PATH_CATALOG, ios::binary | ios::in | ios::out);

	strcpy(computer.name, _name.c_str());
	strcpy(computer.component.cpu, _cpu.c_str());
	strcpy(computer.component.gpu, _gpu.c_str());
	strcpy(computer.component.motherboard, _motherboard.c_str());
	strcpy(computer.photo, _photo.c_str());
	computer.component.ram = _ram;
	computer.component.hdd = _hdd;
	computer.component.ssd = _ssd;
	computer.component.psu = _psu;
	computer.amount = _amount;
	computer.cost = _cost;

	inFile.seekp((place + adminPage * 8) * sizeof(Computers), ios::beg);

	inFile.write((char*)&computer, sizeof(Computers));

	inFile.close();
}

bool checkingExist(string _name, string filePath)
{
	createComputersFile(PATH_CATALOG);
	ifstream fromFile(filePath, ios::binary);
	Computers computer;
	bool check = false;

	while (true)
	{
		fromFile.read((char*)&computer, sizeof(Computers));
		if (fromFile.eof())
		{
			break;
		}
		if (computer.name == _name)
		{
			check = true;
			break;
		}
	}
	fromFile.close();
	return check;
}

Computers* getComputersList(string _name, string _cpu, string _gpu, string _motherboard, int _ram1, int _ram2, int _psu1, int _psu2, int _hdd1, int _hdd2, int _ssd1, int _ssd2, long _cost1, long _cost2)
{
	fstream fromFile(PATH_CATALOG, ios::binary | ios::in);

	streampos tempPosition;
	Computers* computer = new Computers[8];
	position -= indexRead * sizeof(Computers);
	fromFile.seekg(position, ios::beg);

	for (int i = 0 + 7 * indexRead; (indexRead == 0) ? (i < 8) : (i >= 0);)
	{
		fromFile.read((char*)&computer[i], sizeof(Computers));
		if (fromFile.eof())
		{
			strcpy(computer[i].name, "");
			break;
		}
		if (getComplianceWithConditions(computer[i], _name, _cpu, _gpu, _motherboard, _ram1, _ram2, _psu1, _psu2, _hdd1, _hdd2, _ssd1, _ssd2, _cost1, _cost2))
		{
			if (indexRead == 0)
			{
				i++;
			}
			else
			{
				if (itemsOfThisPage == 0)
				{
					i--;
				}
				else
				{
					tempPosition = fromFile.tellg();
					itemsOfThisPage--;
				}
			}
			position = fromFile.tellg();
		}
		if (indexRead == 1)
		{
			position -= 2 * sizeof(Computers);
			fromFile.seekg(position, ios::beg);
		}
	}
	fromFile.close();
	if (indexRead == 1)
	{
		position = tempPosition;
		position -= sizeof(Computers);
	}
	return computer;
}

Computers* getComputersListThisPage(string _name, string _cpu, string _gpu, string _motherboard, int _ram1, int _ram2, int _psu1, int _psu2, int _hdd1, int _hdd2, int _ssd1, int _ssd2, long _cost1, long _cost2)
{
	fstream fromFile(PATH_CATALOG, ios::binary | ios::in);

	Computers* computer = new Computers[8];

	streampos tempPosition = position;
	position -= indexRead * sizeof(Computers);
	fromFile.seekg(position, ios::beg);

	for (int i = 0 + (itemsOfThisPage - 1) * indexRead; (indexRead == 0) ? (i < 8) : (i >= 0);)
	{
		fromFile.read((char*)&computer[i], sizeof(Computers));
		if (fromFile.eof())
		{
			strcpy(computer[i].name, "");
			break;
		}
		if (getComplianceWithConditions(computer[i], _name, _cpu, _gpu, _motherboard, _ram1, _ram2, _psu1, _psu2, _hdd1, _hdd2, _ssd1, _ssd2, _cost1, _cost2))
		{
			if (indexRead == 0)
			{
				i++;
			}
			else
			{
				i--;
			}
			position = fromFile.tellg();
		}
		else
		{
			strcpy(computer[i].name, "");
		}
		if (indexRead == 1)
		{
			position -= 2 * sizeof(Computers);
			fromFile.seekg(position, ios::beg);
		}
	}
	fromFile.close();
	if (indexRead == 1)
	{
		position = tempPosition;
	}
	return computer;
}