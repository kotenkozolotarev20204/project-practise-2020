﻿#pragma once

#include <stdlib.h>
#include <msclr/marshal_cppstd.h>
#include <string>
#include "SimpleFunctions.h"
#include "ShoppingCart.h"
#include "SimpleFunctions.h"

namespace ComputersShop {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Сводка для Catalog
	/// </summary>
	public ref class ChangeSpecs : public System::Windows::Forms::Form
	{
	public:
		ChangeSpecs(void)
		{
			InitializeComponent();
			//
			//TODO: добавьте код конструктора
			//
		}

	protected:
		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		~ChangeSpecs()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:

	private: System::Windows::Forms::TextBox^ AmountOrder0;

	private: System::Windows::Forms::Button^ button10;


	private: System::Windows::Forms::Button^ button9;

	private: System::Windows::Forms::Label^ label9;
	private: System::Windows::Forms::Button^ button1;

	private: System::Windows::Forms::Label^ label8;

	private: System::Windows::Forms::Label^ label7;

	private: System::Windows::Forms::Label^ label6;

	private: System::Windows::Forms::Label^ label5;

	private: System::Windows::Forms::Label^ label4;

	private: System::Windows::Forms::Label^ label3;

	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::GroupBox^ groupBox1;
	private: System::Windows::Forms::PictureBox^ pictureBox1;

	private: System::Windows::Forms::Label^ label1;

	private: System::Windows::Forms::LinkLabel^ linkLabel1;
	private: System::Windows::Forms::LinkLabel^ linkLabel2;

	private: System::Windows::Forms::Label^ label15;

	private: System::Windows::Forms::Label^ label19;

	private: System::Windows::Forms::TextBox^ textBox4;
	private: System::Windows::Forms::TextBox^ textBox3;
	private: System::Windows::Forms::TextBox^ textBox2;
	private: System::Windows::Forms::TextBox^ textBox1;
	private: System::Windows::Forms::ComboBox^ comboBox1;
	private: System::Windows::Forms::ComboBox^ comboBox2;
	private: System::Windows::Forms::ComboBox^ comboBox3;
	private: System::Windows::Forms::TextBox^ textBox6;
	private: System::Windows::Forms::TextBox^ textBox5;
	private: System::Windows::Forms::GroupBox^ groupBox2;
	private: System::Windows::Forms::TextBox^ textBox7;
	private: System::Windows::Forms::TextBox^ textBox8;
	private: System::Windows::Forms::ComboBox^ comboBox4;
	private: System::Windows::Forms::ComboBox^ comboBox5;
	private: System::Windows::Forms::ComboBox^ comboBox6;
	private: System::Windows::Forms::TextBox^ textBox9;
	private: System::Windows::Forms::TextBox^ textBox10;
	private: System::Windows::Forms::TextBox^ textBox11;
	private: System::Windows::Forms::TextBox^ textBox12;
	private: System::Windows::Forms::Label^ label10;
	private: System::Windows::Forms::Label^ label11;
	private: System::Windows::Forms::TextBox^ AmountOrder1;

	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Button^ button11;
	private: System::Windows::Forms::Label^ label12;
	private: System::Windows::Forms::Button^ button12;
	private: System::Windows::Forms::Label^ label13;
	private: System::Windows::Forms::Label^ label14;
	private: System::Windows::Forms::Label^ label16;
	private: System::Windows::Forms::Label^ label18;
	private: System::Windows::Forms::Label^ label20;
	private: System::Windows::Forms::Label^ label22;
	private: System::Windows::Forms::Label^ label27;
	private: System::Windows::Forms::PictureBox^ pictureBox2;
	private: System::Windows::Forms::GroupBox^ groupBox3;
	private: System::Windows::Forms::TextBox^ textBox14;
	private: System::Windows::Forms::TextBox^ textBox15;
	private: System::Windows::Forms::ComboBox^ comboBox7;
	private: System::Windows::Forms::ComboBox^ comboBox8;
	private: System::Windows::Forms::ComboBox^ comboBox9;
	private: System::Windows::Forms::TextBox^ textBox16;
	private: System::Windows::Forms::TextBox^ textBox17;
	private: System::Windows::Forms::TextBox^ textBox18;
	private: System::Windows::Forms::TextBox^ textBox19;
	private: System::Windows::Forms::Label^ label17;
	private: System::Windows::Forms::Label^ label23;
	private: System::Windows::Forms::TextBox^ AmountOrder2;

	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::Button^ button13;
	private: System::Windows::Forms::Label^ label24;
	private: System::Windows::Forms::Button^ button14;
	private: System::Windows::Forms::Label^ label26;
	private: System::Windows::Forms::Label^ label28;
	private: System::Windows::Forms::Label^ label30;
	private: System::Windows::Forms::Label^ label31;
	private: System::Windows::Forms::Label^ label32;
	private: System::Windows::Forms::Label^ label34;
	private: System::Windows::Forms::Label^ label35;
	private: System::Windows::Forms::PictureBox^ pictureBox3;
	private: System::Windows::Forms::GroupBox^ groupBox4;
	private: System::Windows::Forms::TextBox^ textBox21;
	private: System::Windows::Forms::TextBox^ textBox22;
	private: System::Windows::Forms::ComboBox^ comboBox10;
	private: System::Windows::Forms::ComboBox^ comboBox11;
	private: System::Windows::Forms::ComboBox^ comboBox12;
	private: System::Windows::Forms::TextBox^ textBox23;
	private: System::Windows::Forms::TextBox^ textBox24;
	private: System::Windows::Forms::TextBox^ textBox25;
	private: System::Windows::Forms::TextBox^ textBox26;
	private: System::Windows::Forms::Label^ label21;
	private: System::Windows::Forms::Label^ label36;
	private: System::Windows::Forms::TextBox^ AmountOrder3;

	private: System::Windows::Forms::Button^ button4;
	private: System::Windows::Forms::Button^ button15;
	private: System::Windows::Forms::Label^ label38;
	private: System::Windows::Forms::Button^ button16;
	private: System::Windows::Forms::Label^ label39;
	private: System::Windows::Forms::Label^ label40;
	private: System::Windows::Forms::Label^ label41;
	private: System::Windows::Forms::Label^ label42;
	private: System::Windows::Forms::Label^ label43;
	private: System::Windows::Forms::Label^ label44;
	private: System::Windows::Forms::Label^ label45;
	private: System::Windows::Forms::PictureBox^ pictureBox4;
	private: System::Windows::Forms::GroupBox^ groupBox5;
	private: System::Windows::Forms::TextBox^ textBox28;
	private: System::Windows::Forms::TextBox^ textBox29;
	private: System::Windows::Forms::ComboBox^ comboBox13;
	private: System::Windows::Forms::ComboBox^ comboBox14;
	private: System::Windows::Forms::ComboBox^ comboBox15;
	private: System::Windows::Forms::TextBox^ textBox30;
	private: System::Windows::Forms::TextBox^ textBox31;
	private: System::Windows::Forms::TextBox^ textBox32;
	private: System::Windows::Forms::TextBox^ textBox33;
	private: System::Windows::Forms::Label^ label25;
	private: System::Windows::Forms::Label^ label29;
	private: System::Windows::Forms::TextBox^ AmountOrder4;

	private: System::Windows::Forms::Button^ button5;
	private: System::Windows::Forms::Button^ button6;
	private: System::Windows::Forms::Label^ label46;
	private: System::Windows::Forms::Button^ button17;
	private: System::Windows::Forms::Label^ label47;
	private: System::Windows::Forms::Label^ label48;
	private: System::Windows::Forms::Label^ label49;
	private: System::Windows::Forms::Label^ label50;
	private: System::Windows::Forms::Label^ label51;
	private: System::Windows::Forms::Label^ label52;
	private: System::Windows::Forms::Label^ label53;
	private: System::Windows::Forms::PictureBox^ pictureBox5;
	private: System::Windows::Forms::GroupBox^ groupBox6;
	private: System::Windows::Forms::TextBox^ textBox35;
	private: System::Windows::Forms::TextBox^ textBox36;
	private: System::Windows::Forms::ComboBox^ comboBox16;
	private: System::Windows::Forms::ComboBox^ comboBox17;
	private: System::Windows::Forms::ComboBox^ comboBox18;
	private: System::Windows::Forms::TextBox^ textBox37;
	private: System::Windows::Forms::TextBox^ textBox38;
	private: System::Windows::Forms::TextBox^ textBox39;
	private: System::Windows::Forms::TextBox^ textBox40;
	private: System::Windows::Forms::Label^ label54;
	private: System::Windows::Forms::Label^ label55;
	private: System::Windows::Forms::TextBox^ AmountOrder5;

	private: System::Windows::Forms::Button^ button18;
	private: System::Windows::Forms::Button^ button19;
	private: System::Windows::Forms::Label^ label56;
	private: System::Windows::Forms::Button^ button20;
	private: System::Windows::Forms::Label^ label57;
	private: System::Windows::Forms::Label^ label58;
	private: System::Windows::Forms::Label^ label59;
	private: System::Windows::Forms::Label^ label60;
	private: System::Windows::Forms::Label^ label61;
	private: System::Windows::Forms::Label^ label62;
	private: System::Windows::Forms::Label^ label63;
	private: System::Windows::Forms::PictureBox^ pictureBox6;
	private: System::Windows::Forms::GroupBox^ groupBox7;
	private: System::Windows::Forms::TextBox^ textBox42;
	private: System::Windows::Forms::TextBox^ textBox43;
	private: System::Windows::Forms::ComboBox^ comboBox19;
	private: System::Windows::Forms::ComboBox^ comboBox20;
	private: System::Windows::Forms::ComboBox^ comboBox21;
	private: System::Windows::Forms::TextBox^ textBox44;
	private: System::Windows::Forms::TextBox^ textBox45;
	private: System::Windows::Forms::TextBox^ textBox46;
	private: System::Windows::Forms::TextBox^ textBox47;
	private: System::Windows::Forms::Label^ label33;
	private: System::Windows::Forms::Label^ label64;
	private: System::Windows::Forms::TextBox^ AmountOrder6;

	private: System::Windows::Forms::Button^ button7;
	private: System::Windows::Forms::Button^ button21;
	private: System::Windows::Forms::Label^ label65;
	private: System::Windows::Forms::Button^ button22;
	private: System::Windows::Forms::Label^ label66;
	private: System::Windows::Forms::Label^ label67;
	private: System::Windows::Forms::Label^ label68;
	private: System::Windows::Forms::Label^ label69;
	private: System::Windows::Forms::Label^ label70;
	private: System::Windows::Forms::Label^ label71;
	private: System::Windows::Forms::Label^ label72;
	private: System::Windows::Forms::PictureBox^ pictureBox7;
	private: System::Windows::Forms::GroupBox^ groupBox8;
	private: System::Windows::Forms::TextBox^ textBox49;
	private: System::Windows::Forms::TextBox^ textBox50;
	private: System::Windows::Forms::ComboBox^ comboBox22;
	private: System::Windows::Forms::ComboBox^ comboBox23;
	private: System::Windows::Forms::ComboBox^ comboBox24;
	private: System::Windows::Forms::TextBox^ textBox51;
	private: System::Windows::Forms::TextBox^ textBox52;
	private: System::Windows::Forms::TextBox^ textBox53;
	private: System::Windows::Forms::TextBox^ textBox54;
	private: System::Windows::Forms::Label^ label37;
	private: System::Windows::Forms::Label^ label73;
	private: System::Windows::Forms::TextBox^ AmountOrder7;

	private: System::Windows::Forms::Button^ button8;
	private: System::Windows::Forms::Button^ button23;
	private: System::Windows::Forms::Label^ label74;
	private: System::Windows::Forms::Button^ button24;
	private: System::Windows::Forms::Label^ label75;
	private: System::Windows::Forms::Label^ label76;
	private: System::Windows::Forms::Label^ label77;
	private: System::Windows::Forms::Label^ label78;
	private: System::Windows::Forms::Label^ label79;
	private: System::Windows::Forms::Label^ label80;
	private: System::Windows::Forms::Label^ label81;
	private: System::Windows::Forms::PictureBox^ pictureBox8;
	private: System::Windows::Forms::OpenFileDialog^ openFileDialog1;
	private: System::Windows::Forms::Button^ button25;
	private: System::Windows::Forms::Button^ button26;
	private: System::Windows::Forms::Button^ button27;
	private: System::Windows::Forms::Button^ button28;
	private: System::Windows::Forms::Button^ button29;
	private: System::Windows::Forms::Button^ button30;
	private: System::Windows::Forms::Button^ button31;
	private: System::Windows::Forms::Button^ button32;
private: System::Windows::Forms::LinkLabel^ linkLabel3;


	protected:


	private:
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		void InitializeComponent(void)
		{
			this->AmountOrder0 = (gcnew System::Windows::Forms::TextBox());
			this->button10 = (gcnew System::Windows::Forms::Button());
			this->button9 = (gcnew System::Windows::Forms::Button());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->button25 = (gcnew System::Windows::Forms::Button());
			this->textBox6 = (gcnew System::Windows::Forms::TextBox());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->comboBox3 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox2 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->linkLabel1 = (gcnew System::Windows::Forms::LinkLabel());
			this->linkLabel2 = (gcnew System::Windows::Forms::LinkLabel());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->button26 = (gcnew System::Windows::Forms::Button());
			this->textBox7 = (gcnew System::Windows::Forms::TextBox());
			this->textBox8 = (gcnew System::Windows::Forms::TextBox());
			this->comboBox4 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox5 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox6 = (gcnew System::Windows::Forms::ComboBox());
			this->textBox9 = (gcnew System::Windows::Forms::TextBox());
			this->textBox10 = (gcnew System::Windows::Forms::TextBox());
			this->textBox11 = (gcnew System::Windows::Forms::TextBox());
			this->textBox12 = (gcnew System::Windows::Forms::TextBox());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->AmountOrder1 = (gcnew System::Windows::Forms::TextBox());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button11 = (gcnew System::Windows::Forms::Button());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->button12 = (gcnew System::Windows::Forms::Button());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->label22 = (gcnew System::Windows::Forms::Label());
			this->label27 = (gcnew System::Windows::Forms::Label());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->button27 = (gcnew System::Windows::Forms::Button());
			this->textBox14 = (gcnew System::Windows::Forms::TextBox());
			this->textBox15 = (gcnew System::Windows::Forms::TextBox());
			this->comboBox7 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox8 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox9 = (gcnew System::Windows::Forms::ComboBox());
			this->textBox16 = (gcnew System::Windows::Forms::TextBox());
			this->textBox17 = (gcnew System::Windows::Forms::TextBox());
			this->textBox18 = (gcnew System::Windows::Forms::TextBox());
			this->textBox19 = (gcnew System::Windows::Forms::TextBox());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->label23 = (gcnew System::Windows::Forms::Label());
			this->AmountOrder2 = (gcnew System::Windows::Forms::TextBox());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button13 = (gcnew System::Windows::Forms::Button());
			this->label24 = (gcnew System::Windows::Forms::Label());
			this->button14 = (gcnew System::Windows::Forms::Button());
			this->label26 = (gcnew System::Windows::Forms::Label());
			this->label28 = (gcnew System::Windows::Forms::Label());
			this->label30 = (gcnew System::Windows::Forms::Label());
			this->label31 = (gcnew System::Windows::Forms::Label());
			this->label32 = (gcnew System::Windows::Forms::Label());
			this->label34 = (gcnew System::Windows::Forms::Label());
			this->label35 = (gcnew System::Windows::Forms::Label());
			this->pictureBox3 = (gcnew System::Windows::Forms::PictureBox());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->button28 = (gcnew System::Windows::Forms::Button());
			this->textBox21 = (gcnew System::Windows::Forms::TextBox());
			this->textBox22 = (gcnew System::Windows::Forms::TextBox());
			this->comboBox10 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox11 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox12 = (gcnew System::Windows::Forms::ComboBox());
			this->textBox23 = (gcnew System::Windows::Forms::TextBox());
			this->textBox24 = (gcnew System::Windows::Forms::TextBox());
			this->textBox25 = (gcnew System::Windows::Forms::TextBox());
			this->textBox26 = (gcnew System::Windows::Forms::TextBox());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->label36 = (gcnew System::Windows::Forms::Label());
			this->AmountOrder3 = (gcnew System::Windows::Forms::TextBox());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button15 = (gcnew System::Windows::Forms::Button());
			this->label38 = (gcnew System::Windows::Forms::Label());
			this->button16 = (gcnew System::Windows::Forms::Button());
			this->label39 = (gcnew System::Windows::Forms::Label());
			this->label40 = (gcnew System::Windows::Forms::Label());
			this->label41 = (gcnew System::Windows::Forms::Label());
			this->label42 = (gcnew System::Windows::Forms::Label());
			this->label43 = (gcnew System::Windows::Forms::Label());
			this->label44 = (gcnew System::Windows::Forms::Label());
			this->label45 = (gcnew System::Windows::Forms::Label());
			this->pictureBox4 = (gcnew System::Windows::Forms::PictureBox());
			this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
			this->button29 = (gcnew System::Windows::Forms::Button());
			this->textBox28 = (gcnew System::Windows::Forms::TextBox());
			this->textBox29 = (gcnew System::Windows::Forms::TextBox());
			this->comboBox13 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox14 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox15 = (gcnew System::Windows::Forms::ComboBox());
			this->textBox30 = (gcnew System::Windows::Forms::TextBox());
			this->textBox31 = (gcnew System::Windows::Forms::TextBox());
			this->textBox32 = (gcnew System::Windows::Forms::TextBox());
			this->textBox33 = (gcnew System::Windows::Forms::TextBox());
			this->label25 = (gcnew System::Windows::Forms::Label());
			this->label29 = (gcnew System::Windows::Forms::Label());
			this->AmountOrder4 = (gcnew System::Windows::Forms::TextBox());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->label46 = (gcnew System::Windows::Forms::Label());
			this->button17 = (gcnew System::Windows::Forms::Button());
			this->label47 = (gcnew System::Windows::Forms::Label());
			this->label48 = (gcnew System::Windows::Forms::Label());
			this->label49 = (gcnew System::Windows::Forms::Label());
			this->label50 = (gcnew System::Windows::Forms::Label());
			this->label51 = (gcnew System::Windows::Forms::Label());
			this->label52 = (gcnew System::Windows::Forms::Label());
			this->label53 = (gcnew System::Windows::Forms::Label());
			this->pictureBox5 = (gcnew System::Windows::Forms::PictureBox());
			this->groupBox6 = (gcnew System::Windows::Forms::GroupBox());
			this->button30 = (gcnew System::Windows::Forms::Button());
			this->textBox35 = (gcnew System::Windows::Forms::TextBox());
			this->textBox36 = (gcnew System::Windows::Forms::TextBox());
			this->comboBox16 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox17 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox18 = (gcnew System::Windows::Forms::ComboBox());
			this->textBox37 = (gcnew System::Windows::Forms::TextBox());
			this->textBox38 = (gcnew System::Windows::Forms::TextBox());
			this->textBox39 = (gcnew System::Windows::Forms::TextBox());
			this->textBox40 = (gcnew System::Windows::Forms::TextBox());
			this->label54 = (gcnew System::Windows::Forms::Label());
			this->label55 = (gcnew System::Windows::Forms::Label());
			this->AmountOrder5 = (gcnew System::Windows::Forms::TextBox());
			this->button18 = (gcnew System::Windows::Forms::Button());
			this->button19 = (gcnew System::Windows::Forms::Button());
			this->label56 = (gcnew System::Windows::Forms::Label());
			this->button20 = (gcnew System::Windows::Forms::Button());
			this->label57 = (gcnew System::Windows::Forms::Label());
			this->label58 = (gcnew System::Windows::Forms::Label());
			this->label59 = (gcnew System::Windows::Forms::Label());
			this->label60 = (gcnew System::Windows::Forms::Label());
			this->label61 = (gcnew System::Windows::Forms::Label());
			this->label62 = (gcnew System::Windows::Forms::Label());
			this->label63 = (gcnew System::Windows::Forms::Label());
			this->pictureBox6 = (gcnew System::Windows::Forms::PictureBox());
			this->groupBox7 = (gcnew System::Windows::Forms::GroupBox());
			this->button31 = (gcnew System::Windows::Forms::Button());
			this->textBox42 = (gcnew System::Windows::Forms::TextBox());
			this->textBox43 = (gcnew System::Windows::Forms::TextBox());
			this->comboBox19 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox20 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox21 = (gcnew System::Windows::Forms::ComboBox());
			this->textBox44 = (gcnew System::Windows::Forms::TextBox());
			this->textBox45 = (gcnew System::Windows::Forms::TextBox());
			this->textBox46 = (gcnew System::Windows::Forms::TextBox());
			this->textBox47 = (gcnew System::Windows::Forms::TextBox());
			this->label33 = (gcnew System::Windows::Forms::Label());
			this->label64 = (gcnew System::Windows::Forms::Label());
			this->AmountOrder6 = (gcnew System::Windows::Forms::TextBox());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->button21 = (gcnew System::Windows::Forms::Button());
			this->label65 = (gcnew System::Windows::Forms::Label());
			this->button22 = (gcnew System::Windows::Forms::Button());
			this->label66 = (gcnew System::Windows::Forms::Label());
			this->label67 = (gcnew System::Windows::Forms::Label());
			this->label68 = (gcnew System::Windows::Forms::Label());
			this->label69 = (gcnew System::Windows::Forms::Label());
			this->label70 = (gcnew System::Windows::Forms::Label());
			this->label71 = (gcnew System::Windows::Forms::Label());
			this->label72 = (gcnew System::Windows::Forms::Label());
			this->pictureBox7 = (gcnew System::Windows::Forms::PictureBox());
			this->groupBox8 = (gcnew System::Windows::Forms::GroupBox());
			this->button32 = (gcnew System::Windows::Forms::Button());
			this->textBox49 = (gcnew System::Windows::Forms::TextBox());
			this->textBox50 = (gcnew System::Windows::Forms::TextBox());
			this->comboBox22 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox23 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox24 = (gcnew System::Windows::Forms::ComboBox());
			this->textBox51 = (gcnew System::Windows::Forms::TextBox());
			this->textBox52 = (gcnew System::Windows::Forms::TextBox());
			this->textBox53 = (gcnew System::Windows::Forms::TextBox());
			this->textBox54 = (gcnew System::Windows::Forms::TextBox());
			this->label37 = (gcnew System::Windows::Forms::Label());
			this->label73 = (gcnew System::Windows::Forms::Label());
			this->AmountOrder7 = (gcnew System::Windows::Forms::TextBox());
			this->button8 = (gcnew System::Windows::Forms::Button());
			this->button23 = (gcnew System::Windows::Forms::Button());
			this->label74 = (gcnew System::Windows::Forms::Label());
			this->button24 = (gcnew System::Windows::Forms::Button());
			this->label75 = (gcnew System::Windows::Forms::Label());
			this->label76 = (gcnew System::Windows::Forms::Label());
			this->label77 = (gcnew System::Windows::Forms::Label());
			this->label78 = (gcnew System::Windows::Forms::Label());
			this->label79 = (gcnew System::Windows::Forms::Label());
			this->label80 = (gcnew System::Windows::Forms::Label());
			this->label81 = (gcnew System::Windows::Forms::Label());
			this->pictureBox8 = (gcnew System::Windows::Forms::PictureBox());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->linkLabel3 = (gcnew System::Windows::Forms::LinkLabel());
			this->groupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->groupBox2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			this->groupBox3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->BeginInit();
			this->groupBox4->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->BeginInit();
			this->groupBox5->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox5))->BeginInit();
			this->groupBox6->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox6))->BeginInit();
			this->groupBox7->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox7))->BeginInit();
			this->groupBox8->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox8))->BeginInit();
			this->SuspendLayout();
			// 
			// AmountOrder0
			// 
			this->AmountOrder0->ForeColor = System::Drawing::Color::Black;
			this->AmountOrder0->Location = System::Drawing::Point(667, 137);
			this->AmountOrder0->Margin = System::Windows::Forms::Padding(2);
			this->AmountOrder0->MaxLength = 3;
			this->AmountOrder0->Name = L"AmountOrder0";
			this->AmountOrder0->Size = System::Drawing::Size(80, 20);
			this->AmountOrder0->TabIndex = 20;
			this->AmountOrder0->Text = L"1";
			this->AmountOrder0->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->AmountOrder0->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// button10
			// 
			this->button10->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button10->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button10->Location = System::Drawing::Point(751, 137);
			this->button10->Margin = System::Windows::Forms::Padding(2);
			this->button10->Name = L"button10";
			this->button10->Size = System::Drawing::Size(22, 20);
			this->button10->TabIndex = 19;
			this->button10->Text = L"+";
			this->button10->UseVisualStyleBackColor = true;
			this->button10->Click += gcnew System::EventHandler(this, &ChangeSpecs::button10_Click);
			// 
			// button9
			// 
			this->button9->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button9->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button9->Location = System::Drawing::Point(641, 137);
			this->button9->Margin = System::Windows::Forms::Padding(2);
			this->button9->Name = L"button9";
			this->button9->Size = System::Drawing::Size(22, 20);
			this->button9->TabIndex = 18;
			this->button9->Text = L"-";
			this->button9->UseVisualStyleBackColor = true;
			this->button9->Click += gcnew System::EventHandler(this, &ChangeSpecs::button9_Click);
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(525, 140);
			this->label9->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(69, 13);
			this->label9->TabIndex = 16;
			this->label9->Text = L"Количество:";
			// 
			// button1
			// 
			this->button1->BackColor = System::Drawing::SystemColors::MenuHighlight;
			this->button1->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button1->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button1->Location = System::Drawing::Point(363, 172);
			this->button1->Margin = System::Windows::Forms::Padding(2);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(125, 39);
			this->button1->TabIndex = 15;
			this->button1->Text = L"Изменить";
			this->button1->UseVisualStyleBackColor = false;
			this->button1->Click += gcnew System::EventHandler(this, &ChangeSpecs::button1_Click);
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label8->Location = System::Drawing::Point(525, 27);
			this->label8->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(36, 13);
			this->label8->TabIndex = 13;
			this->label8->Text = L"Цена:";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(525, 84);
			this->label7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(72, 13);
			this->label7->TabIndex = 11;
			this->label7->Text = L"Объём HDD:";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(218, 115);
			this->label6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(110, 13);
			this->label6->TabIndex = 9;
			this->label6->Text = L"Материнская плата:";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(525, 55);
			this->label5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(71, 13);
			this->label5->TabIndex = 7;
			this->label5->Text = L"Объём ОЗУ:";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(218, 87);
			this->label4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(70, 13);
			this->label4->TabIndex = 5;
			this->label4->Text = L"Видеокарта:";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(218, 58);
			this->label3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(66, 13);
			this->label3->TabIndex = 3;
			this->label3->Text = L"Процессор:";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(217, 27);
			this->label2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(60, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Название:";
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->button25);
			this->groupBox1->Controls->Add(this->textBox6);
			this->groupBox1->Controls->Add(this->textBox5);
			this->groupBox1->Controls->Add(this->comboBox3);
			this->groupBox1->Controls->Add(this->comboBox2);
			this->groupBox1->Controls->Add(this->comboBox1);
			this->groupBox1->Controls->Add(this->textBox4);
			this->groupBox1->Controls->Add(this->textBox3);
			this->groupBox1->Controls->Add(this->textBox2);
			this->groupBox1->Controls->Add(this->textBox1);
			this->groupBox1->Controls->Add(this->label15);
			this->groupBox1->Controls->Add(this->label19);
			this->groupBox1->Controls->Add(this->AmountOrder0);
			this->groupBox1->Controls->Add(this->button10);
			this->groupBox1->Controls->Add(this->button9);
			this->groupBox1->Controls->Add(this->label9);
			this->groupBox1->Controls->Add(this->button1);
			this->groupBox1->Controls->Add(this->label8);
			this->groupBox1->Controls->Add(this->label7);
			this->groupBox1->Controls->Add(this->label6);
			this->groupBox1->Controls->Add(this->label5);
			this->groupBox1->Controls->Add(this->label4);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->pictureBox1);
			this->groupBox1->Location = System::Drawing::Point(19, 49);
			this->groupBox1->Margin = System::Windows::Forms::Padding(2);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Padding = System::Windows::Forms::Padding(2);
			this->groupBox1->Size = System::Drawing::Size(782, 222);
			this->groupBox1->TabIndex = 28;
			this->groupBox1->TabStop = false;
			// 
			// button25
			// 
			this->button25->BackColor = System::Drawing::Color::Red;
			this->button25->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button25->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button25->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button25->Location = System::Drawing::Point(528, 172);
			this->button25->Margin = System::Windows::Forms::Padding(2);
			this->button25->Name = L"button25";
			this->button25->Size = System::Drawing::Size(125, 39);
			this->button25->TabIndex = 40;
			this->button25->Text = L"Удалить";
			this->button25->UseVisualStyleBackColor = false;
			this->button25->Click += gcnew System::EventHandler(this, &ChangeSpecs::button25_Click);
			// 
			// textBox6
			// 
			this->textBox6->Location = System::Drawing::Point(332, 24);
			this->textBox6->Name = L"textBox6";
			this->textBox6->Size = System::Drawing::Size(120, 20);
			this->textBox6->TabIndex = 39;
			// 
			// textBox5
			// 
			this->textBox5->Location = System::Drawing::Point(655, 24);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(106, 20);
			this->textBox5->TabIndex = 38;
			this->textBox5->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// comboBox3
			// 
			this->comboBox3->AutoCompleteCustomSource->AddRange(gcnew cli::array< System::String^  >(7) {
				L"64", L"128", L"240", L"256",
					L"512", L"1024", L"2048"
			});
			this->comboBox3->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox3->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox3->FormattingEnabled = true;
			this->comboBox3->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"64", L"128", L"240", L"256", L"512", L"1024",
					L"2048"
			});
			this->comboBox3->Location = System::Drawing::Point(655, 107);
			this->comboBox3->Name = L"comboBox3";
			this->comboBox3->Size = System::Drawing::Size(106, 21);
			this->comboBox3->TabIndex = 37;
			// 
			// comboBox2
			// 
			this->comboBox2->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox2->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox2->FormattingEnabled = true;
			this->comboBox2->Items->AddRange(gcnew cli::array< System::Object^  >(9) {
				L"32", L"64", L"128", L"256", L"512", L"1024",
					L"2048", L"4096", L"8192"
			});
			this->comboBox2->Location = System::Drawing::Point(655, 80);
			this->comboBox2->Name = L"comboBox2";
			this->comboBox2->Size = System::Drawing::Size(106, 21);
			this->comboBox2->TabIndex = 36;
			// 
			// comboBox1
			// 
			this->comboBox1->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Items->AddRange(gcnew cli::array< System::Object^  >(8) { L"2", L"4", L"6", L"8", L"16", L"32", L"64", L"128" });
			this->comboBox1->Location = System::Drawing::Point(655, 51);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(106, 21);
			this->comboBox1->TabIndex = 35;
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(332, 140);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(120, 20);
			this->textBox4->TabIndex = 30;
			this->textBox4->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(332, 111);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(120, 20);
			this->textBox3->TabIndex = 29;
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(332, 84);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(120, 20);
			this->textBox2->TabIndex = 28;
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(332, 54);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(120, 20);
			this->textBox1->TabIndex = 27;
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(525, 112);
			this->label15->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(70, 13);
			this->label15->TabIndex = 25;
			this->label15->Text = L"Объём SSD:";
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(218, 143);
			this->label19->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(81, 13);
			this->label19->TabIndex = 23;
			this->label19->Text = L"Мощность БП:";
			// 
			// pictureBox1
			// 
			this->pictureBox1->Cursor = System::Windows::Forms::Cursors::Hand;
			this->pictureBox1->Location = System::Drawing::Point(15, 16);
			this->pictureBox1->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(180, 195);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			this->pictureBox1->Click += gcnew System::EventHandler(this, &ChangeSpecs::pictureBox_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 20, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label1->Location = System::Drawing::Point(28, 13);
			this->label1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(114, 31);
			this->label1->TabIndex = 30;
			this->label1->Text = L"Каталог";
			// 
			// linkLabel1
			// 
			this->linkLabel1->AutoSize = true;
			this->linkLabel1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->linkLabel1->Location = System::Drawing::Point(16, 1881);
			this->linkLabel1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->linkLabel1->Name = L"linkLabel1";
			this->linkLabel1->Size = System::Drawing::Size(112, 17);
			this->linkLabel1->TabIndex = 41;
			this->linkLabel1->TabStop = true;
			this->linkLabel1->Text = L"← Предыдущая";
			this->linkLabel1->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &ChangeSpecs::linkLabel1_LinkClicked);
			// 
			// linkLabel2
			// 
			this->linkLabel2->AutoSize = true;
			this->linkLabel2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->linkLabel2->Location = System::Drawing::Point(698, 1881);
			this->linkLabel2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->linkLabel2->Name = L"linkLabel2";
			this->linkLabel2->Size = System::Drawing::Size(103, 17);
			this->linkLabel2->TabIndex = 42;
			this->linkLabel2->TabStop = true;
			this->linkLabel2->Text = L"Следующая →";
			this->linkLabel2->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &ChangeSpecs::linkLabel2_LinkClicked);
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->button26);
			this->groupBox2->Controls->Add(this->textBox7);
			this->groupBox2->Controls->Add(this->textBox8);
			this->groupBox2->Controls->Add(this->comboBox4);
			this->groupBox2->Controls->Add(this->comboBox5);
			this->groupBox2->Controls->Add(this->comboBox6);
			this->groupBox2->Controls->Add(this->textBox9);
			this->groupBox2->Controls->Add(this->textBox10);
			this->groupBox2->Controls->Add(this->textBox11);
			this->groupBox2->Controls->Add(this->textBox12);
			this->groupBox2->Controls->Add(this->label10);
			this->groupBox2->Controls->Add(this->label11);
			this->groupBox2->Controls->Add(this->AmountOrder1);
			this->groupBox2->Controls->Add(this->button2);
			this->groupBox2->Controls->Add(this->button11);
			this->groupBox2->Controls->Add(this->label12);
			this->groupBox2->Controls->Add(this->button12);
			this->groupBox2->Controls->Add(this->label13);
			this->groupBox2->Controls->Add(this->label14);
			this->groupBox2->Controls->Add(this->label16);
			this->groupBox2->Controls->Add(this->label18);
			this->groupBox2->Controls->Add(this->label20);
			this->groupBox2->Controls->Add(this->label22);
			this->groupBox2->Controls->Add(this->label27);
			this->groupBox2->Controls->Add(this->pictureBox2);
			this->groupBox2->Location = System::Drawing::Point(19, 278);
			this->groupBox2->Margin = System::Windows::Forms::Padding(2);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Padding = System::Windows::Forms::Padding(2);
			this->groupBox2->Size = System::Drawing::Size(782, 222);
			this->groupBox2->TabIndex = 43;
			this->groupBox2->TabStop = false;
			this->groupBox2->Visible = false;
			// 
			// button26
			// 
			this->button26->BackColor = System::Drawing::Color::Red;
			this->button26->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button26->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button26->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button26->Location = System::Drawing::Point(529, 172);
			this->button26->Margin = System::Windows::Forms::Padding(2);
			this->button26->Name = L"button26";
			this->button26->Size = System::Drawing::Size(125, 39);
			this->button26->TabIndex = 41;
			this->button26->Text = L"Удалить";
			this->button26->UseVisualStyleBackColor = false;
			this->button26->Click += gcnew System::EventHandler(this, &ChangeSpecs::button26_Click);
			// 
			// textBox7
			// 
			this->textBox7->Location = System::Drawing::Point(332, 24);
			this->textBox7->Name = L"textBox7";
			this->textBox7->Size = System::Drawing::Size(120, 20);
			this->textBox7->TabIndex = 39;
			// 
			// textBox8
			// 
			this->textBox8->Location = System::Drawing::Point(655, 24);
			this->textBox8->Name = L"textBox8";
			this->textBox8->Size = System::Drawing::Size(106, 20);
			this->textBox8->TabIndex = 38;
			this->textBox8->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// comboBox4
			// 
			this->comboBox4->AutoCompleteCustomSource->AddRange(gcnew cli::array< System::String^  >(7) {
				L"64", L"128", L"240", L"256",
					L"512", L"1024", L"2048"
			});
			this->comboBox4->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox4->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox4->FormattingEnabled = true;
			this->comboBox4->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"64", L"128", L"240", L"256", L"512", L"1024",
					L"2048"
			});
			this->comboBox4->Location = System::Drawing::Point(655, 107);
			this->comboBox4->Name = L"comboBox4";
			this->comboBox4->Size = System::Drawing::Size(106, 21);
			this->comboBox4->TabIndex = 37;
			// 
			// comboBox5
			// 
			this->comboBox5->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox5->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox5->FormattingEnabled = true;
			this->comboBox5->Items->AddRange(gcnew cli::array< System::Object^  >(9) {
				L"32", L"64", L"128", L"256", L"512", L"1024",
					L"2048", L"4096", L"8192"
			});
			this->comboBox5->Location = System::Drawing::Point(655, 80);
			this->comboBox5->Name = L"comboBox5";
			this->comboBox5->Size = System::Drawing::Size(106, 21);
			this->comboBox5->TabIndex = 36;
			// 
			// comboBox6
			// 
			this->comboBox6->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox6->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox6->FormattingEnabled = true;
			this->comboBox6->Items->AddRange(gcnew cli::array< System::Object^  >(8) { L"2", L"4", L"6", L"8", L"16", L"32", L"64", L"128" });
			this->comboBox6->Location = System::Drawing::Point(655, 51);
			this->comboBox6->Name = L"comboBox6";
			this->comboBox6->Size = System::Drawing::Size(106, 21);
			this->comboBox6->TabIndex = 35;
			// 
			// textBox9
			// 
			this->textBox9->Location = System::Drawing::Point(332, 140);
			this->textBox9->Name = L"textBox9";
			this->textBox9->Size = System::Drawing::Size(120, 20);
			this->textBox9->TabIndex = 30;
			this->textBox9->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// textBox10
			// 
			this->textBox10->Location = System::Drawing::Point(332, 111);
			this->textBox10->Name = L"textBox10";
			this->textBox10->Size = System::Drawing::Size(120, 20);
			this->textBox10->TabIndex = 29;
			// 
			// textBox11
			// 
			this->textBox11->Location = System::Drawing::Point(332, 84);
			this->textBox11->Name = L"textBox11";
			this->textBox11->Size = System::Drawing::Size(120, 20);
			this->textBox11->TabIndex = 28;
			// 
			// textBox12
			// 
			this->textBox12->Location = System::Drawing::Point(332, 54);
			this->textBox12->Name = L"textBox12";
			this->textBox12->Size = System::Drawing::Size(120, 20);
			this->textBox12->TabIndex = 27;
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(525, 112);
			this->label10->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(70, 13);
			this->label10->TabIndex = 25;
			this->label10->Text = L"Объём SSD:";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(218, 143);
			this->label11->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(81, 13);
			this->label11->TabIndex = 23;
			this->label11->Text = L"Мощность БП:";
			// 
			// AmountOrder1
			// 
			this->AmountOrder1->ForeColor = System::Drawing::Color::Black;
			this->AmountOrder1->Location = System::Drawing::Point(667, 137);
			this->AmountOrder1->Margin = System::Windows::Forms::Padding(2);
			this->AmountOrder1->MaxLength = 3;
			this->AmountOrder1->Name = L"AmountOrder1";
			this->AmountOrder1->Size = System::Drawing::Size(80, 20);
			this->AmountOrder1->TabIndex = 20;
			this->AmountOrder1->Text = L"1";
			this->AmountOrder1->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->AmountOrder1->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// button2
			// 
			this->button2->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button2->Location = System::Drawing::Point(751, 137);
			this->button2->Margin = System::Windows::Forms::Padding(2);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(22, 20);
			this->button2->TabIndex = 19;
			this->button2->Text = L"+";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &ChangeSpecs::button2_Click);
			// 
			// button11
			// 
			this->button11->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button11->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button11->Location = System::Drawing::Point(641, 137);
			this->button11->Margin = System::Windows::Forms::Padding(2);
			this->button11->Name = L"button11";
			this->button11->Size = System::Drawing::Size(22, 20);
			this->button11->TabIndex = 18;
			this->button11->Text = L"-";
			this->button11->UseVisualStyleBackColor = true;
			this->button11->Click += gcnew System::EventHandler(this, &ChangeSpecs::button11_Click);
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(525, 140);
			this->label12->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(69, 13);
			this->label12->TabIndex = 16;
			this->label12->Text = L"Количество:";
			// 
			// button12
			// 
			this->button12->BackColor = System::Drawing::SystemColors::MenuHighlight;
			this->button12->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button12->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button12->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button12->Location = System::Drawing::Point(363, 172);
			this->button12->Margin = System::Windows::Forms::Padding(2);
			this->button12->Name = L"button12";
			this->button12->Size = System::Drawing::Size(125, 39);
			this->button12->TabIndex = 15;
			this->button12->Text = L"Изменить";
			this->button12->UseVisualStyleBackColor = false;
			this->button12->Click += gcnew System::EventHandler(this, &ChangeSpecs::button12_Click);
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label13->Location = System::Drawing::Point(525, 27);
			this->label13->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(36, 13);
			this->label13->TabIndex = 13;
			this->label13->Text = L"Цена:";
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(525, 84);
			this->label14->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(72, 13);
			this->label14->TabIndex = 11;
			this->label14->Text = L"Объём HDD:";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(218, 115);
			this->label16->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(110, 13);
			this->label16->TabIndex = 9;
			this->label16->Text = L"Материнская плата:";
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(525, 55);
			this->label18->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(71, 13);
			this->label18->TabIndex = 7;
			this->label18->Text = L"Объём ОЗУ:";
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(218, 87);
			this->label20->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(70, 13);
			this->label20->TabIndex = 5;
			this->label20->Text = L"Видеокарта:";
			// 
			// label22
			// 
			this->label22->AutoSize = true;
			this->label22->Location = System::Drawing::Point(218, 58);
			this->label22->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label22->Name = L"label22";
			this->label22->Size = System::Drawing::Size(66, 13);
			this->label22->TabIndex = 3;
			this->label22->Text = L"Процессор:";
			// 
			// label27
			// 
			this->label27->AutoSize = true;
			this->label27->Location = System::Drawing::Point(217, 27);
			this->label27->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label27->Name = L"label27";
			this->label27->Size = System::Drawing::Size(60, 13);
			this->label27->TabIndex = 1;
			this->label27->Text = L"Название:";
			// 
			// pictureBox2
			// 
			this->pictureBox2->Cursor = System::Windows::Forms::Cursors::Hand;
			this->pictureBox2->Location = System::Drawing::Point(15, 16);
			this->pictureBox2->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(180, 195);
			this->pictureBox2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox2->TabIndex = 0;
			this->pictureBox2->TabStop = false;
			this->pictureBox2->Click += gcnew System::EventHandler(this, &ChangeSpecs::pictureBox_Click);
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->button27);
			this->groupBox3->Controls->Add(this->textBox14);
			this->groupBox3->Controls->Add(this->textBox15);
			this->groupBox3->Controls->Add(this->comboBox7);
			this->groupBox3->Controls->Add(this->comboBox8);
			this->groupBox3->Controls->Add(this->comboBox9);
			this->groupBox3->Controls->Add(this->textBox16);
			this->groupBox3->Controls->Add(this->textBox17);
			this->groupBox3->Controls->Add(this->textBox18);
			this->groupBox3->Controls->Add(this->textBox19);
			this->groupBox3->Controls->Add(this->label17);
			this->groupBox3->Controls->Add(this->label23);
			this->groupBox3->Controls->Add(this->AmountOrder2);
			this->groupBox3->Controls->Add(this->button3);
			this->groupBox3->Controls->Add(this->button13);
			this->groupBox3->Controls->Add(this->label24);
			this->groupBox3->Controls->Add(this->button14);
			this->groupBox3->Controls->Add(this->label26);
			this->groupBox3->Controls->Add(this->label28);
			this->groupBox3->Controls->Add(this->label30);
			this->groupBox3->Controls->Add(this->label31);
			this->groupBox3->Controls->Add(this->label32);
			this->groupBox3->Controls->Add(this->label34);
			this->groupBox3->Controls->Add(this->label35);
			this->groupBox3->Controls->Add(this->pictureBox3);
			this->groupBox3->Location = System::Drawing::Point(19, 505);
			this->groupBox3->Margin = System::Windows::Forms::Padding(2);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Padding = System::Windows::Forms::Padding(2);
			this->groupBox3->Size = System::Drawing::Size(782, 222);
			this->groupBox3->TabIndex = 44;
			this->groupBox3->TabStop = false;
			this->groupBox3->Visible = false;
			// 
			// button27
			// 
			this->button27->BackColor = System::Drawing::Color::Red;
			this->button27->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button27->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button27->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button27->Location = System::Drawing::Point(528, 172);
			this->button27->Margin = System::Windows::Forms::Padding(2);
			this->button27->Name = L"button27";
			this->button27->Size = System::Drawing::Size(125, 39);
			this->button27->TabIndex = 42;
			this->button27->Text = L"Удалить";
			this->button27->UseVisualStyleBackColor = false;
			this->button27->Click += gcnew System::EventHandler(this, &ChangeSpecs::button27_Click);
			// 
			// textBox14
			// 
			this->textBox14->Location = System::Drawing::Point(332, 24);
			this->textBox14->Name = L"textBox14";
			this->textBox14->Size = System::Drawing::Size(120, 20);
			this->textBox14->TabIndex = 39;
			// 
			// textBox15
			// 
			this->textBox15->Location = System::Drawing::Point(655, 24);
			this->textBox15->Name = L"textBox15";
			this->textBox15->Size = System::Drawing::Size(106, 20);
			this->textBox15->TabIndex = 38;
			this->textBox15->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// comboBox7
			// 
			this->comboBox7->AutoCompleteCustomSource->AddRange(gcnew cli::array< System::String^  >(7) {
				L"64", L"128", L"240", L"256",
					L"512", L"1024", L"2048"
			});
			this->comboBox7->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox7->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox7->FormattingEnabled = true;
			this->comboBox7->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"64", L"128", L"240", L"256", L"512", L"1024",
					L"2048"
			});
			this->comboBox7->Location = System::Drawing::Point(655, 107);
			this->comboBox7->Name = L"comboBox7";
			this->comboBox7->Size = System::Drawing::Size(106, 21);
			this->comboBox7->TabIndex = 37;
			// 
			// comboBox8
			// 
			this->comboBox8->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox8->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox8->FormattingEnabled = true;
			this->comboBox8->Items->AddRange(gcnew cli::array< System::Object^  >(9) {
				L"32", L"64", L"128", L"256", L"512", L"1024",
					L"2048", L"4096", L"8192"
			});
			this->comboBox8->Location = System::Drawing::Point(655, 80);
			this->comboBox8->Name = L"comboBox8";
			this->comboBox8->Size = System::Drawing::Size(106, 21);
			this->comboBox8->TabIndex = 36;
			// 
			// comboBox9
			// 
			this->comboBox9->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox9->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox9->FormattingEnabled = true;
			this->comboBox9->Items->AddRange(gcnew cli::array< System::Object^  >(8) { L"2", L"4", L"6", L"8", L"16", L"32", L"64", L"128" });
			this->comboBox9->Location = System::Drawing::Point(655, 51);
			this->comboBox9->Name = L"comboBox9";
			this->comboBox9->Size = System::Drawing::Size(106, 21);
			this->comboBox9->TabIndex = 35;
			// 
			// textBox16
			// 
			this->textBox16->Location = System::Drawing::Point(332, 140);
			this->textBox16->Name = L"textBox16";
			this->textBox16->Size = System::Drawing::Size(120, 20);
			this->textBox16->TabIndex = 30;
			this->textBox16->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// textBox17
			// 
			this->textBox17->Location = System::Drawing::Point(332, 111);
			this->textBox17->Name = L"textBox17";
			this->textBox17->Size = System::Drawing::Size(120, 20);
			this->textBox17->TabIndex = 29;
			// 
			// textBox18
			// 
			this->textBox18->Location = System::Drawing::Point(332, 84);
			this->textBox18->Name = L"textBox18";
			this->textBox18->Size = System::Drawing::Size(120, 20);
			this->textBox18->TabIndex = 28;
			// 
			// textBox19
			// 
			this->textBox19->Location = System::Drawing::Point(332, 54);
			this->textBox19->Name = L"textBox19";
			this->textBox19->Size = System::Drawing::Size(120, 20);
			this->textBox19->TabIndex = 27;
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(525, 112);
			this->label17->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(70, 13);
			this->label17->TabIndex = 25;
			this->label17->Text = L"Объём SSD:";
			// 
			// label23
			// 
			this->label23->AutoSize = true;
			this->label23->Location = System::Drawing::Point(218, 143);
			this->label23->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label23->Name = L"label23";
			this->label23->Size = System::Drawing::Size(81, 13);
			this->label23->TabIndex = 23;
			this->label23->Text = L"Мощность БП:";
			// 
			// AmountOrder2
			// 
			this->AmountOrder2->ForeColor = System::Drawing::Color::Black;
			this->AmountOrder2->Location = System::Drawing::Point(667, 137);
			this->AmountOrder2->Margin = System::Windows::Forms::Padding(2);
			this->AmountOrder2->MaxLength = 3;
			this->AmountOrder2->Name = L"AmountOrder2";
			this->AmountOrder2->Size = System::Drawing::Size(80, 20);
			this->AmountOrder2->TabIndex = 20;
			this->AmountOrder2->Text = L"1";
			this->AmountOrder2->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->AmountOrder2->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// button3
			// 
			this->button3->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button3->Location = System::Drawing::Point(751, 137);
			this->button3->Margin = System::Windows::Forms::Padding(2);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(22, 20);
			this->button3->TabIndex = 19;
			this->button3->Text = L"+";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &ChangeSpecs::button3_Click);
			// 
			// button13
			// 
			this->button13->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button13->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button13->Location = System::Drawing::Point(641, 137);
			this->button13->Margin = System::Windows::Forms::Padding(2);
			this->button13->Name = L"button13";
			this->button13->Size = System::Drawing::Size(22, 20);
			this->button13->TabIndex = 18;
			this->button13->Text = L"-";
			this->button13->UseVisualStyleBackColor = true;
			this->button13->Click += gcnew System::EventHandler(this, &ChangeSpecs::button13_Click);
			// 
			// label24
			// 
			this->label24->AutoSize = true;
			this->label24->Location = System::Drawing::Point(525, 140);
			this->label24->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label24->Name = L"label24";
			this->label24->Size = System::Drawing::Size(69, 13);
			this->label24->TabIndex = 16;
			this->label24->Text = L"Количество:";
			// 
			// button14
			// 
			this->button14->BackColor = System::Drawing::SystemColors::MenuHighlight;
			this->button14->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button14->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button14->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button14->Location = System::Drawing::Point(363, 172);
			this->button14->Margin = System::Windows::Forms::Padding(2);
			this->button14->Name = L"button14";
			this->button14->Size = System::Drawing::Size(125, 39);
			this->button14->TabIndex = 15;
			this->button14->Text = L"Изменить";
			this->button14->UseVisualStyleBackColor = false;
			this->button14->Click += gcnew System::EventHandler(this, &ChangeSpecs::button14_Click);
			// 
			// label26
			// 
			this->label26->AutoSize = true;
			this->label26->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label26->Location = System::Drawing::Point(525, 27);
			this->label26->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label26->Name = L"label26";
			this->label26->Size = System::Drawing::Size(36, 13);
			this->label26->TabIndex = 13;
			this->label26->Text = L"Цена:";
			// 
			// label28
			// 
			this->label28->AutoSize = true;
			this->label28->Location = System::Drawing::Point(525, 84);
			this->label28->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label28->Name = L"label28";
			this->label28->Size = System::Drawing::Size(72, 13);
			this->label28->TabIndex = 11;
			this->label28->Text = L"Объём HDD:";
			// 
			// label30
			// 
			this->label30->AutoSize = true;
			this->label30->Location = System::Drawing::Point(218, 115);
			this->label30->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label30->Name = L"label30";
			this->label30->Size = System::Drawing::Size(110, 13);
			this->label30->TabIndex = 9;
			this->label30->Text = L"Материнская плата:";
			// 
			// label31
			// 
			this->label31->AutoSize = true;
			this->label31->Location = System::Drawing::Point(525, 55);
			this->label31->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label31->Name = L"label31";
			this->label31->Size = System::Drawing::Size(71, 13);
			this->label31->TabIndex = 7;
			this->label31->Text = L"Объём ОЗУ:";
			// 
			// label32
			// 
			this->label32->AutoSize = true;
			this->label32->Location = System::Drawing::Point(218, 87);
			this->label32->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label32->Name = L"label32";
			this->label32->Size = System::Drawing::Size(70, 13);
			this->label32->TabIndex = 5;
			this->label32->Text = L"Видеокарта:";
			// 
			// label34
			// 
			this->label34->AutoSize = true;
			this->label34->Location = System::Drawing::Point(218, 58);
			this->label34->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label34->Name = L"label34";
			this->label34->Size = System::Drawing::Size(66, 13);
			this->label34->TabIndex = 3;
			this->label34->Text = L"Процессор:";
			// 
			// label35
			// 
			this->label35->AutoSize = true;
			this->label35->Location = System::Drawing::Point(217, 27);
			this->label35->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label35->Name = L"label35";
			this->label35->Size = System::Drawing::Size(60, 13);
			this->label35->TabIndex = 1;
			this->label35->Text = L"Название:";
			// 
			// pictureBox3
			// 
			this->pictureBox3->Cursor = System::Windows::Forms::Cursors::Hand;
			this->pictureBox3->Location = System::Drawing::Point(15, 16);
			this->pictureBox3->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox3->Name = L"pictureBox3";
			this->pictureBox3->Size = System::Drawing::Size(180, 195);
			this->pictureBox3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox3->TabIndex = 0;
			this->pictureBox3->TabStop = false;
			this->pictureBox3->Click += gcnew System::EventHandler(this, &ChangeSpecs::pictureBox_Click);
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->button28);
			this->groupBox4->Controls->Add(this->textBox21);
			this->groupBox4->Controls->Add(this->textBox22);
			this->groupBox4->Controls->Add(this->comboBox10);
			this->groupBox4->Controls->Add(this->comboBox11);
			this->groupBox4->Controls->Add(this->comboBox12);
			this->groupBox4->Controls->Add(this->textBox23);
			this->groupBox4->Controls->Add(this->textBox24);
			this->groupBox4->Controls->Add(this->textBox25);
			this->groupBox4->Controls->Add(this->textBox26);
			this->groupBox4->Controls->Add(this->label21);
			this->groupBox4->Controls->Add(this->label36);
			this->groupBox4->Controls->Add(this->AmountOrder3);
			this->groupBox4->Controls->Add(this->button4);
			this->groupBox4->Controls->Add(this->button15);
			this->groupBox4->Controls->Add(this->label38);
			this->groupBox4->Controls->Add(this->button16);
			this->groupBox4->Controls->Add(this->label39);
			this->groupBox4->Controls->Add(this->label40);
			this->groupBox4->Controls->Add(this->label41);
			this->groupBox4->Controls->Add(this->label42);
			this->groupBox4->Controls->Add(this->label43);
			this->groupBox4->Controls->Add(this->label44);
			this->groupBox4->Controls->Add(this->label45);
			this->groupBox4->Controls->Add(this->pictureBox4);
			this->groupBox4->Location = System::Drawing::Point(19, 733);
			this->groupBox4->Margin = System::Windows::Forms::Padding(2);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Padding = System::Windows::Forms::Padding(2);
			this->groupBox4->Size = System::Drawing::Size(782, 222);
			this->groupBox4->TabIndex = 45;
			this->groupBox4->TabStop = false;
			this->groupBox4->Visible = false;
			// 
			// button28
			// 
			this->button28->BackColor = System::Drawing::Color::Red;
			this->button28->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button28->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button28->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button28->Location = System::Drawing::Point(528, 172);
			this->button28->Margin = System::Windows::Forms::Padding(2);
			this->button28->Name = L"button28";
			this->button28->Size = System::Drawing::Size(125, 39);
			this->button28->TabIndex = 43;
			this->button28->Text = L"Удалить";
			this->button28->UseVisualStyleBackColor = false;
			this->button28->Click += gcnew System::EventHandler(this, &ChangeSpecs::button28_Click);
			// 
			// textBox21
			// 
			this->textBox21->Location = System::Drawing::Point(332, 24);
			this->textBox21->Name = L"textBox21";
			this->textBox21->Size = System::Drawing::Size(120, 20);
			this->textBox21->TabIndex = 39;
			// 
			// textBox22
			// 
			this->textBox22->Location = System::Drawing::Point(655, 24);
			this->textBox22->Name = L"textBox22";
			this->textBox22->Size = System::Drawing::Size(106, 20);
			this->textBox22->TabIndex = 38;
			this->textBox22->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// comboBox10
			// 
			this->comboBox10->AutoCompleteCustomSource->AddRange(gcnew cli::array< System::String^  >(7) {
				L"64", L"128", L"240", L"256",
					L"512", L"1024", L"2048"
			});
			this->comboBox10->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox10->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox10->FormattingEnabled = true;
			this->comboBox10->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"64", L"128", L"240", L"256", L"512", L"1024",
					L"2048"
			});
			this->comboBox10->Location = System::Drawing::Point(655, 107);
			this->comboBox10->Name = L"comboBox10";
			this->comboBox10->Size = System::Drawing::Size(106, 21);
			this->comboBox10->TabIndex = 37;
			// 
			// comboBox11
			// 
			this->comboBox11->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox11->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox11->FormattingEnabled = true;
			this->comboBox11->Items->AddRange(gcnew cli::array< System::Object^  >(9) {
				L"32", L"64", L"128", L"256", L"512", L"1024",
					L"2048", L"4096", L"8192"
			});
			this->comboBox11->Location = System::Drawing::Point(655, 80);
			this->comboBox11->Name = L"comboBox11";
			this->comboBox11->Size = System::Drawing::Size(106, 21);
			this->comboBox11->TabIndex = 36;
			// 
			// comboBox12
			// 
			this->comboBox12->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox12->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox12->FormattingEnabled = true;
			this->comboBox12->Items->AddRange(gcnew cli::array< System::Object^  >(8) { L"2", L"4", L"6", L"8", L"16", L"32", L"64", L"128" });
			this->comboBox12->Location = System::Drawing::Point(655, 51);
			this->comboBox12->Name = L"comboBox12";
			this->comboBox12->Size = System::Drawing::Size(106, 21);
			this->comboBox12->TabIndex = 35;
			// 
			// textBox23
			// 
			this->textBox23->Location = System::Drawing::Point(332, 140);
			this->textBox23->Name = L"textBox23";
			this->textBox23->Size = System::Drawing::Size(120, 20);
			this->textBox23->TabIndex = 30;
			this->textBox23->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// textBox24
			// 
			this->textBox24->Location = System::Drawing::Point(332, 111);
			this->textBox24->Name = L"textBox24";
			this->textBox24->Size = System::Drawing::Size(120, 20);
			this->textBox24->TabIndex = 29;
			// 
			// textBox25
			// 
			this->textBox25->Location = System::Drawing::Point(332, 84);
			this->textBox25->Name = L"textBox25";
			this->textBox25->Size = System::Drawing::Size(120, 20);
			this->textBox25->TabIndex = 28;
			// 
			// textBox26
			// 
			this->textBox26->Location = System::Drawing::Point(332, 54);
			this->textBox26->Name = L"textBox26";
			this->textBox26->Size = System::Drawing::Size(120, 20);
			this->textBox26->TabIndex = 27;
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Location = System::Drawing::Point(525, 112);
			this->label21->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(70, 13);
			this->label21->TabIndex = 25;
			this->label21->Text = L"Объём SSD:";
			// 
			// label36
			// 
			this->label36->AutoSize = true;
			this->label36->Location = System::Drawing::Point(218, 143);
			this->label36->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label36->Name = L"label36";
			this->label36->Size = System::Drawing::Size(81, 13);
			this->label36->TabIndex = 23;
			this->label36->Text = L"Мощность БП:";
			// 
			// AmountOrder3
			// 
			this->AmountOrder3->ForeColor = System::Drawing::Color::Black;
			this->AmountOrder3->Location = System::Drawing::Point(667, 137);
			this->AmountOrder3->Margin = System::Windows::Forms::Padding(2);
			this->AmountOrder3->MaxLength = 3;
			this->AmountOrder3->Name = L"AmountOrder3";
			this->AmountOrder3->Size = System::Drawing::Size(80, 20);
			this->AmountOrder3->TabIndex = 20;
			this->AmountOrder3->Text = L"1";
			this->AmountOrder3->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->AmountOrder3->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// button4
			// 
			this->button4->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button4->Location = System::Drawing::Point(751, 137);
			this->button4->Margin = System::Windows::Forms::Padding(2);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(22, 20);
			this->button4->TabIndex = 19;
			this->button4->Text = L"+";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &ChangeSpecs::button4_Click);
			// 
			// button15
			// 
			this->button15->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button15->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button15->Location = System::Drawing::Point(641, 137);
			this->button15->Margin = System::Windows::Forms::Padding(2);
			this->button15->Name = L"button15";
			this->button15->Size = System::Drawing::Size(22, 20);
			this->button15->TabIndex = 18;
			this->button15->Text = L"-";
			this->button15->UseVisualStyleBackColor = true;
			this->button15->Click += gcnew System::EventHandler(this, &ChangeSpecs::button15_Click);
			// 
			// label38
			// 
			this->label38->AutoSize = true;
			this->label38->Location = System::Drawing::Point(525, 140);
			this->label38->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label38->Name = L"label38";
			this->label38->Size = System::Drawing::Size(69, 13);
			this->label38->TabIndex = 16;
			this->label38->Text = L"Количество:";
			// 
			// button16
			// 
			this->button16->BackColor = System::Drawing::SystemColors::MenuHighlight;
			this->button16->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button16->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button16->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button16->Location = System::Drawing::Point(363, 172);
			this->button16->Margin = System::Windows::Forms::Padding(2);
			this->button16->Name = L"button16";
			this->button16->Size = System::Drawing::Size(125, 39);
			this->button16->TabIndex = 15;
			this->button16->Text = L"Изменить";
			this->button16->UseVisualStyleBackColor = false;
			this->button16->Click += gcnew System::EventHandler(this, &ChangeSpecs::button16_Click);
			// 
			// label39
			// 
			this->label39->AutoSize = true;
			this->label39->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label39->Location = System::Drawing::Point(525, 27);
			this->label39->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label39->Name = L"label39";
			this->label39->Size = System::Drawing::Size(36, 13);
			this->label39->TabIndex = 13;
			this->label39->Text = L"Цена:";
			// 
			// label40
			// 
			this->label40->AutoSize = true;
			this->label40->Location = System::Drawing::Point(525, 84);
			this->label40->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label40->Name = L"label40";
			this->label40->Size = System::Drawing::Size(72, 13);
			this->label40->TabIndex = 11;
			this->label40->Text = L"Объём HDD:";
			// 
			// label41
			// 
			this->label41->AutoSize = true;
			this->label41->Location = System::Drawing::Point(218, 115);
			this->label41->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label41->Name = L"label41";
			this->label41->Size = System::Drawing::Size(110, 13);
			this->label41->TabIndex = 9;
			this->label41->Text = L"Материнская плата:";
			// 
			// label42
			// 
			this->label42->AutoSize = true;
			this->label42->Location = System::Drawing::Point(525, 55);
			this->label42->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label42->Name = L"label42";
			this->label42->Size = System::Drawing::Size(71, 13);
			this->label42->TabIndex = 7;
			this->label42->Text = L"Объём ОЗУ:";
			// 
			// label43
			// 
			this->label43->AutoSize = true;
			this->label43->Location = System::Drawing::Point(218, 87);
			this->label43->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label43->Name = L"label43";
			this->label43->Size = System::Drawing::Size(70, 13);
			this->label43->TabIndex = 5;
			this->label43->Text = L"Видеокарта:";
			// 
			// label44
			// 
			this->label44->AutoSize = true;
			this->label44->Location = System::Drawing::Point(218, 58);
			this->label44->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label44->Name = L"label44";
			this->label44->Size = System::Drawing::Size(66, 13);
			this->label44->TabIndex = 3;
			this->label44->Text = L"Процессор:";
			// 
			// label45
			// 
			this->label45->AutoSize = true;
			this->label45->Location = System::Drawing::Point(217, 27);
			this->label45->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label45->Name = L"label45";
			this->label45->Size = System::Drawing::Size(60, 13);
			this->label45->TabIndex = 1;
			this->label45->Text = L"Название:";
			// 
			// pictureBox4
			// 
			this->pictureBox4->Cursor = System::Windows::Forms::Cursors::Hand;
			this->pictureBox4->Location = System::Drawing::Point(15, 16);
			this->pictureBox4->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox4->Name = L"pictureBox4";
			this->pictureBox4->Size = System::Drawing::Size(180, 195);
			this->pictureBox4->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox4->TabIndex = 0;
			this->pictureBox4->TabStop = false;
			this->pictureBox4->Click += gcnew System::EventHandler(this, &ChangeSpecs::pictureBox_Click);
			// 
			// groupBox5
			// 
			this->groupBox5->Controls->Add(this->button29);
			this->groupBox5->Controls->Add(this->textBox28);
			this->groupBox5->Controls->Add(this->textBox29);
			this->groupBox5->Controls->Add(this->comboBox13);
			this->groupBox5->Controls->Add(this->comboBox14);
			this->groupBox5->Controls->Add(this->comboBox15);
			this->groupBox5->Controls->Add(this->textBox30);
			this->groupBox5->Controls->Add(this->textBox31);
			this->groupBox5->Controls->Add(this->textBox32);
			this->groupBox5->Controls->Add(this->textBox33);
			this->groupBox5->Controls->Add(this->label25);
			this->groupBox5->Controls->Add(this->label29);
			this->groupBox5->Controls->Add(this->AmountOrder4);
			this->groupBox5->Controls->Add(this->button5);
			this->groupBox5->Controls->Add(this->button6);
			this->groupBox5->Controls->Add(this->label46);
			this->groupBox5->Controls->Add(this->button17);
			this->groupBox5->Controls->Add(this->label47);
			this->groupBox5->Controls->Add(this->label48);
			this->groupBox5->Controls->Add(this->label49);
			this->groupBox5->Controls->Add(this->label50);
			this->groupBox5->Controls->Add(this->label51);
			this->groupBox5->Controls->Add(this->label52);
			this->groupBox5->Controls->Add(this->label53);
			this->groupBox5->Controls->Add(this->pictureBox5);
			this->groupBox5->Location = System::Drawing::Point(19, 959);
			this->groupBox5->Margin = System::Windows::Forms::Padding(2);
			this->groupBox5->Name = L"groupBox5";
			this->groupBox5->Padding = System::Windows::Forms::Padding(2);
			this->groupBox5->Size = System::Drawing::Size(782, 222);
			this->groupBox5->TabIndex = 46;
			this->groupBox5->TabStop = false;
			this->groupBox5->Visible = false;
			// 
			// button29
			// 
			this->button29->BackColor = System::Drawing::Color::Red;
			this->button29->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button29->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button29->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button29->Location = System::Drawing::Point(528, 172);
			this->button29->Margin = System::Windows::Forms::Padding(2);
			this->button29->Name = L"button29";
			this->button29->Size = System::Drawing::Size(125, 39);
			this->button29->TabIndex = 44;
			this->button29->Text = L"Удалить";
			this->button29->UseVisualStyleBackColor = false;
			this->button29->Click += gcnew System::EventHandler(this, &ChangeSpecs::button29_Click);
			// 
			// textBox28
			// 
			this->textBox28->Location = System::Drawing::Point(332, 24);
			this->textBox28->Name = L"textBox28";
			this->textBox28->Size = System::Drawing::Size(120, 20);
			this->textBox28->TabIndex = 39;
			// 
			// textBox29
			// 
			this->textBox29->Location = System::Drawing::Point(655, 24);
			this->textBox29->Name = L"textBox29";
			this->textBox29->Size = System::Drawing::Size(106, 20);
			this->textBox29->TabIndex = 38;
			this->textBox29->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// comboBox13
			// 
			this->comboBox13->AutoCompleteCustomSource->AddRange(gcnew cli::array< System::String^  >(7) {
				L"64", L"128", L"240", L"256",
					L"512", L"1024", L"2048"
			});
			this->comboBox13->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox13->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox13->FormattingEnabled = true;
			this->comboBox13->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"64", L"128", L"240", L"256", L"512", L"1024",
					L"2048"
			});
			this->comboBox13->Location = System::Drawing::Point(655, 107);
			this->comboBox13->Name = L"comboBox13";
			this->comboBox13->Size = System::Drawing::Size(106, 21);
			this->comboBox13->TabIndex = 37;
			// 
			// comboBox14
			// 
			this->comboBox14->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox14->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox14->FormattingEnabled = true;
			this->comboBox14->Items->AddRange(gcnew cli::array< System::Object^  >(9) {
				L"32", L"64", L"128", L"256", L"512", L"1024",
					L"2048", L"4096", L"8192"
			});
			this->comboBox14->Location = System::Drawing::Point(655, 80);
			this->comboBox14->Name = L"comboBox14";
			this->comboBox14->Size = System::Drawing::Size(106, 21);
			this->comboBox14->TabIndex = 36;
			// 
			// comboBox15
			// 
			this->comboBox15->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox15->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox15->FormattingEnabled = true;
			this->comboBox15->Items->AddRange(gcnew cli::array< System::Object^  >(8) { L"2", L"4", L"6", L"8", L"16", L"32", L"64", L"128" });
			this->comboBox15->Location = System::Drawing::Point(655, 51);
			this->comboBox15->Name = L"comboBox15";
			this->comboBox15->Size = System::Drawing::Size(106, 21);
			this->comboBox15->TabIndex = 35;
			// 
			// textBox30
			// 
			this->textBox30->Location = System::Drawing::Point(332, 140);
			this->textBox30->Name = L"textBox30";
			this->textBox30->Size = System::Drawing::Size(120, 20);
			this->textBox30->TabIndex = 30;
			this->textBox30->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// textBox31
			// 
			this->textBox31->Location = System::Drawing::Point(332, 111);
			this->textBox31->Name = L"textBox31";
			this->textBox31->Size = System::Drawing::Size(120, 20);
			this->textBox31->TabIndex = 29;
			// 
			// textBox32
			// 
			this->textBox32->Location = System::Drawing::Point(332, 84);
			this->textBox32->Name = L"textBox32";
			this->textBox32->Size = System::Drawing::Size(120, 20);
			this->textBox32->TabIndex = 28;
			// 
			// textBox33
			// 
			this->textBox33->Location = System::Drawing::Point(332, 54);
			this->textBox33->Name = L"textBox33";
			this->textBox33->Size = System::Drawing::Size(120, 20);
			this->textBox33->TabIndex = 27;
			// 
			// label25
			// 
			this->label25->AutoSize = true;
			this->label25->Location = System::Drawing::Point(525, 112);
			this->label25->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label25->Name = L"label25";
			this->label25->Size = System::Drawing::Size(70, 13);
			this->label25->TabIndex = 25;
			this->label25->Text = L"Объём SSD:";
			// 
			// label29
			// 
			this->label29->AutoSize = true;
			this->label29->Location = System::Drawing::Point(218, 143);
			this->label29->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label29->Name = L"label29";
			this->label29->Size = System::Drawing::Size(81, 13);
			this->label29->TabIndex = 23;
			this->label29->Text = L"Мощность БП:";
			// 
			// AmountOrder4
			// 
			this->AmountOrder4->ForeColor = System::Drawing::Color::Black;
			this->AmountOrder4->Location = System::Drawing::Point(667, 137);
			this->AmountOrder4->Margin = System::Windows::Forms::Padding(2);
			this->AmountOrder4->MaxLength = 3;
			this->AmountOrder4->Name = L"AmountOrder4";
			this->AmountOrder4->Size = System::Drawing::Size(80, 20);
			this->AmountOrder4->TabIndex = 20;
			this->AmountOrder4->Text = L"1";
			this->AmountOrder4->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->AmountOrder4->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// button5
			// 
			this->button5->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button5->Location = System::Drawing::Point(751, 137);
			this->button5->Margin = System::Windows::Forms::Padding(2);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(22, 20);
			this->button5->TabIndex = 19;
			this->button5->Text = L"+";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &ChangeSpecs::button5_Click);
			// 
			// button6
			// 
			this->button6->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button6->Location = System::Drawing::Point(641, 137);
			this->button6->Margin = System::Windows::Forms::Padding(2);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(22, 20);
			this->button6->TabIndex = 18;
			this->button6->Text = L"-";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Click += gcnew System::EventHandler(this, &ChangeSpecs::button6_Click);
			// 
			// label46
			// 
			this->label46->AutoSize = true;
			this->label46->Location = System::Drawing::Point(525, 140);
			this->label46->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label46->Name = L"label46";
			this->label46->Size = System::Drawing::Size(69, 13);
			this->label46->TabIndex = 16;
			this->label46->Text = L"Количество:";
			// 
			// button17
			// 
			this->button17->BackColor = System::Drawing::SystemColors::MenuHighlight;
			this->button17->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button17->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button17->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button17->Location = System::Drawing::Point(363, 172);
			this->button17->Margin = System::Windows::Forms::Padding(2);
			this->button17->Name = L"button17";
			this->button17->Size = System::Drawing::Size(125, 39);
			this->button17->TabIndex = 15;
			this->button17->Text = L"Изменить";
			this->button17->UseVisualStyleBackColor = false;
			this->button17->Click += gcnew System::EventHandler(this, &ChangeSpecs::button17_Click);
			// 
			// label47
			// 
			this->label47->AutoSize = true;
			this->label47->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label47->Location = System::Drawing::Point(525, 27);
			this->label47->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label47->Name = L"label47";
			this->label47->Size = System::Drawing::Size(36, 13);
			this->label47->TabIndex = 13;
			this->label47->Text = L"Цена:";
			// 
			// label48
			// 
			this->label48->AutoSize = true;
			this->label48->Location = System::Drawing::Point(525, 84);
			this->label48->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label48->Name = L"label48";
			this->label48->Size = System::Drawing::Size(72, 13);
			this->label48->TabIndex = 11;
			this->label48->Text = L"Объём HDD:";
			// 
			// label49
			// 
			this->label49->AutoSize = true;
			this->label49->Location = System::Drawing::Point(218, 115);
			this->label49->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label49->Name = L"label49";
			this->label49->Size = System::Drawing::Size(110, 13);
			this->label49->TabIndex = 9;
			this->label49->Text = L"Материнская плата:";
			// 
			// label50
			// 
			this->label50->AutoSize = true;
			this->label50->Location = System::Drawing::Point(525, 55);
			this->label50->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label50->Name = L"label50";
			this->label50->Size = System::Drawing::Size(71, 13);
			this->label50->TabIndex = 7;
			this->label50->Text = L"Объём ОЗУ:";
			// 
			// label51
			// 
			this->label51->AutoSize = true;
			this->label51->Location = System::Drawing::Point(218, 87);
			this->label51->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label51->Name = L"label51";
			this->label51->Size = System::Drawing::Size(70, 13);
			this->label51->TabIndex = 5;
			this->label51->Text = L"Видеокарта:";
			// 
			// label52
			// 
			this->label52->AutoSize = true;
			this->label52->Location = System::Drawing::Point(218, 58);
			this->label52->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label52->Name = L"label52";
			this->label52->Size = System::Drawing::Size(66, 13);
			this->label52->TabIndex = 3;
			this->label52->Text = L"Процессор:";
			// 
			// label53
			// 
			this->label53->AutoSize = true;
			this->label53->Location = System::Drawing::Point(217, 27);
			this->label53->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label53->Name = L"label53";
			this->label53->Size = System::Drawing::Size(60, 13);
			this->label53->TabIndex = 1;
			this->label53->Text = L"Название:";
			// 
			// pictureBox5
			// 
			this->pictureBox5->Cursor = System::Windows::Forms::Cursors::Hand;
			this->pictureBox5->Location = System::Drawing::Point(15, 16);
			this->pictureBox5->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox5->Name = L"pictureBox5";
			this->pictureBox5->Size = System::Drawing::Size(180, 195);
			this->pictureBox5->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox5->TabIndex = 0;
			this->pictureBox5->TabStop = false;
			this->pictureBox5->Click += gcnew System::EventHandler(this, &ChangeSpecs::pictureBox_Click);
			// 
			// groupBox6
			// 
			this->groupBox6->Controls->Add(this->button30);
			this->groupBox6->Controls->Add(this->textBox35);
			this->groupBox6->Controls->Add(this->textBox36);
			this->groupBox6->Controls->Add(this->comboBox16);
			this->groupBox6->Controls->Add(this->comboBox17);
			this->groupBox6->Controls->Add(this->comboBox18);
			this->groupBox6->Controls->Add(this->textBox37);
			this->groupBox6->Controls->Add(this->textBox38);
			this->groupBox6->Controls->Add(this->textBox39);
			this->groupBox6->Controls->Add(this->textBox40);
			this->groupBox6->Controls->Add(this->label54);
			this->groupBox6->Controls->Add(this->label55);
			this->groupBox6->Controls->Add(this->AmountOrder5);
			this->groupBox6->Controls->Add(this->button18);
			this->groupBox6->Controls->Add(this->button19);
			this->groupBox6->Controls->Add(this->label56);
			this->groupBox6->Controls->Add(this->button20);
			this->groupBox6->Controls->Add(this->label57);
			this->groupBox6->Controls->Add(this->label58);
			this->groupBox6->Controls->Add(this->label59);
			this->groupBox6->Controls->Add(this->label60);
			this->groupBox6->Controls->Add(this->label61);
			this->groupBox6->Controls->Add(this->label62);
			this->groupBox6->Controls->Add(this->label63);
			this->groupBox6->Controls->Add(this->pictureBox6);
			this->groupBox6->Location = System::Drawing::Point(19, 1188);
			this->groupBox6->Margin = System::Windows::Forms::Padding(2);
			this->groupBox6->Name = L"groupBox6";
			this->groupBox6->Padding = System::Windows::Forms::Padding(2);
			this->groupBox6->Size = System::Drawing::Size(782, 222);
			this->groupBox6->TabIndex = 47;
			this->groupBox6->TabStop = false;
			this->groupBox6->Visible = false;
			// 
			// button30
			// 
			this->button30->BackColor = System::Drawing::Color::Red;
			this->button30->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button30->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button30->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button30->Location = System::Drawing::Point(528, 172);
			this->button30->Margin = System::Windows::Forms::Padding(2);
			this->button30->Name = L"button30";
			this->button30->Size = System::Drawing::Size(125, 39);
			this->button30->TabIndex = 45;
			this->button30->Text = L"Удалить";
			this->button30->UseVisualStyleBackColor = false;
			this->button30->Click += gcnew System::EventHandler(this, &ChangeSpecs::button30_Click);
			// 
			// textBox35
			// 
			this->textBox35->Location = System::Drawing::Point(332, 24);
			this->textBox35->Name = L"textBox35";
			this->textBox35->Size = System::Drawing::Size(120, 20);
			this->textBox35->TabIndex = 39;
			// 
			// textBox36
			// 
			this->textBox36->Location = System::Drawing::Point(655, 24);
			this->textBox36->Name = L"textBox36";
			this->textBox36->Size = System::Drawing::Size(106, 20);
			this->textBox36->TabIndex = 38;
			this->textBox36->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// comboBox16
			// 
			this->comboBox16->AutoCompleteCustomSource->AddRange(gcnew cli::array< System::String^  >(7) {
				L"64", L"128", L"240", L"256",
					L"512", L"1024", L"2048"
			});
			this->comboBox16->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox16->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox16->FormattingEnabled = true;
			this->comboBox16->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"64", L"128", L"240", L"256", L"512", L"1024",
					L"2048"
			});
			this->comboBox16->Location = System::Drawing::Point(655, 107);
			this->comboBox16->Name = L"comboBox16";
			this->comboBox16->Size = System::Drawing::Size(106, 21);
			this->comboBox16->TabIndex = 37;
			// 
			// comboBox17
			// 
			this->comboBox17->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox17->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox17->FormattingEnabled = true;
			this->comboBox17->Items->AddRange(gcnew cli::array< System::Object^  >(9) {
				L"32", L"64", L"128", L"256", L"512", L"1024",
					L"2048", L"4096", L"8192"
			});
			this->comboBox17->Location = System::Drawing::Point(655, 80);
			this->comboBox17->Name = L"comboBox17";
			this->comboBox17->Size = System::Drawing::Size(106, 21);
			this->comboBox17->TabIndex = 36;
			// 
			// comboBox18
			// 
			this->comboBox18->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox18->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox18->FormattingEnabled = true;
			this->comboBox18->Items->AddRange(gcnew cli::array< System::Object^  >(8) { L"2", L"4", L"6", L"8", L"16", L"32", L"64", L"128" });
			this->comboBox18->Location = System::Drawing::Point(655, 51);
			this->comboBox18->Name = L"comboBox18";
			this->comboBox18->Size = System::Drawing::Size(106, 21);
			this->comboBox18->TabIndex = 35;
			// 
			// textBox37
			// 
			this->textBox37->Location = System::Drawing::Point(332, 140);
			this->textBox37->Name = L"textBox37";
			this->textBox37->Size = System::Drawing::Size(120, 20);
			this->textBox37->TabIndex = 30;
			this->textBox37->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// textBox38
			// 
			this->textBox38->Location = System::Drawing::Point(332, 111);
			this->textBox38->Name = L"textBox38";
			this->textBox38->Size = System::Drawing::Size(120, 20);
			this->textBox38->TabIndex = 29;
			// 
			// textBox39
			// 
			this->textBox39->Location = System::Drawing::Point(332, 84);
			this->textBox39->Name = L"textBox39";
			this->textBox39->Size = System::Drawing::Size(120, 20);
			this->textBox39->TabIndex = 28;
			// 
			// textBox40
			// 
			this->textBox40->Location = System::Drawing::Point(332, 54);
			this->textBox40->Name = L"textBox40";
			this->textBox40->Size = System::Drawing::Size(120, 20);
			this->textBox40->TabIndex = 27;
			// 
			// label54
			// 
			this->label54->AutoSize = true;
			this->label54->Location = System::Drawing::Point(525, 112);
			this->label54->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label54->Name = L"label54";
			this->label54->Size = System::Drawing::Size(70, 13);
			this->label54->TabIndex = 25;
			this->label54->Text = L"Объём SSD:";
			// 
			// label55
			// 
			this->label55->AutoSize = true;
			this->label55->Location = System::Drawing::Point(218, 143);
			this->label55->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label55->Name = L"label55";
			this->label55->Size = System::Drawing::Size(81, 13);
			this->label55->TabIndex = 23;
			this->label55->Text = L"Мощность БП:";
			// 
			// AmountOrder5
			// 
			this->AmountOrder5->ForeColor = System::Drawing::Color::Black;
			this->AmountOrder5->Location = System::Drawing::Point(667, 137);
			this->AmountOrder5->Margin = System::Windows::Forms::Padding(2);
			this->AmountOrder5->MaxLength = 3;
			this->AmountOrder5->Name = L"AmountOrder5";
			this->AmountOrder5->Size = System::Drawing::Size(80, 20);
			this->AmountOrder5->TabIndex = 20;
			this->AmountOrder5->Text = L"1";
			this->AmountOrder5->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->AmountOrder5->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// button18
			// 
			this->button18->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button18->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button18->Location = System::Drawing::Point(751, 137);
			this->button18->Margin = System::Windows::Forms::Padding(2);
			this->button18->Name = L"button18";
			this->button18->Size = System::Drawing::Size(22, 20);
			this->button18->TabIndex = 19;
			this->button18->Text = L"+";
			this->button18->UseVisualStyleBackColor = true;
			this->button18->Click += gcnew System::EventHandler(this, &ChangeSpecs::button18_Click);
			// 
			// button19
			// 
			this->button19->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button19->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button19->Location = System::Drawing::Point(641, 137);
			this->button19->Margin = System::Windows::Forms::Padding(2);
			this->button19->Name = L"button19";
			this->button19->Size = System::Drawing::Size(22, 20);
			this->button19->TabIndex = 18;
			this->button19->Text = L"-";
			this->button19->UseVisualStyleBackColor = true;
			this->button19->Click += gcnew System::EventHandler(this, &ChangeSpecs::button19_Click);
			// 
			// label56
			// 
			this->label56->AutoSize = true;
			this->label56->Location = System::Drawing::Point(525, 140);
			this->label56->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label56->Name = L"label56";
			this->label56->Size = System::Drawing::Size(69, 13);
			this->label56->TabIndex = 16;
			this->label56->Text = L"Количество:";
			// 
			// button20
			// 
			this->button20->BackColor = System::Drawing::SystemColors::MenuHighlight;
			this->button20->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button20->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button20->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button20->Location = System::Drawing::Point(363, 172);
			this->button20->Margin = System::Windows::Forms::Padding(2);
			this->button20->Name = L"button20";
			this->button20->Size = System::Drawing::Size(125, 39);
			this->button20->TabIndex = 15;
			this->button20->Text = L"Изменить";
			this->button20->UseVisualStyleBackColor = false;
			this->button20->Click += gcnew System::EventHandler(this, &ChangeSpecs::button20_Click);
			// 
			// label57
			// 
			this->label57->AutoSize = true;
			this->label57->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label57->Location = System::Drawing::Point(525, 27);
			this->label57->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label57->Name = L"label57";
			this->label57->Size = System::Drawing::Size(36, 13);
			this->label57->TabIndex = 13;
			this->label57->Text = L"Цена:";
			// 
			// label58
			// 
			this->label58->AutoSize = true;
			this->label58->Location = System::Drawing::Point(525, 84);
			this->label58->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label58->Name = L"label58";
			this->label58->Size = System::Drawing::Size(72, 13);
			this->label58->TabIndex = 11;
			this->label58->Text = L"Объём HDD:";
			// 
			// label59
			// 
			this->label59->AutoSize = true;
			this->label59->Location = System::Drawing::Point(218, 115);
			this->label59->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label59->Name = L"label59";
			this->label59->Size = System::Drawing::Size(110, 13);
			this->label59->TabIndex = 9;
			this->label59->Text = L"Материнская плата:";
			// 
			// label60
			// 
			this->label60->AutoSize = true;
			this->label60->Location = System::Drawing::Point(525, 55);
			this->label60->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label60->Name = L"label60";
			this->label60->Size = System::Drawing::Size(71, 13);
			this->label60->TabIndex = 7;
			this->label60->Text = L"Объём ОЗУ:";
			// 
			// label61
			// 
			this->label61->AutoSize = true;
			this->label61->Location = System::Drawing::Point(218, 87);
			this->label61->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label61->Name = L"label61";
			this->label61->Size = System::Drawing::Size(70, 13);
			this->label61->TabIndex = 5;
			this->label61->Text = L"Видеокарта:";
			// 
			// label62
			// 
			this->label62->AutoSize = true;
			this->label62->Location = System::Drawing::Point(218, 58);
			this->label62->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label62->Name = L"label62";
			this->label62->Size = System::Drawing::Size(66, 13);
			this->label62->TabIndex = 3;
			this->label62->Text = L"Процессор:";
			// 
			// label63
			// 
			this->label63->AutoSize = true;
			this->label63->Location = System::Drawing::Point(217, 27);
			this->label63->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label63->Name = L"label63";
			this->label63->Size = System::Drawing::Size(60, 13);
			this->label63->TabIndex = 1;
			this->label63->Text = L"Название:";
			// 
			// pictureBox6
			// 
			this->pictureBox6->Cursor = System::Windows::Forms::Cursors::Hand;
			this->pictureBox6->Location = System::Drawing::Point(15, 16);
			this->pictureBox6->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox6->Name = L"pictureBox6";
			this->pictureBox6->Size = System::Drawing::Size(180, 195);
			this->pictureBox6->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox6->TabIndex = 0;
			this->pictureBox6->TabStop = false;
			this->pictureBox6->Click += gcnew System::EventHandler(this, &ChangeSpecs::pictureBox_Click);
			// 
			// groupBox7
			// 
			this->groupBox7->Controls->Add(this->button31);
			this->groupBox7->Controls->Add(this->textBox42);
			this->groupBox7->Controls->Add(this->textBox43);
			this->groupBox7->Controls->Add(this->comboBox19);
			this->groupBox7->Controls->Add(this->comboBox20);
			this->groupBox7->Controls->Add(this->comboBox21);
			this->groupBox7->Controls->Add(this->textBox44);
			this->groupBox7->Controls->Add(this->textBox45);
			this->groupBox7->Controls->Add(this->textBox46);
			this->groupBox7->Controls->Add(this->textBox47);
			this->groupBox7->Controls->Add(this->label33);
			this->groupBox7->Controls->Add(this->label64);
			this->groupBox7->Controls->Add(this->AmountOrder6);
			this->groupBox7->Controls->Add(this->button7);
			this->groupBox7->Controls->Add(this->button21);
			this->groupBox7->Controls->Add(this->label65);
			this->groupBox7->Controls->Add(this->button22);
			this->groupBox7->Controls->Add(this->label66);
			this->groupBox7->Controls->Add(this->label67);
			this->groupBox7->Controls->Add(this->label68);
			this->groupBox7->Controls->Add(this->label69);
			this->groupBox7->Controls->Add(this->label70);
			this->groupBox7->Controls->Add(this->label71);
			this->groupBox7->Controls->Add(this->label72);
			this->groupBox7->Controls->Add(this->pictureBox7);
			this->groupBox7->Location = System::Drawing::Point(19, 1415);
			this->groupBox7->Margin = System::Windows::Forms::Padding(2);
			this->groupBox7->Name = L"groupBox7";
			this->groupBox7->Padding = System::Windows::Forms::Padding(2);
			this->groupBox7->Size = System::Drawing::Size(782, 222);
			this->groupBox7->TabIndex = 48;
			this->groupBox7->TabStop = false;
			this->groupBox7->Visible = false;
			// 
			// button31
			// 
			this->button31->BackColor = System::Drawing::Color::Red;
			this->button31->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button31->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button31->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button31->Location = System::Drawing::Point(528, 172);
			this->button31->Margin = System::Windows::Forms::Padding(2);
			this->button31->Name = L"button31";
			this->button31->Size = System::Drawing::Size(125, 39);
			this->button31->TabIndex = 46;
			this->button31->Text = L"Удалить";
			this->button31->UseVisualStyleBackColor = false;
			this->button31->Click += gcnew System::EventHandler(this, &ChangeSpecs::button31_Click);
			// 
			// textBox42
			// 
			this->textBox42->Location = System::Drawing::Point(332, 24);
			this->textBox42->Name = L"textBox42";
			this->textBox42->Size = System::Drawing::Size(120, 20);
			this->textBox42->TabIndex = 39;
			// 
			// textBox43
			// 
			this->textBox43->Location = System::Drawing::Point(655, 24);
			this->textBox43->Name = L"textBox43";
			this->textBox43->Size = System::Drawing::Size(106, 20);
			this->textBox43->TabIndex = 38;
			this->textBox43->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// comboBox19
			// 
			this->comboBox19->AutoCompleteCustomSource->AddRange(gcnew cli::array< System::String^  >(7) {
				L"64", L"128", L"240", L"256",
					L"512", L"1024", L"2048"
			});
			this->comboBox19->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox19->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox19->FormattingEnabled = true;
			this->comboBox19->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"64", L"128", L"240", L"256", L"512", L"1024",
					L"2048"
			});
			this->comboBox19->Location = System::Drawing::Point(655, 107);
			this->comboBox19->Name = L"comboBox19";
			this->comboBox19->Size = System::Drawing::Size(106, 21);
			this->comboBox19->TabIndex = 37;
			// 
			// comboBox20
			// 
			this->comboBox20->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox20->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox20->FormattingEnabled = true;
			this->comboBox20->Items->AddRange(gcnew cli::array< System::Object^  >(9) {
				L"32", L"64", L"128", L"256", L"512", L"1024",
					L"2048", L"4096", L"8192"
			});
			this->comboBox20->Location = System::Drawing::Point(655, 80);
			this->comboBox20->Name = L"comboBox20";
			this->comboBox20->Size = System::Drawing::Size(106, 21);
			this->comboBox20->TabIndex = 36;
			// 
			// comboBox21
			// 
			this->comboBox21->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox21->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox21->FormattingEnabled = true;
			this->comboBox21->Items->AddRange(gcnew cli::array< System::Object^  >(8) { L"2", L"4", L"6", L"8", L"16", L"32", L"64", L"128" });
			this->comboBox21->Location = System::Drawing::Point(655, 51);
			this->comboBox21->Name = L"comboBox21";
			this->comboBox21->Size = System::Drawing::Size(106, 21);
			this->comboBox21->TabIndex = 35;
			// 
			// textBox44
			// 
			this->textBox44->Location = System::Drawing::Point(332, 140);
			this->textBox44->Name = L"textBox44";
			this->textBox44->Size = System::Drawing::Size(120, 20);
			this->textBox44->TabIndex = 30;
			this->textBox44->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// textBox45
			// 
			this->textBox45->Location = System::Drawing::Point(332, 111);
			this->textBox45->Name = L"textBox45";
			this->textBox45->Size = System::Drawing::Size(120, 20);
			this->textBox45->TabIndex = 29;
			// 
			// textBox46
			// 
			this->textBox46->Location = System::Drawing::Point(332, 84);
			this->textBox46->Name = L"textBox46";
			this->textBox46->Size = System::Drawing::Size(120, 20);
			this->textBox46->TabIndex = 28;
			// 
			// textBox47
			// 
			this->textBox47->Location = System::Drawing::Point(332, 54);
			this->textBox47->Name = L"textBox47";
			this->textBox47->Size = System::Drawing::Size(120, 20);
			this->textBox47->TabIndex = 27;
			// 
			// label33
			// 
			this->label33->AutoSize = true;
			this->label33->Location = System::Drawing::Point(525, 112);
			this->label33->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label33->Name = L"label33";
			this->label33->Size = System::Drawing::Size(70, 13);
			this->label33->TabIndex = 25;
			this->label33->Text = L"Объём SSD:";
			// 
			// label64
			// 
			this->label64->AutoSize = true;
			this->label64->Location = System::Drawing::Point(218, 143);
			this->label64->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label64->Name = L"label64";
			this->label64->Size = System::Drawing::Size(81, 13);
			this->label64->TabIndex = 23;
			this->label64->Text = L"Мощность БП:";
			// 
			// AmountOrder6
			// 
			this->AmountOrder6->ForeColor = System::Drawing::Color::Black;
			this->AmountOrder6->Location = System::Drawing::Point(667, 137);
			this->AmountOrder6->Margin = System::Windows::Forms::Padding(2);
			this->AmountOrder6->MaxLength = 3;
			this->AmountOrder6->Name = L"AmountOrder6";
			this->AmountOrder6->Size = System::Drawing::Size(80, 20);
			this->AmountOrder6->TabIndex = 20;
			this->AmountOrder6->Text = L"1";
			this->AmountOrder6->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->AmountOrder6->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// button7
			// 
			this->button7->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button7->Location = System::Drawing::Point(751, 137);
			this->button7->Margin = System::Windows::Forms::Padding(2);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(22, 20);
			this->button7->TabIndex = 19;
			this->button7->Text = L"+";
			this->button7->UseVisualStyleBackColor = true;
			this->button7->Click += gcnew System::EventHandler(this, &ChangeSpecs::button7_Click);
			// 
			// button21
			// 
			this->button21->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button21->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button21->Location = System::Drawing::Point(641, 137);
			this->button21->Margin = System::Windows::Forms::Padding(2);
			this->button21->Name = L"button21";
			this->button21->Size = System::Drawing::Size(22, 20);
			this->button21->TabIndex = 18;
			this->button21->Text = L"-";
			this->button21->UseVisualStyleBackColor = true;
			this->button21->Click += gcnew System::EventHandler(this, &ChangeSpecs::button21_Click);
			// 
			// label65
			// 
			this->label65->AutoSize = true;
			this->label65->Location = System::Drawing::Point(525, 140);
			this->label65->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label65->Name = L"label65";
			this->label65->Size = System::Drawing::Size(69, 13);
			this->label65->TabIndex = 16;
			this->label65->Text = L"Количество:";
			// 
			// button22
			// 
			this->button22->BackColor = System::Drawing::SystemColors::MenuHighlight;
			this->button22->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button22->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button22->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button22->Location = System::Drawing::Point(363, 172);
			this->button22->Margin = System::Windows::Forms::Padding(2);
			this->button22->Name = L"button22";
			this->button22->Size = System::Drawing::Size(125, 39);
			this->button22->TabIndex = 15;
			this->button22->Text = L"Изменить";
			this->button22->UseVisualStyleBackColor = false;
			this->button22->Click += gcnew System::EventHandler(this, &ChangeSpecs::button22_Click);
			// 
			// label66
			// 
			this->label66->AutoSize = true;
			this->label66->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label66->Location = System::Drawing::Point(525, 27);
			this->label66->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label66->Name = L"label66";
			this->label66->Size = System::Drawing::Size(36, 13);
			this->label66->TabIndex = 13;
			this->label66->Text = L"Цена:";
			// 
			// label67
			// 
			this->label67->AutoSize = true;
			this->label67->Location = System::Drawing::Point(525, 84);
			this->label67->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label67->Name = L"label67";
			this->label67->Size = System::Drawing::Size(72, 13);
			this->label67->TabIndex = 11;
			this->label67->Text = L"Объём HDD:";
			// 
			// label68
			// 
			this->label68->AutoSize = true;
			this->label68->Location = System::Drawing::Point(218, 115);
			this->label68->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label68->Name = L"label68";
			this->label68->Size = System::Drawing::Size(110, 13);
			this->label68->TabIndex = 9;
			this->label68->Text = L"Материнская плата:";
			// 
			// label69
			// 
			this->label69->AutoSize = true;
			this->label69->Location = System::Drawing::Point(525, 55);
			this->label69->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label69->Name = L"label69";
			this->label69->Size = System::Drawing::Size(71, 13);
			this->label69->TabIndex = 7;
			this->label69->Text = L"Объём ОЗУ:";
			// 
			// label70
			// 
			this->label70->AutoSize = true;
			this->label70->Location = System::Drawing::Point(218, 87);
			this->label70->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label70->Name = L"label70";
			this->label70->Size = System::Drawing::Size(70, 13);
			this->label70->TabIndex = 5;
			this->label70->Text = L"Видеокарта:";
			// 
			// label71
			// 
			this->label71->AutoSize = true;
			this->label71->Location = System::Drawing::Point(218, 58);
			this->label71->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label71->Name = L"label71";
			this->label71->Size = System::Drawing::Size(66, 13);
			this->label71->TabIndex = 3;
			this->label71->Text = L"Процессор:";
			// 
			// label72
			// 
			this->label72->AutoSize = true;
			this->label72->Location = System::Drawing::Point(217, 27);
			this->label72->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label72->Name = L"label72";
			this->label72->Size = System::Drawing::Size(60, 13);
			this->label72->TabIndex = 1;
			this->label72->Text = L"Название:";
			// 
			// pictureBox7
			// 
			this->pictureBox7->Cursor = System::Windows::Forms::Cursors::Hand;
			this->pictureBox7->Location = System::Drawing::Point(15, 16);
			this->pictureBox7->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox7->Name = L"pictureBox7";
			this->pictureBox7->Size = System::Drawing::Size(180, 195);
			this->pictureBox7->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox7->TabIndex = 0;
			this->pictureBox7->TabStop = false;
			this->pictureBox7->Click += gcnew System::EventHandler(this, &ChangeSpecs::pictureBox_Click);
			// 
			// groupBox8
			// 
			this->groupBox8->Controls->Add(this->button32);
			this->groupBox8->Controls->Add(this->textBox49);
			this->groupBox8->Controls->Add(this->textBox50);
			this->groupBox8->Controls->Add(this->comboBox22);
			this->groupBox8->Controls->Add(this->comboBox23);
			this->groupBox8->Controls->Add(this->comboBox24);
			this->groupBox8->Controls->Add(this->textBox51);
			this->groupBox8->Controls->Add(this->textBox52);
			this->groupBox8->Controls->Add(this->textBox53);
			this->groupBox8->Controls->Add(this->textBox54);
			this->groupBox8->Controls->Add(this->label37);
			this->groupBox8->Controls->Add(this->label73);
			this->groupBox8->Controls->Add(this->AmountOrder7);
			this->groupBox8->Controls->Add(this->button8);
			this->groupBox8->Controls->Add(this->button23);
			this->groupBox8->Controls->Add(this->label74);
			this->groupBox8->Controls->Add(this->button24);
			this->groupBox8->Controls->Add(this->label75);
			this->groupBox8->Controls->Add(this->label76);
			this->groupBox8->Controls->Add(this->label77);
			this->groupBox8->Controls->Add(this->label78);
			this->groupBox8->Controls->Add(this->label79);
			this->groupBox8->Controls->Add(this->label80);
			this->groupBox8->Controls->Add(this->label81);
			this->groupBox8->Controls->Add(this->pictureBox8);
			this->groupBox8->Location = System::Drawing::Point(19, 1641);
			this->groupBox8->Margin = System::Windows::Forms::Padding(2);
			this->groupBox8->Name = L"groupBox8";
			this->groupBox8->Padding = System::Windows::Forms::Padding(2);
			this->groupBox8->Size = System::Drawing::Size(782, 222);
			this->groupBox8->TabIndex = 40;
			this->groupBox8->TabStop = false;
			this->groupBox8->Visible = false;
			// 
			// button32
			// 
			this->button32->BackColor = System::Drawing::Color::Red;
			this->button32->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button32->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button32->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button32->Location = System::Drawing::Point(528, 172);
			this->button32->Margin = System::Windows::Forms::Padding(2);
			this->button32->Name = L"button32";
			this->button32->Size = System::Drawing::Size(125, 39);
			this->button32->TabIndex = 47;
			this->button32->Text = L"Удалить";
			this->button32->UseVisualStyleBackColor = false;
			this->button32->Click += gcnew System::EventHandler(this, &ChangeSpecs::button32_Click);
			// 
			// textBox49
			// 
			this->textBox49->Location = System::Drawing::Point(332, 24);
			this->textBox49->Name = L"textBox49";
			this->textBox49->Size = System::Drawing::Size(120, 20);
			this->textBox49->TabIndex = 39;
			// 
			// textBox50
			// 
			this->textBox50->Location = System::Drawing::Point(655, 24);
			this->textBox50->Name = L"textBox50";
			this->textBox50->Size = System::Drawing::Size(106, 20);
			this->textBox50->TabIndex = 38;
			this->textBox50->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// comboBox22
			// 
			this->comboBox22->AutoCompleteCustomSource->AddRange(gcnew cli::array< System::String^  >(7) {
				L"64", L"128", L"240", L"256",
					L"512", L"1024", L"2048"
			});
			this->comboBox22->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox22->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox22->FormattingEnabled = true;
			this->comboBox22->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"64", L"128", L"240", L"256", L"512", L"1024",
					L"2048"
			});
			this->comboBox22->Location = System::Drawing::Point(655, 107);
			this->comboBox22->Name = L"comboBox22";
			this->comboBox22->Size = System::Drawing::Size(106, 21);
			this->comboBox22->TabIndex = 37;
			// 
			// comboBox23
			// 
			this->comboBox23->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox23->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox23->FormattingEnabled = true;
			this->comboBox23->Items->AddRange(gcnew cli::array< System::Object^  >(9) {
				L"32", L"64", L"128", L"256", L"512", L"1024",
					L"2048", L"4096", L"8192"
			});
			this->comboBox23->Location = System::Drawing::Point(655, 80);
			this->comboBox23->Name = L"comboBox23";
			this->comboBox23->Size = System::Drawing::Size(106, 21);
			this->comboBox23->TabIndex = 36;
			// 
			// comboBox24
			// 
			this->comboBox24->Cursor = System::Windows::Forms::Cursors::Hand;
			this->comboBox24->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox24->FormattingEnabled = true;
			this->comboBox24->Items->AddRange(gcnew cli::array< System::Object^  >(8) { L"2", L"4", L"6", L"8", L"16", L"32", L"64", L"128" });
			this->comboBox24->Location = System::Drawing::Point(655, 51);
			this->comboBox24->Name = L"comboBox24";
			this->comboBox24->Size = System::Drawing::Size(106, 21);
			this->comboBox24->TabIndex = 35;
			// 
			// textBox51
			// 
			this->textBox51->Location = System::Drawing::Point(332, 140);
			this->textBox51->Name = L"textBox51";
			this->textBox51->Size = System::Drawing::Size(120, 20);
			this->textBox51->TabIndex = 30;
			this->textBox51->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// textBox52
			// 
			this->textBox52->Location = System::Drawing::Point(332, 111);
			this->textBox52->Name = L"textBox52";
			this->textBox52->Size = System::Drawing::Size(120, 20);
			this->textBox52->TabIndex = 29;
			// 
			// textBox53
			// 
			this->textBox53->Location = System::Drawing::Point(332, 84);
			this->textBox53->Name = L"textBox53";
			this->textBox53->Size = System::Drawing::Size(120, 20);
			this->textBox53->TabIndex = 28;
			// 
			// textBox54
			// 
			this->textBox54->Location = System::Drawing::Point(332, 54);
			this->textBox54->Name = L"textBox54";
			this->textBox54->Size = System::Drawing::Size(120, 20);
			this->textBox54->TabIndex = 27;
			// 
			// label37
			// 
			this->label37->AutoSize = true;
			this->label37->Location = System::Drawing::Point(525, 112);
			this->label37->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label37->Name = L"label37";
			this->label37->Size = System::Drawing::Size(70, 13);
			this->label37->TabIndex = 25;
			this->label37->Text = L"Объём SSD:";
			// 
			// label73
			// 
			this->label73->AutoSize = true;
			this->label73->Location = System::Drawing::Point(218, 143);
			this->label73->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label73->Name = L"label73";
			this->label73->Size = System::Drawing::Size(81, 13);
			this->label73->TabIndex = 23;
			this->label73->Text = L"Мощность БП:";
			// 
			// AmountOrder7
			// 
			this->AmountOrder7->ForeColor = System::Drawing::Color::Black;
			this->AmountOrder7->Location = System::Drawing::Point(667, 137);
			this->AmountOrder7->Margin = System::Windows::Forms::Padding(2);
			this->AmountOrder7->MaxLength = 3;
			this->AmountOrder7->Name = L"AmountOrder7";
			this->AmountOrder7->Size = System::Drawing::Size(80, 20);
			this->AmountOrder7->TabIndex = 20;
			this->AmountOrder7->Text = L"1";
			this->AmountOrder7->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->AmountOrder7->TextChanged += gcnew System::EventHandler(this, &ChangeSpecs::textBoxChange_TextChanged);
			// 
			// button8
			// 
			this->button8->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button8->Location = System::Drawing::Point(751, 137);
			this->button8->Margin = System::Windows::Forms::Padding(2);
			this->button8->Name = L"button8";
			this->button8->Size = System::Drawing::Size(22, 20);
			this->button8->TabIndex = 19;
			this->button8->Text = L"+";
			this->button8->UseVisualStyleBackColor = true;
			this->button8->Click += gcnew System::EventHandler(this, &ChangeSpecs::button8_Click);
			// 
			// button23
			// 
			this->button23->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button23->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button23->Location = System::Drawing::Point(641, 137);
			this->button23->Margin = System::Windows::Forms::Padding(2);
			this->button23->Name = L"button23";
			this->button23->Size = System::Drawing::Size(22, 20);
			this->button23->TabIndex = 18;
			this->button23->Text = L"-";
			this->button23->UseVisualStyleBackColor = true;
			this->button23->Click += gcnew System::EventHandler(this, &ChangeSpecs::button23_Click);
			// 
			// label74
			// 
			this->label74->AutoSize = true;
			this->label74->Location = System::Drawing::Point(525, 140);
			this->label74->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label74->Name = L"label74";
			this->label74->Size = System::Drawing::Size(69, 13);
			this->label74->TabIndex = 16;
			this->label74->Text = L"Количество:";
			// 
			// button24
			// 
			this->button24->BackColor = System::Drawing::SystemColors::MenuHighlight;
			this->button24->Cursor = System::Windows::Forms::Cursors::Hand;
			this->button24->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button24->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button24->Location = System::Drawing::Point(363, 172);
			this->button24->Margin = System::Windows::Forms::Padding(2);
			this->button24->Name = L"button24";
			this->button24->Size = System::Drawing::Size(125, 39);
			this->button24->TabIndex = 15;
			this->button24->Text = L"Изменить";
			this->button24->UseVisualStyleBackColor = false;
			this->button24->Click += gcnew System::EventHandler(this, &ChangeSpecs::button24_Click);
			// 
			// label75
			// 
			this->label75->AutoSize = true;
			this->label75->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label75->Location = System::Drawing::Point(525, 27);
			this->label75->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label75->Name = L"label75";
			this->label75->Size = System::Drawing::Size(36, 13);
			this->label75->TabIndex = 13;
			this->label75->Text = L"Цена:";
			// 
			// label76
			// 
			this->label76->AutoSize = true;
			this->label76->Location = System::Drawing::Point(525, 84);
			this->label76->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label76->Name = L"label76";
			this->label76->Size = System::Drawing::Size(129, 13);
			this->label76->TabIndex = 11;
			this->label76->Text = L"Объём жёсткого диска:";
			// 
			// label77
			// 
			this->label77->AutoSize = true;
			this->label77->Location = System::Drawing::Point(218, 115);
			this->label77->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label77->Name = L"label77";
			this->label77->Size = System::Drawing::Size(110, 13);
			this->label77->TabIndex = 9;
			this->label77->Text = L"Материнская плата:";
			// 
			// label78
			// 
			this->label78->AutoSize = true;
			this->label78->Location = System::Drawing::Point(525, 55);
			this->label78->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label78->Name = L"label78";
			this->label78->Size = System::Drawing::Size(64, 13);
			this->label78->TabIndex = 7;
			this->label78->Text = L"Объём ОП:";
			// 
			// label79
			// 
			this->label79->AutoSize = true;
			this->label79->Location = System::Drawing::Point(218, 87);
			this->label79->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label79->Name = L"label79";
			this->label79->Size = System::Drawing::Size(70, 13);
			this->label79->TabIndex = 5;
			this->label79->Text = L"Видеокарта:";
			// 
			// label80
			// 
			this->label80->AutoSize = true;
			this->label80->Location = System::Drawing::Point(218, 58);
			this->label80->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label80->Name = L"label80";
			this->label80->Size = System::Drawing::Size(66, 13);
			this->label80->TabIndex = 3;
			this->label80->Text = L"Процессор:";
			// 
			// label81
			// 
			this->label81->AutoSize = true;
			this->label81->Location = System::Drawing::Point(217, 27);
			this->label81->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label81->Name = L"label81";
			this->label81->Size = System::Drawing::Size(60, 13);
			this->label81->TabIndex = 1;
			this->label81->Text = L"Название:";
			// 
			// pictureBox8
			// 
			this->pictureBox8->Cursor = System::Windows::Forms::Cursors::Hand;
			this->pictureBox8->Location = System::Drawing::Point(15, 16);
			this->pictureBox8->Margin = System::Windows::Forms::Padding(2);
			this->pictureBox8->Name = L"pictureBox8";
			this->pictureBox8->Size = System::Drawing::Size(180, 195);
			this->pictureBox8->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->pictureBox8->TabIndex = 0;
			this->pictureBox8->TabStop = false;
			this->pictureBox8->Click += gcnew System::EventHandler(this, &ChangeSpecs::pictureBox_Click);
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->Filter = L"JPEG|*.jpg|PNG|*.png";
			// 
			// linkLabel3
			// 
			this->linkLabel3->AutoSize = true;
			this->linkLabel3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->linkLabel3->Location = System::Drawing::Point(689, 26);
			this->linkLabel3->Name = L"linkLabel3";
			this->linkLabel3->Size = System::Drawing::Size(112, 16);
			this->linkLabel3->TabIndex = 49;
			this->linkLabel3->TabStop = true;
			this->linkLabel3->Text = L"В главное меню";
			this->linkLabel3->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &ChangeSpecs::linkLabel3_LinkClicked);
			// 
			// ChangeSpecs
			// 
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::None;
			this->AutoScroll = true;
			this->AutoValidate = System::Windows::Forms::AutoValidate::Disable;
			this->BackColor = System::Drawing::SystemColors::ButtonHighlight;
			this->ClientSize = System::Drawing::Size(831, 758);
			this->Controls->Add(this->linkLabel3);
			this->Controls->Add(this->groupBox8);
			this->Controls->Add(this->groupBox7);
			this->Controls->Add(this->groupBox6);
			this->Controls->Add(this->groupBox5);
			this->Controls->Add(this->groupBox4);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->linkLabel2);
			this->Controls->Add(this->linkLabel1);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->label1);
			this->Cursor = System::Windows::Forms::Cursors::Default;
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->Margin = System::Windows::Forms::Padding(2);
			this->MaximizeBox = false;
			this->Name = L"ChangeSpecs";
			this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Изменение характеристик";
			this->Load += gcnew System::EventHandler(this, &ChangeSpecs::ChangeSpecs_Load);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->EndInit();
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->EndInit();
			this->groupBox5->ResumeLayout(false);
			this->groupBox5->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox5))->EndInit();
			this->groupBox6->ResumeLayout(false);
			this->groupBox6->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox6))->EndInit();
			this->groupBox7->ResumeLayout(false);
			this->groupBox7->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox7))->EndInit();
			this->groupBox8->ResumeLayout(false);
			this->groupBox8->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox8))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void ChangeSpecs_Load(System::Object^ sender, System::EventArgs^ e)
	{
		int fileLength = getFileLength(PATH_CATALOG);
		Computers* computer;
		computer = getComputers(adminPage * 8);

		int groupBoxYcoordinate = 230;

		char buff[10];

		textBox6->Text = msclr::interop::marshal_as<System::String^>(computer[0].name);
		textBox1->Text = msclr::interop::marshal_as<System::String^>(computer[0].component.cpu);
		textBox2->Text = msclr::interop::marshal_as<System::String^>(computer[0].component.gpu);
		textBox3->Text = msclr::interop::marshal_as<System::String^>(computer[0].component.motherboard);
		textBox4->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[0].component.psu, buff, 10));
		comboBox1->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[0].component.ram, buff, 10));
		comboBox2->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[0].component.hdd, buff, 10));
		comboBox3->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[0].component.ssd, buff, 10));
		AmountOrder0->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[0].amount, buff, 10));
		string str = _itoa(computer[0].cost, buff, 10);
		textBox5->Text = msclr::interop::marshal_as<System::String^>(str);
		pictureBox1->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[0].photo);

		int diff = fileLength - adminPage * 8;
		diff = abs(diff);

		if (diff <= 8)
		{
			linkLabel2->Hide();
			this->linkLabel1->Location = System::Drawing::Point(16, 1881 - groupBoxYcoordinate * (8 - diff));
			groupBox2->Hide();
			groupBox3->Hide();
			groupBox4->Hide();
			groupBox5->Hide();
			groupBox6->Hide();
			groupBox7->Hide();
			groupBox8->Hide();
			switch (diff - 1)
			{
			case 7:
			{
				groupBox8->Show();
			}
			case 6:
			{
				groupBox7->Show();
			}
			case 5:
			{
				groupBox6->Show();
			}
			case 4:
			{
				groupBox5->Show();
			}
			case 3:
			{
				groupBox4->Show();
			}
			case 2:
			{
				groupBox3->Show();
			}
			case 1:
			{
				groupBox2->Show();
			}
			}

			if (diff == 2)
			{
				this->ClientSize = System::Drawing::Size(831, 758 - groupBoxYcoordinate);
			}
			if (diff == 1)
			{
				this->ClientSize = System::Drawing::Size(831, 758 - groupBoxYcoordinate * 2);
			}
		}
		else
		{
			this->linkLabel1->Location = System::Drawing::Point(16, 1865);
			linkLabel2->Show();
			groupBox2->Show();
			groupBox3->Show();
			groupBox4->Show();
			groupBox5->Show();
			groupBox6->Show();
			groupBox7->Show();
			groupBox8->Show();
		}

		if (diff != fileLength)
		{
			linkLabel1->Show();
		}
		else
		{
			linkLabel1->Hide();
		}

		if (groupBox2->Visible == true)
		{
			textBox7->Text = msclr::interop::marshal_as<System::String^>(computer[1].name);
			textBox12->Text = msclr::interop::marshal_as<System::String^>(computer[1].component.cpu);
			textBox11->Text = msclr::interop::marshal_as<System::String^>(computer[1].component.gpu);
			textBox10->Text = msclr::interop::marshal_as<System::String^>(computer[1].component.motherboard);
			textBox9->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[1].component.psu, buff, 10));
			comboBox6->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[1].component.ram, buff, 10));
			comboBox5->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[1].component.hdd, buff, 10));
			comboBox4->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[1].component.ssd, buff, 10));
			AmountOrder1->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[1].amount, buff, 10));
			str = _itoa(computer[1].cost, buff, 10);
			textBox8->Text = msclr::interop::marshal_as<System::String^>(str);
			pictureBox2->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[1].photo);
		}

		if (groupBox3->Visible == true)
		{
			textBox14->Text = msclr::interop::marshal_as<System::String^>(computer[2].name);
			textBox19->Text = msclr::interop::marshal_as<System::String^>(computer[2].component.cpu);
			textBox18->Text = msclr::interop::marshal_as<System::String^>(computer[2].component.gpu);
			textBox17->Text = msclr::interop::marshal_as<System::String^>(computer[2].component.motherboard);
			textBox16->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[2].component.psu, buff, 10));
			comboBox9->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[2].component.ram, buff, 10));
			comboBox8->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[2].component.hdd, buff, 10));
			comboBox7->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[2].component.ssd, buff, 10));
			AmountOrder2->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[2].amount, buff, 10));
			str = _itoa(computer[2].cost, buff, 10);
			textBox15->Text = msclr::interop::marshal_as<System::String^>(str);
			pictureBox3->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[2].photo);
		}

		if (groupBox4->Visible == true)
		{
			textBox21->Text = msclr::interop::marshal_as<System::String^>(computer[3].name);
			textBox26->Text = msclr::interop::marshal_as<System::String^>(computer[3].component.cpu);
			textBox25->Text = msclr::interop::marshal_as<System::String^>(computer[3].component.gpu);
			textBox24->Text = msclr::interop::marshal_as<System::String^>(computer[3].component.motherboard);
			textBox23->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[3].component.psu, buff, 10));
			comboBox12->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[3].component.ram, buff, 10));
			comboBox11->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[3].component.hdd, buff, 10));
			comboBox10->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[3].component.ssd, buff, 10));
			AmountOrder3->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[3].amount, buff, 10));
			str = _itoa(computer[3].cost, buff, 10);
			textBox22->Text = msclr::interop::marshal_as<System::String^>(str);
			pictureBox4->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[3].photo);
		}

		if (groupBox5->Visible == true)
		{
			textBox28->Text = msclr::interop::marshal_as<System::String^>(computer[4].name);
			textBox33->Text = msclr::interop::marshal_as<System::String^>(computer[4].component.cpu);
			textBox32->Text = msclr::interop::marshal_as<System::String^>(computer[4].component.gpu);
			textBox31->Text = msclr::interop::marshal_as<System::String^>(computer[4].component.motherboard);
			textBox30->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[4].component.psu, buff, 10));
			comboBox15->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[4].component.ram, buff, 10));
			comboBox14->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[4].component.hdd, buff, 10));
			comboBox13->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[4].component.ssd, buff, 10));
			AmountOrder4->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[4].amount, buff, 10));
			str = _itoa(computer[4].cost, buff, 10);
			textBox29->Text = msclr::interop::marshal_as<System::String^>(str);
			pictureBox5->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[4].photo);
		}

		if (groupBox6->Visible == true)
		{
			textBox35->Text = msclr::interop::marshal_as<System::String^>(computer[5].name);
			textBox40->Text = msclr::interop::marshal_as<System::String^>(computer[5].component.cpu);
			textBox39->Text = msclr::interop::marshal_as<System::String^>(computer[5].component.gpu);
			textBox38->Text = msclr::interop::marshal_as<System::String^>(computer[5].component.motherboard);
			textBox37->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[5].component.psu, buff, 10));
			comboBox18->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[5].component.ram, buff, 10));
			comboBox17->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[5].component.hdd, buff, 10));
			comboBox16->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[5].component.ssd, buff, 10));
			AmountOrder5->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[5].amount, buff, 10));
			str = _itoa(computer[5].cost, buff, 10);
			textBox36->Text = msclr::interop::marshal_as<System::String^>(str);
			pictureBox6->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[5].photo);
		}

		if (groupBox7->Visible == true)
		{
			textBox42->Text = msclr::interop::marshal_as<System::String^>(computer[6].name);
			textBox47->Text = msclr::interop::marshal_as<System::String^>(computer[6].component.cpu);
			textBox46->Text = msclr::interop::marshal_as<System::String^>(computer[6].component.gpu);
			textBox45->Text = msclr::interop::marshal_as<System::String^>(computer[6].component.motherboard);
			textBox44->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[6].component.psu, buff, 10));
			comboBox21->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[6].component.ram, buff, 10));
			comboBox20->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[6].component.hdd, buff, 10));
			comboBox19->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[6].component.ssd, buff, 10));
			AmountOrder6->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[6].amount, buff, 10));
			str = _itoa(computer[6].cost, buff, 10);
			textBox43->Text = msclr::interop::marshal_as<System::String^>(str);
			pictureBox7->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[6].photo);
		}

		if (groupBox8->Visible == true)
		{
			textBox49->Text = msclr::interop::marshal_as<System::String^>(computer[7].name);
			textBox54->Text = msclr::interop::marshal_as<System::String^>(computer[7].component.cpu);
			textBox53->Text = msclr::interop::marshal_as<System::String^>(computer[7].component.gpu);
			textBox52->Text = msclr::interop::marshal_as<System::String^>(computer[7].component.motherboard);
			textBox51->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[7].component.psu, buff, 10));
			comboBox24->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[7].component.ram, buff, 10));
			comboBox23->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[7].component.hdd, buff, 10));
			comboBox22->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[7].component.ssd, buff, 10));
			AmountOrder7->Text = msclr::interop::marshal_as<System::String^>(_itoa(computer[7].amount, buff, 10));
			str = _itoa(computer[7].cost, buff, 10);
			textBox50->Text = msclr::interop::marshal_as<System::String^>(str);
			pictureBox8->ImageLocation = msclr::interop::marshal_as<System::String^>(std::experimental::filesystem::current_path().string() + computer[7].photo);
		}

		delete[8] computer;
	}

	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e)
	{
		Color color;
		if ((textBox4->ForeColor == color.Black) && (AmountOrder0->ForeColor == color.Black) && (textBox5->ForeColor == color.Black))
		{
			string cpu = msclr::interop::marshal_as<std::string>(textBox1->Text);
			string gpu = msclr::interop::marshal_as<std::string>(textBox2->Text);
			string name = msclr::interop::marshal_as<std::string>(textBox6->Text);
			string motherboard = msclr::interop::marshal_as<std::string>(textBox3->Text);
			int spu = atoi(msclr::interop::marshal_as<std::string>(textBox4->Text).c_str());
			int amount = atoi(msclr::interop::marshal_as<std::string>(AmountOrder0->Text).c_str());
			long int cost = atol(msclr::interop::marshal_as<std::string>(textBox5->Text).c_str());
			int ram = atoi(msclr::interop::marshal_as<std::string>(comboBox1->Text).c_str());
			int hdd = atoi(msclr::interop::marshal_as<std::string>(comboBox2->Text).c_str());
			int ssd = atoi(msclr::interop::marshal_as<std::string>(comboBox3->Text).c_str());

			Computers* computer = getComputers(adminPage * 8);
			string photo = std::experimental::filesystem::current_path().string() + computer[0].photo;
			photo = std::experimental::filesystem::current_path().string() + "\\Image\\" + name + ".jpg";
			wstring wphoto(photo.begin(), photo.end());
			std::wstring copyPhoto = msclr::interop::marshal_as<std::wstring>(pictureBox1->ImageLocation->ToString());
			CopyFileW((LPCWSTR)copyPhoto.c_str(), (LPCWSTR)wphoto.c_str(), false);
			if (name != computer[0].name)
			{
				photo = std::experimental::filesystem::current_path().string() + "\\Image\\" + computer[0].name + ".jpg";
				wphoto.clear();
				wphoto.append(photo.begin(), photo.end());
				DeleteFileW((LPCWSTR)wphoto.c_str());
			}
			photo = "\\Image\\" + name + ".jpg";

			addComputerToStore(name, cpu, gpu, motherboard, ram, spu, hdd, ssd, amount, cost, photo, 0);
			MessageBox::Show("Изменение проведено успешно!");
		}
		else
		{
			if (textBox4->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для мощности БП!");
			}

			if (AmountOrder0->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для количества товара!");
			}

			if (textBox5->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для цены товара!");
			}
		}
	}

	private: System::Void button12_Click(System::Object^ sender, System::EventArgs^ e)
	{
		Color color;
		if ((textBox9->ForeColor == color.Black) && (AmountOrder1->ForeColor == color.Black) && (textBox8->ForeColor == color.Black))
		{
			string cpu = msclr::interop::marshal_as<std::string>(textBox12->Text);
			string gpu = msclr::interop::marshal_as<std::string>(textBox11->Text);
			string name = msclr::interop::marshal_as<std::string>(textBox7->Text);
			string motherboard = msclr::interop::marshal_as<std::string>(textBox10->Text);
			int spu = atoi(msclr::interop::marshal_as<std::string>(textBox9->Text).c_str());
			int amount = atoi(msclr::interop::marshal_as<std::string>(AmountOrder1->Text).c_str());
			long int cost = atol(msclr::interop::marshal_as<std::string>(textBox8->Text).c_str());
			int ram = atoi(msclr::interop::marshal_as<std::string>(comboBox6->Text).c_str());
			int hdd = atoi(msclr::interop::marshal_as<std::string>(comboBox5->Text).c_str());
			int ssd = atoi(msclr::interop::marshal_as<std::string>(comboBox4->Text).c_str());

			Computers* computer = getComputers(adminPage * 8);
			string photo = std::experimental::filesystem::current_path().string() + computer[1].photo;
			photo = std::experimental::filesystem::current_path().string() + "\\Image\\" + name + ".jpg";
			wstring wphoto(photo.begin(), photo.end());
			std::wstring copyPhoto = msclr::interop::marshal_as<std::wstring>(pictureBox2->ImageLocation->ToString());
			CopyFileW((LPCWSTR)copyPhoto.c_str(), (LPCWSTR)wphoto.c_str(), false);
			if (name != computer[1].name)
			{
				photo = std::experimental::filesystem::current_path().string() + "\\Image\\" + computer[1].name + ".jpg";
				wphoto.clear();
				wphoto.append(photo.begin(), photo.end());
				DeleteFileW((LPCWSTR)wphoto.c_str());
			}
			photo = "\\Image\\" + name + ".jpg";

			addComputerToStore(name, cpu, gpu, motherboard, ram, spu, hdd, ssd, amount, cost, photo, 1);
			MessageBox::Show("Изменение проведено успешно!");
		}
		else
		{
			if (textBox9->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для мощности БП!");
			}

			if (AmountOrder1->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для количества товара!");
			}

			if (textBox8->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для цены товара!");
			}
		}
	}

	private: System::Void button14_Click(System::Object^ sender, System::EventArgs^ e)
	{
		Color color;
		if ((textBox16->ForeColor == color.Black) && (AmountOrder2->ForeColor == color.Black) && (textBox15->ForeColor == color.Black))
		{
			string cpu = msclr::interop::marshal_as<std::string>(textBox19->Text);
			string gpu = msclr::interop::marshal_as<std::string>(textBox18->Text);
			string name = msclr::interop::marshal_as<std::string>(textBox14->Text);
			string motherboard = msclr::interop::marshal_as<std::string>(textBox17->Text);
			int spu = atoi(msclr::interop::marshal_as<std::string>(textBox16->Text).c_str());
			int amount = atoi(msclr::interop::marshal_as<std::string>(AmountOrder2->Text).c_str());
			long int cost = atol(msclr::interop::marshal_as<std::string>(textBox15->Text).c_str());
			int ram = atoi(msclr::interop::marshal_as<std::string>(comboBox9->Text).c_str());
			int hdd = atoi(msclr::interop::marshal_as<std::string>(comboBox8->Text).c_str());
			int ssd = atoi(msclr::interop::marshal_as<std::string>(comboBox7->Text).c_str());

			Computers* computer = getComputers(adminPage * 8);
			string photo = std::experimental::filesystem::current_path().string() + computer[2].photo;
			photo = std::experimental::filesystem::current_path().string() + "\\Image\\" + name + ".jpg";
			wstring wphoto(photo.begin(), photo.end());
			std::wstring copyPhoto = msclr::interop::marshal_as<std::wstring>(pictureBox3->ImageLocation->ToString());
			CopyFileW((LPCWSTR)copyPhoto.c_str(), (LPCWSTR)wphoto.c_str(), false);
			if (name != computer[2].name)
			{
				photo = std::experimental::filesystem::current_path().string() + "\\Image\\" + computer[2].name + ".jpg";
				wphoto.clear();
				wphoto.append(photo.begin(), photo.end());
				DeleteFileW((LPCWSTR)wphoto.c_str());
			}
			photo = "\\Image\\" + name + ".jpg";

			addComputerToStore(name, cpu, gpu, motherboard, ram, spu, hdd, ssd, amount, cost, photo, 2);
			MessageBox::Show("Изменение проведено успешно!");
		}
		else
		{
			if (textBox16->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для мощности БП!");
			}

			if (AmountOrder2->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для количества товара!");
			}

			if (textBox15->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для цены товара!");
			}
		}
	}

	private: System::Void button16_Click(System::Object^ sender, System::EventArgs^ e)
	{
		Color color;
		if ((textBox23->ForeColor == color.Black) && (AmountOrder3->ForeColor == color.Black) && (textBox22->ForeColor == color.Black))
		{
			string cpu = msclr::interop::marshal_as<std::string>(textBox26->Text);
			string gpu = msclr::interop::marshal_as<std::string>(textBox25->Text);
			string name = msclr::interop::marshal_as<std::string>(textBox21->Text);
			string motherboard = msclr::interop::marshal_as<std::string>(textBox24->Text);
			int spu = atoi(msclr::interop::marshal_as<std::string>(textBox23->Text).c_str());
			int amount = atoi(msclr::interop::marshal_as<std::string>(AmountOrder3->Text).c_str());
			long int cost = atol(msclr::interop::marshal_as<std::string>(textBox22->Text).c_str());
			int ram = atoi(msclr::interop::marshal_as<std::string>(comboBox12->Text).c_str());
			int hdd = atoi(msclr::interop::marshal_as<std::string>(comboBox11->Text).c_str());
			int ssd = atoi(msclr::interop::marshal_as<std::string>(comboBox10->Text).c_str());

			Computers* computer = getComputers(adminPage * 8);
			string photo = std::experimental::filesystem::current_path().string() + computer[3].photo;
			photo = std::experimental::filesystem::current_path().string() + "\\Image\\" + name + ".jpg";
			wstring wphoto(photo.begin(), photo.end());
			std::wstring copyPhoto = msclr::interop::marshal_as<std::wstring>(pictureBox4->ImageLocation->ToString());
			CopyFileW((LPCWSTR)copyPhoto.c_str(), (LPCWSTR)wphoto.c_str(), false);
			if (name != computer[3].name)
			{
				photo = std::experimental::filesystem::current_path().string() + "\\Image\\" + computer[3].name + ".jpg";
				wphoto.clear();
				wphoto.append(photo.begin(), photo.end());
				DeleteFileW((LPCWSTR)wphoto.c_str());
			}
			photo = "\\Image\\" + name + ".jpg";

			addComputerToStore(name, cpu, gpu, motherboard, ram, spu, hdd, ssd, amount, cost, photo, 3);
			MessageBox::Show("Изменение проведено успешно!");
		}
		else
		{
			if (textBox23->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для мощности БП!");
			}

			if (AmountOrder3->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для количества товара!");
			}

			if (textBox22->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для цены товара!");
			}
		}
	}

	private: System::Void button17_Click(System::Object^ sender, System::EventArgs^ e)
	{
		Color color;
		if ((textBox30->ForeColor == color.Black) && (AmountOrder4->ForeColor == color.Black) && (textBox29->ForeColor == color.Black))
		{
			string cpu = msclr::interop::marshal_as<std::string>(textBox33->Text);
			string gpu = msclr::interop::marshal_as<std::string>(textBox32->Text);
			string name = msclr::interop::marshal_as<std::string>(textBox28->Text);
			string motherboard = msclr::interop::marshal_as<std::string>(textBox31->Text);
			int spu = atoi(msclr::interop::marshal_as<std::string>(textBox30->Text).c_str());
			int amount = atoi(msclr::interop::marshal_as<std::string>(AmountOrder4->Text).c_str());
			long int cost = atol(msclr::interop::marshal_as<std::string>(textBox29->Text).c_str());
			int ram = atoi(msclr::interop::marshal_as<std::string>(comboBox15->Text).c_str());
			int hdd = atoi(msclr::interop::marshal_as<std::string>(comboBox14->Text).c_str());
			int ssd = atoi(msclr::interop::marshal_as<std::string>(comboBox13->Text).c_str());

			Computers* computer = getComputers(adminPage * 8);
			string photo = std::experimental::filesystem::current_path().string() + computer[4].photo;
			photo = std::experimental::filesystem::current_path().string() + "\\Image\\" + name + ".jpg";
			wstring wphoto(photo.begin(), photo.end());
			std::wstring copyPhoto = msclr::interop::marshal_as<std::wstring>(pictureBox5->ImageLocation->ToString());
			CopyFileW((LPCWSTR)copyPhoto.c_str(), (LPCWSTR)wphoto.c_str(), false);
			if (name != computer[4].name)
			{
				photo = std::experimental::filesystem::current_path().string() + "\\Image\\" + computer[4].name + ".jpg";
				wphoto.clear();
				wphoto.append(photo.begin(), photo.end());
				DeleteFileW((LPCWSTR)wphoto.c_str());
			}
			photo = "\\Image\\" + name + ".jpg";

			addComputerToStore(name, cpu, gpu, motherboard, ram, spu, hdd, ssd, amount, cost, photo, 4);
			MessageBox::Show("Изменение проведено успешно!");
		}
		else
		{
			if (textBox30->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для мощности БП!");
			}

			if (AmountOrder4->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для количества товара!");
			}

			if (textBox29->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для цены товара!");
			}
		}
	}

	private: System::Void button20_Click(System::Object^ sender, System::EventArgs^ e)
	{
		Color color;
		if ((textBox37->ForeColor == color.Black) && (AmountOrder5->ForeColor == color.Black) && (textBox36->ForeColor == color.Black))
		{
			string cpu = msclr::interop::marshal_as<std::string>(textBox40->Text);
			string gpu = msclr::interop::marshal_as<std::string>(textBox39->Text);
			string name = msclr::interop::marshal_as<std::string>(textBox35->Text);
			string motherboard = msclr::interop::marshal_as<std::string>(textBox38->Text);
			int spu = atoi(msclr::interop::marshal_as<std::string>(textBox37->Text).c_str());
			int amount = atoi(msclr::interop::marshal_as<std::string>(AmountOrder5->Text).c_str());
			long int cost = atol(msclr::interop::marshal_as<std::string>(textBox36->Text).c_str());
			int ram = atoi(msclr::interop::marshal_as<std::string>(comboBox18->Text).c_str());
			int hdd = atoi(msclr::interop::marshal_as<std::string>(comboBox17->Text).c_str());
			int ssd = atoi(msclr::interop::marshal_as<std::string>(comboBox16->Text).c_str());

			Computers* computer = getComputers(adminPage * 8);
			string photo = std::experimental::filesystem::current_path().string() + computer[5].photo;
			photo = std::experimental::filesystem::current_path().string() + "\\Image\\" + name + ".jpg";
			wstring wphoto(photo.begin(), photo.end());
			std::wstring copyPhoto = msclr::interop::marshal_as<std::wstring>(pictureBox6->ImageLocation->ToString());
			CopyFileW((LPCWSTR)copyPhoto.c_str(), (LPCWSTR)wphoto.c_str(), false);
			if (name != computer[5].name)
			{
				photo = std::experimental::filesystem::current_path().string() + "\\Image\\" + computer[5].name + ".jpg";
				wphoto.clear();
				wphoto.append(photo.begin(), photo.end());
				DeleteFileW((LPCWSTR)wphoto.c_str());
			}
			photo = "\\Image\\" + name + ".jpg";

			addComputerToStore(name, cpu, gpu, motherboard, ram, spu, hdd, ssd, amount, cost, photo, 5);
			MessageBox::Show("Изменение проведено успешно!");
		}
		else
		{
			if (textBox37->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для мощности БП!");
			}

			if (AmountOrder5->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для количества товара!");
			}

			if (textBox36->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для цены товара!");
			}
		}
	}

	private: System::Void button22_Click(System::Object^ sender, System::EventArgs^ e)
	{
		Color color;
		if ((textBox44->ForeColor == color.Black) && (AmountOrder6->ForeColor == color.Black) && (textBox43->ForeColor == color.Black))
		{
			string cpu = msclr::interop::marshal_as<std::string>(textBox47->Text);
			string gpu = msclr::interop::marshal_as<std::string>(textBox46->Text);
			string name = msclr::interop::marshal_as<std::string>(textBox42->Text);
			string motherboard = msclr::interop::marshal_as<std::string>(textBox45->Text);
			int spu = atoi(msclr::interop::marshal_as<std::string>(textBox44->Text).c_str());
			int amount = atoi(msclr::interop::marshal_as<std::string>(AmountOrder6->Text).c_str());
			long int cost = atol(msclr::interop::marshal_as<std::string>(textBox43->Text).c_str());
			int ram = atoi(msclr::interop::marshal_as<std::string>(comboBox21->Text).c_str());
			int hdd = atoi(msclr::interop::marshal_as<std::string>(comboBox20->Text).c_str());
			int ssd = atoi(msclr::interop::marshal_as<std::string>(comboBox19->Text).c_str());

			Computers* computer = getComputers(adminPage * 8);
			string photo = std::experimental::filesystem::current_path().string() + computer[6].photo;
			photo = std::experimental::filesystem::current_path().string() + "\\Image\\" + name + ".jpg";
			wstring wphoto(photo.begin(), photo.end());
			std::wstring copyPhoto = msclr::interop::marshal_as<std::wstring>(pictureBox7->ImageLocation->ToString());
			CopyFileW((LPCWSTR)copyPhoto.c_str(), (LPCWSTR)wphoto.c_str(), false);
			if (name != computer[6].name)
			{
				photo = std::experimental::filesystem::current_path().string() + "\\Image\\" + computer[6].name + ".jpg";
				wphoto.clear();
				wphoto.append(photo.begin(), photo.end());
				DeleteFileW((LPCWSTR)wphoto.c_str());
			}
			photo = "\\Image\\" + name + ".jpg";

			addComputerToStore(name, cpu, gpu, motherboard, ram, spu, hdd, ssd, amount, cost, photo, 6);
			MessageBox::Show("Изменение проведено успешно!");
		}
		else
		{
			if (textBox44->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для мощности БП!");
			}

			if (AmountOrder6->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для количества товара!");
			}

			if (textBox43->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для цены товара!");
			}
		}
	}

	private: System::Void button24_Click(System::Object^ sender, System::EventArgs^ e)
	{
		Color color;
		if ((textBox51->ForeColor == color.Black) && (AmountOrder7->ForeColor == color.Black) && (textBox50->ForeColor == color.Black))
		{
			string cpu = msclr::interop::marshal_as<std::string>(textBox54->Text);
			string gpu = msclr::interop::marshal_as<std::string>(textBox53->Text);
			string name = msclr::interop::marshal_as<std::string>(textBox49->Text);
			string motherboard = msclr::interop::marshal_as<std::string>(textBox52->Text);
			int spu = atoi(msclr::interop::marshal_as<std::string>(textBox51->Text).c_str());
			int amount = atoi(msclr::interop::marshal_as<std::string>(AmountOrder7->Text).c_str());
			long int cost = atol(msclr::interop::marshal_as<std::string>(textBox50->Text).c_str());
			int ram = atoi(msclr::interop::marshal_as<std::string>(comboBox24->Text).c_str());
			int hdd = atoi(msclr::interop::marshal_as<std::string>(comboBox23->Text).c_str());
			int ssd = atoi(msclr::interop::marshal_as<std::string>(comboBox22->Text).c_str());

			Computers* computer = getComputers(adminPage * 8);
			string photo = std::experimental::filesystem::current_path().string() + computer[7].photo;
			photo = std::experimental::filesystem::current_path().string() + "\\Image\\" + name + ".jpg";
			wstring wphoto(photo.begin(), photo.end());
			std::wstring copyPhoto = msclr::interop::marshal_as<std::wstring>(pictureBox8->ImageLocation->ToString());
			CopyFileW((LPCWSTR)copyPhoto.c_str(), (LPCWSTR)wphoto.c_str(), false);
			if (name != computer[7].name)
			{
				photo = std::experimental::filesystem::current_path().string() + "\\Image\\" + computer[7].name + ".jpg";
				wphoto.clear();
				wphoto.append(photo.begin(), photo.end());
				DeleteFileW((LPCWSTR)wphoto.c_str());
			}
			photo = "\\Image\\" + name + ".jpg";

			addComputerToStore(name, cpu, gpu, motherboard, ram, spu, hdd, ssd, amount, cost, photo, 7);
			MessageBox::Show("Изменение проведено успешно!");
		}
		else
		{
			if (textBox51->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для мощности БП!");
			}

			if (AmountOrder7->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для количества товара!");
			}

			if (textBox50->ForeColor == color.Red)
			{
				MessageBox::Show("Введите корректные данные для цены товара!");
			}
		}
	}

	private: System::Void linkLabel1_LinkClicked(System::Object^ sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^ e)
	{
		indexRead = 1;
		this->ClientSize = System::Drawing::Size(834, 758);
		this->linkLabel1->Location = System::Drawing::Point(16, 1881);
		adminPage--;

		this->Hide();
		ChangeSpecs form;
		form.ShowDialog();
	}

	private: System::Void linkLabel2_LinkClicked(System::Object^ sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^ e)
	{
		indexRead = 0;
		adminPage++;
		groupBox8->Hide();
		groupBox7->Hide();
		groupBox6->Hide();
		groupBox5->Hide();
		groupBox4->Hide();
		groupBox3->Hide();
		groupBox2->Hide();

		this->Hide();
		ChangeSpecs form;
		form.ShowDialog();
	}

	private: System::Void button9_Click(System::Object^ sender, System::EventArgs^ e)
	{
		int amountOrder = atoi(msclr::interop::marshal_as<std::string>(AmountOrder0->Text).c_str());
		char buffer[20];
		if (amountOrder > 0)
		{
			AmountOrder0->Text = (msclr::interop::marshal_as<System::String^>(_itoa(--amountOrder, buffer, 10)));
		}
	}

	private: System::Void button10_Click(System::Object^ sender, System::EventArgs^ e)
	{
		int amountOrder = atoi(msclr::interop::marshal_as<std::string>(AmountOrder0->Text).c_str());
		char buffer[20];
		AmountOrder0->Text = (msclr::interop::marshal_as<System::String^>(_itoa(++amountOrder, buffer, 10)));
	}

	private: System::Void button11_Click(System::Object^ sender, System::EventArgs^ e)
	{
		int amountOrder = atoi(msclr::interop::marshal_as<std::string>(AmountOrder1->Text).c_str());
		char buffer[20];
		if (amountOrder > 0)
		{
			AmountOrder1->Text = (msclr::interop::marshal_as<System::String^>(_itoa(--amountOrder, buffer, 10)));
		}
	}

	private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e)
	{
		int amountOrder = atoi(msclr::interop::marshal_as<std::string>(AmountOrder1->Text).c_str());
		char buffer[20];
		AmountOrder1->Text = (msclr::interop::marshal_as<System::String^>(_itoa(++amountOrder, buffer, 10)));
	}

	private: System::Void button13_Click(System::Object^ sender, System::EventArgs^ e)
	{
		int amountOrder = atoi(msclr::interop::marshal_as<std::string>(AmountOrder2->Text).c_str());
		char buffer[20];
		if (amountOrder > 0)
		{
			AmountOrder2->Text = (msclr::interop::marshal_as<System::String^>(_itoa(--amountOrder, buffer, 10)));
		}
	}

	private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e)
	{
		int amountOrder = atoi(msclr::interop::marshal_as<std::string>(AmountOrder2->Text).c_str());
		char buffer[20];
		AmountOrder2->Text = (msclr::interop::marshal_as<System::String^>(_itoa(++amountOrder, buffer, 10)));
	}

	private: System::Void button15_Click(System::Object^ sender, System::EventArgs^ e)
	{
		int amountOrder = atoi(msclr::interop::marshal_as<std::string>(AmountOrder3->Text).c_str());
		char buffer[20];
		if (amountOrder > 0)
		{
			AmountOrder3->Text = (msclr::interop::marshal_as<System::String^>(_itoa(--amountOrder, buffer, 10)));
		}
	}

	private: System::Void button4_Click(System::Object^ sender, System::EventArgs^ e)
	{
		int amountOrder = atoi(msclr::interop::marshal_as<std::string>(AmountOrder3->Text).c_str());
		char buffer[20];
		AmountOrder3->Text = (msclr::interop::marshal_as<System::String^>(_itoa(++amountOrder, buffer, 10)));
	}

	private: System::Void button6_Click(System::Object^ sender, System::EventArgs^ e)
	{
		int amountOrder = atoi(msclr::interop::marshal_as<std::string>(AmountOrder4->Text).c_str());
		char buffer[20];
		if (amountOrder > 0)
		{
			AmountOrder4->Text = (msclr::interop::marshal_as<System::String^>(_itoa(--amountOrder, buffer, 10)));
		}
	}

	private: System::Void button5_Click(System::Object^ sender, System::EventArgs^ e)
	{
		int amountOrder = atoi(msclr::interop::marshal_as<std::string>(AmountOrder4->Text).c_str());
		char buffer[20];
		AmountOrder4->Text = (msclr::interop::marshal_as<System::String^>(_itoa(++amountOrder, buffer, 10)));
	}

	private: System::Void button19_Click(System::Object^ sender, System::EventArgs^ e)
	{
		int amountOrder = atoi(msclr::interop::marshal_as<std::string>(AmountOrder5->Text).c_str());
		char buffer[20];
		if (amountOrder > 0)
		{
			AmountOrder5->Text = (msclr::interop::marshal_as<System::String^>(_itoa(--amountOrder, buffer, 10)));
		}
	}

	private: System::Void button18_Click(System::Object^ sender, System::EventArgs^ e)
	{
		int amountOrder = atoi(msclr::interop::marshal_as<std::string>(AmountOrder5->Text).c_str());
		char buffer[20];
		AmountOrder5->Text = (msclr::interop::marshal_as<System::String^>(_itoa(++amountOrder, buffer, 10)));
	}

	private: System::Void button21_Click(System::Object^ sender, System::EventArgs^ e)
	{
		int amountOrder = atoi(msclr::interop::marshal_as<std::string>(AmountOrder6->Text).c_str());
		char buffer[20];
		if (amountOrder > 0)
		{
			AmountOrder6->Text = (msclr::interop::marshal_as<System::String^>(_itoa(--amountOrder, buffer, 10)));
		}
	}

	private: System::Void button7_Click(System::Object^ sender, System::EventArgs^ e)
	{
		int amountOrder = atoi(msclr::interop::marshal_as<std::string>(AmountOrder6->Text).c_str());
		char buffer[20];
		AmountOrder6->Text = (msclr::interop::marshal_as<System::String^>(_itoa(++amountOrder, buffer, 10)));
	}

	private: System::Void button23_Click(System::Object^ sender, System::EventArgs^ e)
	{
		int amountOrder = atoi(msclr::interop::marshal_as<std::string>(AmountOrder7->Text).c_str());
		char buffer[20];
		if (amountOrder > 0)
		{
			AmountOrder7->Text = (msclr::interop::marshal_as<System::String^>(_itoa(--amountOrder, buffer, 10)));
		}
	}

	private: System::Void button8_Click(System::Object^ sender, System::EventArgs^ e)
	{
		int amountOrder = atoi(msclr::interop::marshal_as<std::string>(AmountOrder7->Text).c_str());
		char buffer[20];
		AmountOrder7->Text = (msclr::interop::marshal_as<System::String^>(_itoa(++amountOrder, buffer, 10)));
	}

	private: System::Void button25_Click(System::Object^ sender, System::EventArgs^ e)
	{
		Computers* computer;
		computer = getComputers(adminPage * 8);

		int fileLength = getFileLength(PATH_CATALOG);

		if ((fileLength - (adminPage) * 8) == 1 && fileLength != 1)
		{
			adminPage--;
		}

		string photo = std::experimental::filesystem::current_path().string() + computer[0].photo;
		wstring wphoto(photo.begin(), photo.end());
		DeleteFileW((LPCWSTR)wphoto.c_str());

		deleteComputer(computer[0].name, PATH_CATALOG);
		delete[8] computer;

		this->Hide();
	}

	private: System::Void button26_Click(System::Object^ sender, System::EventArgs^ e)
	{
		Computers* computer;
		computer = getComputers(adminPage * 8);

		string photo = std::experimental::filesystem::current_path().string() + computer[1].photo;
		wstring wphoto(photo.begin(), photo.end());
		DeleteFileW((LPCWSTR)wphoto.c_str());

		deleteComputer(computer[1].name, PATH_CATALOG);
		delete[8] computer;

		this->Hide();
	}

	private: System::Void button27_Click(System::Object^ sender, System::EventArgs^ e)
	{
		Computers* computer;
		computer = getComputers(adminPage * 8);

		string photo = std::experimental::filesystem::current_path().string() + computer[2].photo;
		wstring wphoto(photo.begin(), photo.end());
		DeleteFileW((LPCWSTR)wphoto.c_str());

		deleteComputer(computer[2].name, PATH_CATALOG);
		delete[8] computer;

		this->Hide();
	}

	private: System::Void button28_Click(System::Object^ sender, System::EventArgs^ e)
	{
		Computers* computer;
		computer = getComputers(adminPage * 8);

		string photo = std::experimental::filesystem::current_path().string() + computer[3].photo;
		wstring wphoto(photo.begin(), photo.end());
		DeleteFileW((LPCWSTR)wphoto.c_str());

		deleteComputer(computer[3].name, PATH_CATALOG);
		delete[8] computer;

		this->Hide();
	}

	private: System::Void button29_Click(System::Object^ sender, System::EventArgs^ e)
	{
		Computers* computer;
		computer = getComputers(adminPage * 8);

		string photo = std::experimental::filesystem::current_path().string() + computer[4].photo;
		wstring wphoto(photo.begin(), photo.end());
		DeleteFileW((LPCWSTR)wphoto.c_str());

		deleteComputer(computer[4].name, PATH_CATALOG);
		delete[8] computer;

		this->Hide();
	}

	private: System::Void button30_Click(System::Object^ sender, System::EventArgs^ e)
	{
		Computers* computer;
		computer = getComputers(adminPage * 8);

		string photo = std::experimental::filesystem::current_path().string() + computer[5].photo;
		wstring wphoto(photo.begin(), photo.end());
		DeleteFileW((LPCWSTR)wphoto.c_str());

		deleteComputer(computer[5].name, PATH_CATALOG);
		delete[8] computer;

		this->Hide();
	}

	private: System::Void button31_Click(System::Object^ sender, System::EventArgs^ e)
	{
		Computers* computer;
		computer = getComputers(adminPage * 8);

		string photo = std::experimental::filesystem::current_path().string() + computer[6].photo;
		wstring wphoto(photo.begin(), photo.end());
		DeleteFileW((LPCWSTR)wphoto.c_str());

		deleteComputer(computer[6].name, PATH_CATALOG);
		delete[8] computer;

		this->Hide();
	}

	private: System::Void button32_Click(System::Object^ sender, System::EventArgs^ e)
	{
		Computers* computer;
		computer = getComputers(adminPage * 8);

		string photo = std::experimental::filesystem::current_path().string() + computer[7].photo;
		wstring wphoto(photo.begin(), photo.end());
		DeleteFileW((LPCWSTR)wphoto.c_str());

		deleteComputer(computer[7].name, PATH_CATALOG);
		delete[8] computer;

		this->Hide();
	}

	private: System::Void linkLabel3_LinkClicked(System::Object^ sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^ e)
	{
		this->Hide();
	}

	private: System::Void pictureBox_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		openFileDialog1->ShowDialog();
		System::Windows::Forms::PictureBox^ pictureBox = (PictureBox^)sender;
		pictureBox->ImageLocation = openFileDialog1->FileName;
	}

	private: System::Void textBoxChange_TextChanged(System::Object^ sender, System::EventArgs^ e)
	{
		System::Windows::Forms::TextBox^ textBox = (TextBox^)sender;
		Color color;
		if (checkNoNumber(msclr::interop::marshal_as<std::string>(textBox->Text)))
		{
			textBox->ForeColor = color.Red;
		}
		else
		{
			textBox->ForeColor = color.Black;
		}
	}

};
}