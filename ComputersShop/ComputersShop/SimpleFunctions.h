#pragma once
#include <fstream>
#include <string>
#include "ComputersList.h"

Computers* getComputers(int place)
{
	ifstream fromFile(PATH_CATALOG, ios::binary);

	Computers* computer = new Computers[8];

	fromFile.seekg(place * sizeof(Computers));

	for (int i{}; i < 8; i++)
	{
		if (fromFile.eof())
		{
			break;
		}

		fromFile.read((char*)&computer[i], sizeof(Computers));
	}
	fromFile.close();

	return computer;
}

int getFileLength(string path)
{
	createComputersFile(path);

	ifstream fromFile(path, ios::binary);

	fromFile.seekg(0, ios::end);
	
	return fromFile.tellg() / sizeof(Computers);
}

void deleteComputer(string _name, string filePath)
{
	ifstream fromFile(filePath, ios::binary);
	ofstream inFile("temp.dat", ios::binary);

	Computers computer;

	while (true)
	{
		fromFile.read((char*)&computer, sizeof(Computers));

		if (fromFile.eof())
		{
			break;
		}

		if (computer.name != _name)
		{
			inFile.write((char*)&computer, sizeof(Computers));
		}
	}
	inFile.close();
	fromFile.close();

	remove(filePath.c_str());
	rename("temp.dat", filePath.c_str());
}

bool checkNoNumber(std::string number)
{
	const char digits[11] = "0123456789";
	for (int i = 0; i < number.length(); i++)
	{
		bool check = false;
		for (int j = 0; j < 10; j++)
		{
			if (number[i] == digits[j])
			{
				check = true;
				break;
			}
		}
		if (!check)
		{
			return true;
		}
	}
	return false;
}