#pragma once
#include <iostream>
#include <msclr/marshal_cppstd.h>
#include "File.h"
#include "ComputersList.h"
#include "SimpleFunctions.h"

using namespace std;

const string ORDER_FILE = "Orders.txt";

unsigned int shoppingCartPage = 0;
unsigned int shoppingCartFileLength = 0;
string login;

Computers* getShoppingCartComputers(int place)
{
	ifstream fromFile(shoppingCartFile, ios::binary);

	Computers* computer = new Computers[4];

	fromFile.seekg(place * sizeof(Computers));

	for (int i{}; i < 4; i++)
	{
		fromFile.read((char*)&computer[i], sizeof(Computers));

		if (fromFile.eof())
		{
			break;
		}
	}
	fromFile.close();

	return computer;
}

void addComputerToShoppingCart(Computers computer, string file)
{
	if (!checkingExist(computer.name, shoppingCartFile))
	{
		ofstream inFile(file, ios::binary | ios::app);

		inFile.write((char*)&computer, sizeof(Computers));

		inFile.close();
	}
	else
	{
		System::Windows::Forms::MessageBox::Show("����� ��� �������� � �������!");
	}
}

void refreshShoppingCart(Computers computer, int place)
{
	ofstream inFile(shoppingCartFile, ios::binary | ios::in | ios::out);

	inFile.seekp((place + shoppingCartPage * 4) * sizeof(Computers), ios::beg);
	inFile.write((char*)&computer, sizeof(Computers));

	inFile.close();
}

long getPrice()
{
	ifstream fromFile(shoppingCartFile, ios::binary);

	int binFileLength = getFileLength(shoppingCartFile);

	Computers* computer = new Computers[binFileLength];

	fromFile.read((char*)&computer, sizeof(Computers) * binFileLength);
	long price = 0;

	for (int i{}; i < binFileLength; i++)
	{
		price += computer[i].buyAmount * computer[i].cost;
	}

	delete[binFileLength] computer;
	return price;
}

void reduceAmount()
{
	ifstream fromBinFile(shoppingCartFile, ios::binary);
	ifstream fromMainFile(PATH_CATALOG, ios::binary);

	int shoppingCartFileLength = getFileLength(shoppingCartFile);
	int storeFileLength = getFileLength(PATH_CATALOG);

	Computers* shoppingCartComputer = new Computers[shoppingCartFileLength];
	Computers* computer = new Computers[storeFileLength];

	fromBinFile.read((char*)shoppingCartComputer, sizeof(Computers) * shoppingCartFileLength);
	fromMainFile.read((char*)computer, sizeof(Computers) * storeFileLength);

	fromBinFile.close();
	fromMainFile.close();

	for (int i{}; i < shoppingCartFileLength; i++)
	{
		for (int j{}; j < storeFileLength; j++)
		{
			if (!strcmp(computer[j].name, shoppingCartComputer[i].name))
			{
				computer[j].amount -= shoppingCartComputer[i].buyAmount;
				break;
			}
		}
	}

	ofstream inMainFile(PATH_CATALOG, ios::binary);
	
	inMainFile.write((char*)computer, sizeof(Computers) * storeFileLength);
	inMainFile.close();

	delete[shoppingCartFileLength] shoppingCartComputer;
	delete[storeFileLength] computer;
}

void deleteShoppingCartComputer(string name)
{
	ifstream fromFile(shoppingCartFile, ios::binary);
	ofstream inFile("temp.dat", ios::binary);

	Computers computer;

	while (true)
	{
		fromFile.read((char*)&computer, sizeof(Computers));

		if (fromFile.eof())
		{
			break;
		}

		if (computer.name != name)
		{
			inFile.write((char*)&computer, sizeof(Computers));
		}
	}

	inFile.close();
	fromFile.close();

	remove(shoppingCartFile.c_str());
	rename("temp.dat", shoppingCartFile.c_str());
}

void addToOrderFile()
{
	ofstream inFile(ORDER_FILE, ios::app);
	ifstream fromFile(shoppingCartFile, ios::binary);

	int shoppingCartFileLength = getFileLength(shoppingCartFile);

	Computers* shoppingCartComputers = new Computers[shoppingCartFileLength];

	fromFile.read((char*)shoppingCartComputers, sizeof(Computers) * shoppingCartFileLength);
	fromFile.close();

	for (int i{}; i < shoppingCartFileLength; i++)
	{
		inFile << login << " ������� " << "��������� " << shoppingCartComputers[i].name << " � ���������� " << shoppingCartComputers[i].buyAmount << " ��." << endl;
	}

	inFile.close();
}